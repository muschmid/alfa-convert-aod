#testFile = os.getenv("ALRB_TutorialData") + '/mc21_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.deriv.DAOD_PHYS.e8357_s3802_r13508_p5057/DAOD_PHYS.28625583._000007.pool.root.1'
#filename = str(sys.argv[1])
filename = os.getenv("AthenaFile")

print((filename))

folder = '/beegfs/schmidt/data/data23_13p6TeV.00455462.calibration_ALFACalib.recon.AOD.c1443/'

# Create list of all files in folder
#fileList = os.listdir(folder)

#fileList.sort()

#fileList2 = []

#for file in fileList:
#    fileList2.append(folder + file)

# Print list of all files
#print(fileList2)

#testFile = "/beegfs/schmidt/data/data23_13p6TeV.00455462.calibration_ALFACalib.recon.AOD.c1443/data23_13p6TeV.00455462.calibration_ALFACalib.recon.AOD.c1443._lb0000._SFO-11._0001.1"

#override next line on command line with: --filesInput=XXX
jps.AthenaCommonFlags.FilesInput = [folder+filename]

#Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "ClassAccess" 

# Write output file
jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:/beegfs/schmidt/data/"+filename+".outputs.root"]
svcMgr.THistSvc.MaxFileSize=-1 #speeds up jobs that output lots of histograms

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the main alg sequence
athAlgSeq += alg

# limit the number of events (for testing purposes)
#theApp.EvtMax = 500

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")