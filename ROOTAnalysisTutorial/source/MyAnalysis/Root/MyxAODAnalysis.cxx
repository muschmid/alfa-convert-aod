#include <xAODEventInfo/EventInfo.h>
#include <AsgMessaging/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>

#include <xAODForward/ALFAData.h>
#include <xAODForward/ALFADataContainer.h>
#include <xAODForward/ALFADataAuxContainer.h>
#include "xAODEventInfo/EventInfo.h"

#include "xAODRootAccess/tools/Message.h"
#include "xAODRootAccess/TStore.h"

/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK(CONTEXT, EXP)                    \
    do                                                   \
    {                                                    \
        if (!EXP.isSuccess())                            \
        {                                                \
            Error(CONTEXT,                               \
                  XAOD_MESSAGE("Failed to execute: %s"), \
                  #EXP);                                 \
            return StatusCode::FAILURE;                  \
        }                                                \
    } while (false)

MyxAODAnalysis::MyxAODAnalysis(const std::string &name,
                               ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm(name, pSvcLocator)
{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  This is also where you
    // declare all properties for your algorithm.  Note that things like
    // resetting statistics variables or booking histograms should
    // rather go into the initialize() function.
}

MyxAODAnalysis::~MyxAODAnalysis()
{
    // Deleting variables to avoid memory leak
}

StatusCode MyxAODAnalysis::initialize()
{
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    ANA_MSG_INFO("in initialize");

    ANA_CHECK(book(TTree("analysis", "My analysis ntuple")));

    TTree *mytree = tree("analysis");

    mytree->Branch("RunNumber", &m_runNumber);
    mytree->Branch("EventNumber", &m_eventNumber);

    ANA_CHECK(book(TTree("EventHeader", "EventHeader")));
    ANA_CHECK(book(TTree("TrackingData", "TrackingData")));
    ANA_CHECK(book(TTree("TriggerKeys", "TriggerKeys")));
    ANA_CHECK(book(TTree("TriggerKeysBP", "TriggerKeysBP")));
    ANA_CHECK(book(TTree("TriggerKeysAP", "TriggerKeysAP")));
    ANA_CHECK(book(TTree("TriggerKeysAV", "TriggerKeysAV")));
    ANA_CHECK(book(TTree("TriggerPrescales", "TriggerPrescales")));

    TTree *EventHeader = tree("EventHeader", "EventHeader");
    TTree *TrackingData = tree("TrackingData", "TrackingData");
    TTree *TriggerKeys = tree("TriggerKeys", "TriggerKeys");
    TTree *TriggerKeysBP = tree("TriggerKeysBP", "TriggerKeysBP");
    TTree *TriggerKeysAP = tree("TriggerKeysAP", "TriggerKeysAP");
    TTree *TriggerKeysAV = tree("TriggerKeysAV", "TriggerKeysAV");
    TTree *TriggerPrescales = tree("TriggerPrescales", "TriggerPrescales");

    EventHeader->Branch("Evt_num", &INTEvt_num, "Evt_num/I");
    EventHeader->Branch("Lum_block", &INTLum_block, "Lum_block/I");
    EventHeader->Branch("BCId", &INTBCId, "BCId/I");

    TrackingData->Branch("NumTrack", &INTNumTrack, "NumTrack/I");
    TrackingData->Branch("RecFlag", INTRecFlag, "RecFlag[12][8]/O");
    TrackingData->Branch("x_Det", INTx_Det, "x_Det[12][8]/F");
    TrackingData->Branch("y_Det", INTy_Det, "y_Det[12][8]/F");
    TrackingData->Branch("Detector", INTDetector, "Detector[12][8]/I");
    TrackingData->Branch("OverU", INTOverU, "OverU[12][8]/F");
    TrackingData->Branch("OverV", INTOverV, "OverV[12][8]/F");
    TrackingData->Branch("OverY", INTOverY, "OverY[12][8]/F");
    TrackingData->Branch("NU", INTNU, "NU[12][8]/I");
    TrackingData->Branch("NV", INTNV, "NV[12][8]/I");
    TrackingData->Branch("NY", INTNY, "NY[12][8]/I");
    TrackingData->Branch("Fib_SelMD", INTFib_SelMD, "Fib_SelMD[12][8][20]/I");
    TrackingData->Branch("Fib_SelOD", INTFib_SelOD, "Fib_SelOD[12][8][3]/I");
    TrackingData->Branch("mdMultiplicity", INTMultiMD, "MultiMD[8][20]/I");
    TrackingData->Branch("Scaler", INTScaler, "Scaler[8]/I");
    TrackingData->Branch("TrigPat", INTTrigPat, "TrigPat[8][16]/O");
    TrackingData->Branch("FiberHitsMD", INTFiberHitsMD, "FiberHitsMD[8][20][64]/O");
    TrackingData->Branch("FiberHitsODPos", INTFiberHitsODPos, "FiberHitsODPos[8][3][30]/O");
    TrackingData->Branch("FiberHitsODNeg", INTFiberHitsODNeg, "FiberHitsODNeg[8][3][30]/O");
    TrackingData->Branch("MultiODNeg", INTMultiODNeg, "MultiODNeg[8][3]/I");
    TrackingData->Branch("MultiODPos", INTMultiODPos, "MultiODPos[8][3]/I");
    //~ TrackingData->Branch("QDCTrig",INTQDCTrig,"QDCTrig[8][2]/I");

    TriggerKeys->Branch("L1_TE30", &L1_TE30, "L1_TE30/O");
    TriggerKeys->Branch("L1_MBTS_1_EMPTY", &L1_MBTS_1_EMPTY, "L1_MBTS_1_EMPTY/O");
    TriggerKeys->Branch("L1_MBTS_1_UNPAIRED_ISO", &L1_MBTS_1_UNPAIRED_ISO, "L1_MBTS_1_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_MBTS_2_EMPTY", &L1_MBTS_2_EMPTY, "L1_MBTS_2_EMPTY/O");
    TriggerKeys->Branch("L1_MBTS_2_UNPAIRED_ISO", &L1_MBTS_2_UNPAIRED_ISO, "L1_MBTS_2_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_MBTS_1_1_EMPTY", &L1_MBTS_1_1_EMPTY, "L1_MBTS_1_1_EMPTY/O");
    TriggerKeys->Branch("L1_MBTS_1_1_UNPAIRED_ISO", &L1_MBTS_1_1_UNPAIRED_ISO, "L1_MBTS_1_1_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_AFP_C_ALFA_A", &L1_AFP_C_ALFA_A, "L1_AFP_C_ALFA_A/O");
    TriggerKeys->Branch("L1_AFP_C_ANY", &L1_AFP_C_ANY, "L1_AFP_C_ANY/O");
    TriggerKeys->Branch("L1_MBTS_4_A_UNPAIRED_ISO", &L1_MBTS_4_A_UNPAIRED_ISO, "L1_MBTS_4_A_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_MBTS_4_C_UNPAIRED_ISO", &L1_MBTS_4_C_UNPAIRED_ISO, "L1_MBTS_4_C_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_AFP_C_ALFA_C", &L1_AFP_C_ALFA_C, "L1_AFP_C_ALFA_C/O");
    TriggerKeys->Branch("L1_MBTS_4_A", &L1_MBTS_4_A, "L1_MBTS_4_A/O");
    TriggerKeys->Branch("L1_MBTS_4_C", &L1_MBTS_4_C, "L1_MBTS_4_C/O");
    TriggerKeys->Branch("L1_MBTS_1_BGRP9", &L1_MBTS_1_BGRP9, "L1_MBTS_1_BGRP9/O");
    TriggerKeys->Branch("L1_MBTS_2_BGRP9", &L1_MBTS_2_BGRP9, "L1_MBTS_2_BGRP9/O");
    TriggerKeys->Branch("L1_MBTS_1_BGRP11", &L1_MBTS_1_BGRP11, "L1_MBTS_1_BGRP11/O");
    TriggerKeys->Branch("L1_MBTS_2_BGRP11", &L1_MBTS_2_BGRP11, "L1_MBTS_2_BGRP11/O");
    TriggerKeys->Branch("L1_LUCID", &L1_LUCID, "L1_LUCID/O");
    TriggerKeys->Branch("L1_LUCID_EMPTY", &L1_LUCID_EMPTY, "L1_LUCID_EMPTY/O");
    TriggerKeys->Branch("L1_LUCID_UNPAIRED_ISO", &L1_LUCID_UNPAIRED_ISO, "L1_LUCID_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_LUCID_A_C_EMPTY", &L1_LUCID_A_C_EMPTY, "L1_LUCID_A_C_EMPTY/O");
    TriggerKeys->Branch("L1_LUCID_A_C_UNPAIRED_ISO", &L1_LUCID_A_C_UNPAIRED_ISO, "L1_LUCID_A_C_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_LUCID_A_C_UNPAIRED_NONISO", &L1_LUCID_A_C_UNPAIRED_NONISO, "L1_LUCID_A_C_UNPAIRED_NONISO/O");
    TriggerKeys->Branch("L1_MBTS_1", &L1_MBTS_1, "L1_MBTS_1/O");
    TriggerKeys->Branch("L1_MBTS_2", &L1_MBTS_2, "L1_MBTS_2/O");
    TriggerKeys->Branch("L1_MBTS_1_1", &L1_MBTS_1_1, "L1_MBTS_1_1/O");
    TriggerKeys->Branch("L1_ZDC_A", &L1_ZDC_A, "L1_ZDC_A/O");
    TriggerKeys->Branch("L1_ZDC_C", &L1_ZDC_C, "L1_ZDC_C/O");
    TriggerKeys->Branch("L1_ALFA_ELAST1", &L1_ALFA_ELAST1, "L1_ALFA_ELAST1/O");
    TriggerKeys->Branch("L1_ALFA_ELAST2", &L1_ALFA_ELAST2, "L1_ALFA_ELAST2/O");
    TriggerKeys->Branch("L1_ALFA_ELAST11", &L1_ALFA_ELAST11, "L1_ALFA_ELAST11/O");
    TriggerKeys->Branch("L1_ALFA_ELAST12", &L1_ALFA_ELAST12, "L1_ALFA_ELAST12/O");
    TriggerKeys->Branch("L1_ALFA_ELAST13", &L1_ALFA_ELAST13, "L1_ALFA_ELAST13/O");
    TriggerKeys->Branch("L1_ALFA_ELAST14", &L1_ALFA_ELAST14, "L1_ALFA_ELAST14/O");
    TriggerKeys->Branch("L1_ALFA_ELAST15", &L1_ALFA_ELAST15, "L1_ALFA_ELAST15/O");
    TriggerKeys->Branch("L1_ALFA_ELAST15_Calib", &L1_ALFA_ELAST15_Calib, "L1_ALFA_ELAST15_Calib/O");
    TriggerKeys->Branch("L1_ALFA_ELAST16", &L1_ALFA_ELAST16, "L1_ALFA_ELAST16/O");
    TriggerKeys->Branch("L1_ALFA_ELAST17", &L1_ALFA_ELAST17, "L1_ALFA_ELAST17/O");
    TriggerKeys->Branch("L1_ALFA_ELAST18", &L1_ALFA_ELAST18, "L1_ALFA_ELAST18/O");
    TriggerKeys->Branch("L1_ALFA_ELAST18_Calib", &L1_ALFA_ELAST18_Calib, "L1_ALFA_ELAST18_Calib/O");
    TriggerKeys->Branch("L1_ALFA_SDIFF5", &L1_ALFA_SDIFF5, "L1_ALFA_SDIFF5/O");
    TriggerKeys->Branch("L1_ALFA_SDIFF6", &L1_ALFA_SDIFF6, "L1_ALFA_SDIFF6/O");
    TriggerKeys->Branch("L1_ALFA_SDIFF7", &L1_ALFA_SDIFF7, "L1_ALFA_SDIFF7/O");
    TriggerKeys->Branch("L1_ALFA_SDIFF8", &L1_ALFA_SDIFF8, "L1_ALFA_SDIFF8/O");
    TriggerKeys->Branch("L1_MBTS_1_A_ALFA_C", &L1_MBTS_1_A_ALFA_C, "L1_MBTS_1_A_ALFA_C/O");
    TriggerKeys->Branch("L1_MBTS_1_C_ALFA_A", &L1_MBTS_1_C_ALFA_A, "L1_MBTS_1_C_ALFA_A/O");
    TriggerKeys->Branch("L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO", &L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO, "L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO", &L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO, "L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_MBTS_2_A_ALFA_C", &L1_MBTS_2_A_ALFA_C, "L1_MBTS_2_A_ALFA_C/O");
    TriggerKeys->Branch("L1_MBTS_2_C_ALFA_A", &L1_MBTS_2_C_ALFA_A, "L1_MBTS_2_C_ALFA_A/O");
    TriggerKeys->Branch("L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO", &L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO, "L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO", &L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO, "L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_LUCID_A_ALFA_C", &L1_LUCID_A_ALFA_C, "L1_LUCID_A_ALFA_C/O");
    TriggerKeys->Branch("L1_LUCID_C_ALFA_A", &L1_LUCID_C_ALFA_A, "L1_LUCID_C_ALFA_A/O");
    TriggerKeys->Branch("L1_LUCID_A_ALFA_C_UNPAIRED_ISO", &L1_LUCID_A_ALFA_C_UNPAIRED_ISO, "L1_LUCID_A_ALFA_C_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_LUCID_C_ALFA_A_UNPAIRED_ISO", &L1_LUCID_C_ALFA_A_UNPAIRED_ISO, "L1_LUCID_C_ALFA_A_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_EM3_ALFA_ANY", &L1_EM3_ALFA_ANY, "L1_EM3_ALFA_ANY/O");
    TriggerKeys->Branch("L1_EM3_ALFA_ANY_UNPAIRED_ISO", &L1_EM3_ALFA_ANY_UNPAIRED_ISO, "L1_EM3_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_EM3_ALFA_EINE", &L1_EM3_ALFA_EINE, "L1_EM3_ALFA_EINE/O");
    TriggerKeys->Branch("L1_ALFA_ELASTIC_UNPAIRED_ISO", &L1_ALFA_ELASTIC_UNPAIRED_ISO, "L1_ALFA_ELASTIC_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO", &L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO, "L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_ALFA_ANY_A_EMPTY", &L1_ALFA_ANY_A_EMPTY, "L1_ALFA_ANY_A_EMPTY/O");
    TriggerKeys->Branch("L1_ALFA_ANY_C_EMPTY", &L1_ALFA_ANY_C_EMPTY, "L1_ALFA_ANY_C_EMPTY/O");
    TriggerKeys->Branch("L1_J12_ALFA_ANY", &L1_J12_ALFA_ANY, "L1_J12_ALFA_ANY/O");
    TriggerKeys->Branch("L1_J12_ALFA_ANY_UNPAIRED_ISO", &L1_J12_ALFA_ANY_UNPAIRED_ISO, "L1_J12_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_TE5_ALFA_ANY", &L1_TE5_ALFA_ANY, "L1_TE5_ALFA_ANY/O");
    TriggerKeys->Branch("L1_TE5_ALFA_ANY_UNPAIRED_ISO", &L1_TE5_ALFA_ANY_UNPAIRED_ISO, "L1_TE5_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_TE5_ALFA_EINE", &L1_TE5_ALFA_EINE, "L1_TE5_ALFA_EINE/O");
    TriggerKeys->Branch("L1_TRT_ALFA_ANY", &L1_TRT_ALFA_ANY, "L1_TRT_ALFA_ANY/O");
    TriggerKeys->Branch("L1_TRT_ALFA_ANY_UNPAIRED_ISO", &L1_TRT_ALFA_ANY_UNPAIRED_ISO, "L1_TRT_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_TRT_ALFA_EINE", &L1_TRT_ALFA_EINE, "L1_TRT_ALFA_EINE/O");
    TriggerKeys->Branch("L1_ALFA_BGT", &L1_ALFA_BGT, "L1_ALFA_BGT/O");
    TriggerKeys->Branch("L1_ALFA_BGT_UNPAIRED_ISO", &L1_ALFA_BGT_UNPAIRED_ISO, "L1_ALFA_BGT_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_ALFA_BGT_BGRP10", &L1_ALFA_BGT_BGRP10, "L1_ALFA_BGT_BGRP10/O");
    TriggerKeys->Branch("L1_ALFA_SHOWSYST5", &L1_ALFA_SHOWSYST5, "L1_ALFA_SHOWSYST5/O");
    TriggerKeys->Branch("L1_ALFA_SYST9", &L1_ALFA_SYST9, "L1_ALFA_SYST9/O");
    TriggerKeys->Branch("L1_ALFA_SYST10", &L1_ALFA_SYST10, "L1_ALFA_SYST10/O");
    TriggerKeys->Branch("L1_ALFA_SYST11", &L1_ALFA_SYST11, "L1_ALFA_SYST11/O");
    TriggerKeys->Branch("L1_ALFA_SYST12", &L1_ALFA_SYST12, "L1_ALFA_SYST12/O");
    TriggerKeys->Branch("L1_ALFA_SYST17", &L1_ALFA_SYST17, "L1_ALFA_SYST17/O");
    TriggerKeys->Branch("L1_ALFA_SYST18", &L1_ALFA_SYST18, "L1_ALFA_SYST18/O");
    TriggerKeys->Branch("L1_ALFA_ANY", &L1_ALFA_ANY, "L1_ALFA_ANY/O");
    TriggerKeys->Branch("L1_ALFA_ANY_EMPTY", &L1_ALFA_ANY_EMPTY, "L1_ALFA_ANY_EMPTY/O");
    TriggerKeys->Branch("L1_ALFA_ANY_FIRSTEMPTY", &L1_ALFA_ANY_FIRSTEMPTY, "L1_ALFA_ANY_FIRSTEMPTY/O");
    TriggerKeys->Branch("L1_ALFA_ANY_UNPAIRED_ISO", &L1_ALFA_ANY_UNPAIRED_ISO, "L1_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeys->Branch("L1_ALFA_ANY_UNPAIRED_NONISO", &L1_ALFA_ANY_UNPAIRED_NONISO, "L1_ALFA_ANY_UNPAIRED_NONISO/O");
    TriggerKeys->Branch("L1_ALFA_ANY_BGRP10", &L1_ALFA_ANY_BGRP10, "L1_ALFA_ANY_BGRP10/O");
    TriggerKeys->Branch("L1_ALFA_ANY_ABORTGAPNOTCALIB", &L1_ALFA_ANY_ABORTGAPNOTCALIB, "L1_ALFA_ANY_ABORTGAPNOTCALIB/O");
    TriggerKeys->Branch("L1_ALFA_ANY_CALIB", &L1_ALFA_ANY_CALIB, "L1_ALFA_ANY_CALIB/O");
    TriggerKeys->Branch("L1_ALFA_B7L1U", &L1_ALFA_B7L1U, "L1_ALFA_B7L1U/O");
    TriggerKeys->Branch("L1_ALFA_B7L1L", &L1_ALFA_B7L1L, "L1_ALFA_B7L1L/O");
    TriggerKeys->Branch("L1_ALFA_A7L1U", &L1_ALFA_A7L1U, "L1_ALFA_A7L1U/O");
    TriggerKeys->Branch("L1_ALFA_A7L1L", &L1_ALFA_A7L1L, "L1_ALFA_A7L1L/O");
    TriggerKeys->Branch("L1_ALFA_A7R1U", &L1_ALFA_A7R1U, "L1_ALFA_A7R1U/O");
    TriggerKeys->Branch("L1_ALFA_A7R1L", &L1_ALFA_A7R1L, "L1_ALFA_A7R1L/O");
    TriggerKeys->Branch("L1_ALFA_B7R1U", &L1_ALFA_B7R1U, "L1_ALFA_B7R1U/O");
    TriggerKeys->Branch("L1_ALFA_B7R1L", &L1_ALFA_B7R1L, "L1_ALFA_B7R1L/O");
    TriggerKeys->Branch("L1_ALFA_B7L1U_OD", &L1_ALFA_B7L1U_OD, "L1_ALFA_B7L1U_OD/O");
    TriggerKeys->Branch("L1_ALFA_B7L1L_OD", &L1_ALFA_B7L1L_OD, "L1_ALFA_B7L1L_OD/O");
    TriggerKeys->Branch("L1_ALFA_A7L1U_OD", &L1_ALFA_A7L1U_OD, "L1_ALFA_A7L1U_OD/O");
    TriggerKeys->Branch("L1_ALFA_A7L1L_OD", &L1_ALFA_A7L1L_OD, "L1_ALFA_A7L1L_OD/O");
    TriggerKeys->Branch("L1_ALFA_A7R1U_OD", &L1_ALFA_A7R1U_OD, "L1_ALFA_A7R1U_OD/O");
    TriggerKeys->Branch("L1_ALFA_A7R1L_OD", &L1_ALFA_A7R1L_OD, "L1_ALFA_A7R1L_OD/O");
    TriggerKeys->Branch("L1_ALFA_B7R1U_OD", &L1_ALFA_B7R1U_OD, "L1_ALFA_B7R1U_OD/O");
    TriggerKeys->Branch("L1_ALFA_B7R1L_OD", &L1_ALFA_B7R1L_OD, "L1_ALFA_B7R1L_OD/O");
    TriggerKeys->Branch("L1_ALFA_B7L1_OD", &L1_ALFA_B7L1_OD, "L1_ALFA_B7L1_OD/O");
    TriggerKeys->Branch("L1_ALFA_A7L1_OD", &L1_ALFA_A7L1_OD, "L1_ALFA_A7L1_OD/O");
    TriggerKeys->Branch("L1_ALFA_B7R1_OD", &L1_ALFA_B7R1_OD, "L1_ALFA_B7R1_OD/O");
    TriggerKeys->Branch("L1_ALFA_A7R1_OD", &L1_ALFA_A7R1_OD, "L1_ALFA_A7R1_OD/O");

    TriggerKeysBP->Branch("TBP_L1_TE30", &TBP_L1_TE30, "TBP_L1_TE30/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_1_EMPTY", &TBP_L1_MBTS_1_EMPTY, "TBP_L1_MBTS_1_EMPTY/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_1_UNPAIRED_ISO", &TBP_L1_MBTS_1_UNPAIRED_ISO, "TBP_L1_MBTS_1_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_2_EMPTY", &TBP_L1_MBTS_2_EMPTY, "TBP_L1_MBTS_2_EMPTY/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_2_UNPAIRED_ISO", &TBP_L1_MBTS_2_UNPAIRED_ISO, "TBP_L1_MBTS_2_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_1_1_EMPTY", &TBP_L1_MBTS_1_1_EMPTY, "TBP_L1_MBTS_1_1_EMPTY/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_1_1_UNPAIRED_ISO", &TBP_L1_MBTS_1_1_UNPAIRED_ISO, "TBP_L1_MBTS_1_1_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_AFP_C_ALFA_A", &TBP_L1_AFP_C_ALFA_A, "TBP_L1_AFP_C_ALFA_A/O");
    TriggerKeysBP->Branch("TBP_L1_AFP_C_ANY", &TBP_L1_AFP_C_ANY, "TBP_L1_AFP_C_ANY/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_4_A_UNPAIRED_ISO", &TBP_L1_MBTS_4_A_UNPAIRED_ISO, "TBP_L1_MBTS_4_A_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_4_C_UNPAIRED_ISO", &TBP_L1_MBTS_4_C_UNPAIRED_ISO, "TBP_L1_MBTS_4_C_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_AFP_C_ALFA_C", &TBP_L1_AFP_C_ALFA_C, "TBP_L1_AFP_C_ALFA_C/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_4_A", &TBP_L1_MBTS_4_A, "TBP_L1_MBTS_4_A/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_4_C", &TBP_L1_MBTS_4_C, "TBP_L1_MBTS_4_C/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_1_BGRP9", &TBP_L1_MBTS_1_BGRP9, "TBP_L1_MBTS_1_BGRP9/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_2_BGRP9", &TBP_L1_MBTS_2_BGRP9, "TBP_L1_MBTS_2_BGRP9/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_1_BGRP11", &TBP_L1_MBTS_1_BGRP11, "TBP_L1_MBTS_1_BGRP11/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_2_BGRP11", &TBP_L1_MBTS_2_BGRP11, "TBP_L1_MBTS_2_BGRP11/O");
    TriggerKeysBP->Branch("TBP_L1_LUCID", &TBP_L1_LUCID, "TBP_L1_LUCID/O");
    TriggerKeysBP->Branch("TBP_L1_LUCID_EMPTY", &TBP_L1_LUCID_EMPTY, "TBP_L1_LUCID_EMPTY/O");
    TriggerKeysBP->Branch("TBP_L1_LUCID_UNPAIRED_ISO", &TBP_L1_LUCID_UNPAIRED_ISO, "TBP_L1_LUCID_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_LUCID_A_C_EMPTY", &TBP_L1_LUCID_A_C_EMPTY, "TBP_L1_LUCID_A_C_EMPTY/O");
    TriggerKeysBP->Branch("TBP_L1_LUCID_A_C_UNPAIRED_ISO", &TBP_L1_LUCID_A_C_UNPAIRED_ISO, "TBP_L1_LUCID_A_C_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_LUCID_A_C_UNPAIRED_NONISO", &TBP_L1_LUCID_A_C_UNPAIRED_NONISO, "TBP_L1_LUCID_A_C_UNPAIRED_NONISO/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_1", &TBP_L1_MBTS_1, "TBP_L1_MBTS_1/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_2", &TBP_L1_MBTS_2, "TBP_L1_MBTS_2/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_1_1", &TBP_L1_MBTS_1_1, "TBP_L1_MBTS_1_1/O");
    TriggerKeysBP->Branch("TBP_L1_ZDC_A", &TBP_L1_ZDC_A, "TBP_L1_ZDC_A/O");
    TriggerKeysBP->Branch("TBP_L1_ZDC_C", &TBP_L1_ZDC_C, "TBP_L1_ZDC_C/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ELAST1", &TBP_L1_ALFA_ELAST1, "TBP_L1_ALFA_ELAST1/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ELAST2", &TBP_L1_ALFA_ELAST2, "TBP_L1_ALFA_ELAST2/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ELAST11", &TBP_L1_ALFA_ELAST11, "TBP_L1_ALFA_ELAST11/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ELAST12", &TBP_L1_ALFA_ELAST12, "TBP_L1_ALFA_ELAST12/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ELAST13", &TBP_L1_ALFA_ELAST13, "TBP_L1_ALFA_ELAST13/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ELAST14", &TBP_L1_ALFA_ELAST14, "TBP_L1_ALFA_ELAST14/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ELAST15", &TBP_L1_ALFA_ELAST15, "TBP_L1_ALFA_ELAST15/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ELAST15_Calib", &TBP_L1_ALFA_ELAST15_Calib, "TBP_L1_ALFA_ELAST15_Calib/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ELAST16", &TBP_L1_ALFA_ELAST16, "TBP_L1_ALFA_ELAST16/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ELAST17", &TBP_L1_ALFA_ELAST17, "TBP_L1_ALFA_ELAST17/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ELAST18", &TBP_L1_ALFA_ELAST18, "TBP_L1_ALFA_ELAST18/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ELAST18_Calib", &TBP_L1_ALFA_ELAST18_Calib, "TBP_L1_ALFA_ELAST18_Calib/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_SDIFF5", &TBP_L1_ALFA_SDIFF5, "TBP_L1_ALFA_SDIFF5/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_SDIFF6", &TBP_L1_ALFA_SDIFF6, "TBP_L1_ALFA_SDIFF6/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_SDIFF7", &TBP_L1_ALFA_SDIFF7, "TBP_L1_ALFA_SDIFF7/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_SDIFF8", &TBP_L1_ALFA_SDIFF8, "TBP_L1_ALFA_SDIFF8/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_1_A_ALFA_C", &TBP_L1_MBTS_1_A_ALFA_C, "TBP_L1_MBTS_1_A_ALFA_C/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_1_C_ALFA_A", &TBP_L1_MBTS_1_C_ALFA_A, "TBP_L1_MBTS_1_C_ALFA_A/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO", &TBP_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO, "TBP_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO", &TBP_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO, "TBP_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_2_A_ALFA_C", &TBP_L1_MBTS_2_A_ALFA_C, "TBP_L1_MBTS_2_A_ALFA_C/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_2_C_ALFA_A", &TBP_L1_MBTS_2_C_ALFA_A, "TBP_L1_MBTS_2_C_ALFA_A/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO", &TBP_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO, "TBP_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO", &TBP_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO, "TBP_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_LUCID_A_ALFA_C", &TBP_L1_LUCID_A_ALFA_C, "TBP_L1_LUCID_A_ALFA_C/O");
    TriggerKeysBP->Branch("TBP_L1_LUCID_C_ALFA_A", &TBP_L1_LUCID_C_ALFA_A, "TBP_L1_LUCID_C_ALFA_A/O");
    TriggerKeysBP->Branch("TBP_L1_LUCID_A_ALFA_C_UNPAIRED_ISO", &TBP_L1_LUCID_A_ALFA_C_UNPAIRED_ISO, "TBP_L1_LUCID_A_ALFA_C_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_LUCID_C_ALFA_A_UNPAIRED_ISO", &TBP_L1_LUCID_C_ALFA_A_UNPAIRED_ISO, "TBP_L1_LUCID_C_ALFA_A_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_EM3_ALFA_ANY", &TBP_L1_EM3_ALFA_ANY, "TBP_L1_EM3_ALFA_ANY/O");
    TriggerKeysBP->Branch("TBP_L1_EM3_ALFA_ANY_UNPAIRED_ISO", &TBP_L1_EM3_ALFA_ANY_UNPAIRED_ISO, "TBP_L1_EM3_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_EM3_ALFA_EINE", &TBP_L1_EM3_ALFA_EINE, "TBP_L1_EM3_ALFA_EINE/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ELASTIC_UNPAIRED_ISO", &TBP_L1_ALFA_ELASTIC_UNPAIRED_ISO, "TBP_L1_ALFA_ELASTIC_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO", &TBP_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO, "TBP_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ANY_A_EMPTY", &TBP_L1_ALFA_ANY_A_EMPTY, "TBP_L1_ALFA_ANY_A_EMPTY/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ANY_C_EMPTY", &TBP_L1_ALFA_ANY_C_EMPTY, "TBP_L1_ALFA_ANY_C_EMPTY/O");
    TriggerKeysBP->Branch("TBP_L1_J12_ALFA_ANY", &TBP_L1_J12_ALFA_ANY, "TBP_L1_J12_ALFA_ANY/O");
    TriggerKeysBP->Branch("TBP_L1_J12_ALFA_ANY_UNPAIRED_ISO", &TBP_L1_J12_ALFA_ANY_UNPAIRED_ISO, "TBP_L1_J12_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_TE5_ALFA_ANY", &TBP_L1_TE5_ALFA_ANY, "TBP_L1_TE5_ALFA_ANY/O");
    TriggerKeysBP->Branch("TBP_L1_TE5_ALFA_ANY_UNPAIRED_ISO", &TBP_L1_TE5_ALFA_ANY_UNPAIRED_ISO, "TBP_L1_TE5_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_TE5_ALFA_EINE", &TBP_L1_TE5_ALFA_EINE, "TBP_L1_TE5_ALFA_EINE/O");
    TriggerKeysBP->Branch("TBP_L1_TRT_ALFA_ANY", &TBP_L1_TRT_ALFA_ANY, "TBP_L1_TRT_ALFA_ANY/O");
    TriggerKeysBP->Branch("TBP_L1_TRT_ALFA_ANY_UNPAIRED_ISO", &TBP_L1_TRT_ALFA_ANY_UNPAIRED_ISO, "TBP_L1_TRT_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_TRT_ALFA_EINE", &TBP_L1_TRT_ALFA_EINE, "TBP_L1_TRT_ALFA_EINE/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_BGT", &TBP_L1_ALFA_BGT, "TBP_L1_ALFA_BGT/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_BGT_UNPAIRED_ISO", &TBP_L1_ALFA_BGT_UNPAIRED_ISO, "TBP_L1_ALFA_BGT_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_BGT_BGRP10", &TBP_L1_ALFA_BGT_BGRP10, "TBP_L1_ALFA_BGT_BGRP10/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_SHOWSYST5", &TBP_L1_ALFA_SHOWSYST5, "TBP_L1_ALFA_SHOWSYST5/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_SYST9", &TBP_L1_ALFA_SYST9, "TBP_L1_ALFA_SYST9/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_SYST10", &TBP_L1_ALFA_SYST10, "TBP_L1_ALFA_SYST10/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_SYST11", &TBP_L1_ALFA_SYST11, "TBP_L1_ALFA_SYST11/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_SYST12", &TBP_L1_ALFA_SYST12, "TBP_L1_ALFA_SYST12/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_SYST17", &TBP_L1_ALFA_SYST17, "TBP_L1_ALFA_SYST17/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_SYST18", &TBP_L1_ALFA_SYST18, "TBP_L1_ALFA_SYST18/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ANY", &TBP_L1_ALFA_ANY, "TBP_L1_ALFA_ANY/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ANY_EMPTY", &TBP_L1_ALFA_ANY_EMPTY, "TBP_L1_ALFA_ANY_EMPTY/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ANY_FIRSTEMPTY", &TBP_L1_ALFA_ANY_FIRSTEMPTY, "TBP_L1_ALFA_ANY_FIRSTEMPTY/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ANY_UNPAIRED_ISO", &TBP_L1_ALFA_ANY_UNPAIRED_ISO, "TBP_L1_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ANY_UNPAIRED_NONISO", &TBP_L1_ALFA_ANY_UNPAIRED_NONISO, "TBP_L1_ALFA_ANY_UNPAIRED_NONISO/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ANY_BGRP10", &TBP_L1_ALFA_ANY_BGRP10, "TBP_L1_ALFA_ANY_BGRP10/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ANY_ABORTGAPNOTCALIB", &TBP_L1_ALFA_ANY_ABORTGAPNOTCALIB, "TBP_L1_ALFA_ANY_ABORTGAPNOTCALIB/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_ANY_CALIB", &TBP_L1_ALFA_ANY_CALIB, "TBP_L1_ALFA_ANY_CALIB/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_B7L1U", &TBP_L1_ALFA_B7L1U, "TBP_L1_ALFA_B7L1U/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_B7L1L", &TBP_L1_ALFA_B7L1L, "TBP_L1_ALFA_B7L1L/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_A7L1U", &TBP_L1_ALFA_A7L1U, "TBP_L1_ALFA_A7L1U/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_A7L1L", &TBP_L1_ALFA_A7L1L, "TBP_L1_ALFA_A7L1L/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_A7R1U", &TBP_L1_ALFA_A7R1U, "TBP_L1_ALFA_A7R1U/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_A7R1L", &TBP_L1_ALFA_A7R1L, "TBP_L1_ALFA_A7R1L/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_B7R1U", &TBP_L1_ALFA_B7R1U, "TBP_L1_ALFA_B7R1U/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_B7R1L", &TBP_L1_ALFA_B7R1L, "TBP_L1_ALFA_B7R1L/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_B7L1U_OD", &TBP_L1_ALFA_B7L1U_OD, "TBP_L1_ALFA_B7L1U_OD/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_B7L1L_OD", &TBP_L1_ALFA_B7L1L_OD, "TBP_L1_ALFA_B7L1L_OD/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_A7L1U_OD", &TBP_L1_ALFA_A7L1U_OD, "TBP_L1_ALFA_A7L1U_OD/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_A7L1L_OD", &TBP_L1_ALFA_A7L1L_OD, "TBP_L1_ALFA_A7L1L_OD/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_A7R1U_OD", &TBP_L1_ALFA_A7R1U_OD, "TBP_L1_ALFA_A7R1U_OD/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_A7R1L_OD", &TBP_L1_ALFA_A7R1L_OD, "TBP_L1_ALFA_A7R1L_OD/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_B7R1U_OD", &TBP_L1_ALFA_B7R1U_OD, "TBP_L1_ALFA_B7R1U_OD/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_B7R1L_OD", &TBP_L1_ALFA_B7R1L_OD, "TBP_L1_ALFA_B7R1L_OD/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_B7L1_OD", &TBP_L1_ALFA_B7L1_OD, "TBP_L1_ALFA_B7L1_OD/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_A7L1_OD", &TBP_L1_ALFA_A7L1_OD, "TBP_L1_ALFA_A7L1_OD/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_B7R1_OD", &TBP_L1_ALFA_B7R1_OD, "TBP_L1_ALFA_B7R1_OD/O");
    TriggerKeysBP->Branch("TBP_L1_ALFA_A7R1_OD", &TBP_L1_ALFA_A7R1_OD, "TBP_L1_ALFA_A7R1_OD/O");

    TriggerKeysAP->Branch("TAP_L1_TE30", &TAP_L1_TE30, "TAP_L1_TE30/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_1_EMPTY", &TAP_L1_MBTS_1_EMPTY, "TAP_L1_MBTS_1_EMPTY/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_1_UNPAIRED_ISO", &TAP_L1_MBTS_1_UNPAIRED_ISO, "TAP_L1_MBTS_1_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_2_EMPTY", &TAP_L1_MBTS_2_EMPTY, "TAP_L1_MBTS_2_EMPTY/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_2_UNPAIRED_ISO", &TAP_L1_MBTS_2_UNPAIRED_ISO, "TAP_L1_MBTS_2_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_1_1_EMPTY", &TAP_L1_MBTS_1_1_EMPTY, "TAP_L1_MBTS_1_1_EMPTY/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_1_1_UNPAIRED_ISO", &TAP_L1_MBTS_1_1_UNPAIRED_ISO, "TAP_L1_MBTS_1_1_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_AFP_C_ALFA_A", &TAP_L1_AFP_C_ALFA_A, "TAP_L1_AFP_C_ALFA_A/O");
    TriggerKeysAP->Branch("TAP_L1_AFP_C_ANY", &TAP_L1_AFP_C_ANY, "TAP_L1_AFP_C_ANY/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_4_A_UNPAIRED_ISO", &TAP_L1_MBTS_4_A_UNPAIRED_ISO, "TAP_L1_MBTS_4_A_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_4_C_UNPAIRED_ISO", &TAP_L1_MBTS_4_C_UNPAIRED_ISO, "TAP_L1_MBTS_4_C_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_AFP_C_ALFA_C", &TAP_L1_AFP_C_ALFA_C, "TAP_L1_AFP_C_ALFA_C/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_4_A", &TAP_L1_MBTS_4_A, "TAP_L1_MBTS_4_A/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_4_C", &TAP_L1_MBTS_4_C, "TAP_L1_MBTS_4_C/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_1_BGRP9", &TAP_L1_MBTS_1_BGRP9, "TAP_L1_MBTS_1_BGRP9/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_2_BGRP9", &TAP_L1_MBTS_2_BGRP9, "TAP_L1_MBTS_2_BGRP9/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_1_BGRP11", &TAP_L1_MBTS_1_BGRP11, "TAP_L1_MBTS_1_BGRP11/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_2_BGRP11", &TAP_L1_MBTS_2_BGRP11, "TAP_L1_MBTS_2_BGRP11/O");
    TriggerKeysAP->Branch("TAP_L1_LUCID", &TAP_L1_LUCID, "TAP_L1_LUCID/O");
    TriggerKeysAP->Branch("TAP_L1_LUCID_EMPTY", &TAP_L1_LUCID_EMPTY, "TAP_L1_LUCID_EMPTY/O");
    TriggerKeysAP->Branch("TAP_L1_LUCID_UNPAIRED_ISO", &TAP_L1_LUCID_UNPAIRED_ISO, "TAP_L1_LUCID_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_LUCID_A_C_EMPTY", &TAP_L1_LUCID_A_C_EMPTY, "TAP_L1_LUCID_A_C_EMPTY/O");
    TriggerKeysAP->Branch("TAP_L1_LUCID_A_C_UNPAIRED_ISO", &TAP_L1_LUCID_A_C_UNPAIRED_ISO, "TAP_L1_LUCID_A_C_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_LUCID_A_C_UNPAIRED_NONISO", &TAP_L1_LUCID_A_C_UNPAIRED_NONISO, "TAP_L1_LUCID_A_C_UNPAIRED_NONISO/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_1", &TAP_L1_MBTS_1, "TAP_L1_MBTS_1/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_2", &TAP_L1_MBTS_2, "TAP_L1_MBTS_2/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_1_1", &TAP_L1_MBTS_1_1, "TAP_L1_MBTS_1_1/O");
    TriggerKeysAP->Branch("TAP_L1_ZDC_A", &TAP_L1_ZDC_A, "TAP_L1_ZDC_A/O");
    TriggerKeysAP->Branch("TAP_L1_ZDC_C", &TAP_L1_ZDC_C, "TAP_L1_ZDC_C/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ELAST1", &TAP_L1_ALFA_ELAST1, "TAP_L1_ALFA_ELAST1/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ELAST2", &TAP_L1_ALFA_ELAST2, "TAP_L1_ALFA_ELAST2/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ELAST11", &TAP_L1_ALFA_ELAST11, "TAP_L1_ALFA_ELAST11/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ELAST12", &TAP_L1_ALFA_ELAST12, "TAP_L1_ALFA_ELAST12/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ELAST13", &TAP_L1_ALFA_ELAST13, "TAP_L1_ALFA_ELAST13/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ELAST14", &TAP_L1_ALFA_ELAST14, "TAP_L1_ALFA_ELAST14/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ELAST15", &TAP_L1_ALFA_ELAST15, "TAP_L1_ALFA_ELAST15/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ELAST15_Calib", &TAP_L1_ALFA_ELAST15_Calib, "TAP_L1_ALFA_ELAST15_Calib/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ELAST16", &TAP_L1_ALFA_ELAST16, "TAP_L1_ALFA_ELAST16/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ELAST17", &TAP_L1_ALFA_ELAST17, "TAP_L1_ALFA_ELAST17/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ELAST18", &TAP_L1_ALFA_ELAST18, "TAP_L1_ALFA_ELAST18/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ELAST18_Calib", &TAP_L1_ALFA_ELAST18_Calib, "TAP_L1_ALFA_ELAST18_Calib/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_SDIFF5", &TAP_L1_ALFA_SDIFF5, "TAP_L1_ALFA_SDIFF5/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_SDIFF6", &TAP_L1_ALFA_SDIFF6, "TAP_L1_ALFA_SDIFF6/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_SDIFF7", &TAP_L1_ALFA_SDIFF7, "TAP_L1_ALFA_SDIFF7/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_SDIFF8", &TAP_L1_ALFA_SDIFF8, "TAP_L1_ALFA_SDIFF8/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_1_A_ALFA_C", &TAP_L1_MBTS_1_A_ALFA_C, "TAP_L1_MBTS_1_A_ALFA_C/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_1_C_ALFA_A", &TAP_L1_MBTS_1_C_ALFA_A, "TAP_L1_MBTS_1_C_ALFA_A/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO", &TAP_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO, "TAP_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO", &TAP_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO, "TAP_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_2_A_ALFA_C", &TAP_L1_MBTS_2_A_ALFA_C, "TAP_L1_MBTS_2_A_ALFA_C/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_2_C_ALFA_A", &TAP_L1_MBTS_2_C_ALFA_A, "TAP_L1_MBTS_2_C_ALFA_A/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO", &TAP_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO, "TAP_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO", &TAP_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO, "TAP_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_LUCID_A_ALFA_C", &TAP_L1_LUCID_A_ALFA_C, "TAP_L1_LUCID_A_ALFA_C/O");
    TriggerKeysAP->Branch("TAP_L1_LUCID_C_ALFA_A", &TAP_L1_LUCID_C_ALFA_A, "TAP_L1_LUCID_C_ALFA_A/O");
    TriggerKeysAP->Branch("TAP_L1_LUCID_A_ALFA_C_UNPAIRED_ISO", &TAP_L1_LUCID_A_ALFA_C_UNPAIRED_ISO, "TAP_L1_LUCID_A_ALFA_C_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_LUCID_C_ALFA_A_UNPAIRED_ISO", &TAP_L1_LUCID_C_ALFA_A_UNPAIRED_ISO, "TAP_L1_LUCID_C_ALFA_A_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_EM3_ALFA_ANY", &TAP_L1_EM3_ALFA_ANY, "TAP_L1_EM3_ALFA_ANY/O");
    TriggerKeysAP->Branch("TAP_L1_EM3_ALFA_ANY_UNPAIRED_ISO", &TAP_L1_EM3_ALFA_ANY_UNPAIRED_ISO, "TAP_L1_EM3_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_EM3_ALFA_EINE", &TAP_L1_EM3_ALFA_EINE, "TAP_L1_EM3_ALFA_EINE/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ELASTIC_UNPAIRED_ISO", &TAP_L1_ALFA_ELASTIC_UNPAIRED_ISO, "TAP_L1_ALFA_ELASTIC_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO", &TAP_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO, "TAP_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ANY_A_EMPTY", &TAP_L1_ALFA_ANY_A_EMPTY, "TAP_L1_ALFA_ANY_A_EMPTY/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ANY_C_EMPTY", &TAP_L1_ALFA_ANY_C_EMPTY, "TAP_L1_ALFA_ANY_C_EMPTY/O");
    TriggerKeysAP->Branch("TAP_L1_J12_ALFA_ANY", &TAP_L1_J12_ALFA_ANY, "TAP_L1_J12_ALFA_ANY/O");
    TriggerKeysAP->Branch("TAP_L1_J12_ALFA_ANY_UNPAIRED_ISO", &TAP_L1_J12_ALFA_ANY_UNPAIRED_ISO, "TAP_L1_J12_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_TE5_ALFA_ANY", &TAP_L1_TE5_ALFA_ANY, "TAP_L1_TE5_ALFA_ANY/O");
    TriggerKeysAP->Branch("TAP_L1_TE5_ALFA_ANY_UNPAIRED_ISO", &TAP_L1_TE5_ALFA_ANY_UNPAIRED_ISO, "TAP_L1_TE5_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_TE5_ALFA_EINE", &TAP_L1_TE5_ALFA_EINE, "TAP_L1_TE5_ALFA_EINE/O");
    TriggerKeysAP->Branch("TAP_L1_TRT_ALFA_ANY", &TAP_L1_TRT_ALFA_ANY, "TAP_L1_TRT_ALFA_ANY/O");
    TriggerKeysAP->Branch("TAP_L1_TRT_ALFA_ANY_UNPAIRED_ISO", &TAP_L1_TRT_ALFA_ANY_UNPAIRED_ISO, "TAP_L1_TRT_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_TRT_ALFA_EINE", &TAP_L1_TRT_ALFA_EINE, "TAP_L1_TRT_ALFA_EINE/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_BGT", &TAP_L1_ALFA_BGT, "TAP_L1_ALFA_BGT/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_BGT_UNPAIRED_ISO", &TAP_L1_ALFA_BGT_UNPAIRED_ISO, "TAP_L1_ALFA_BGT_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_BGT_BGRP10", &TAP_L1_ALFA_BGT_BGRP10, "TAP_L1_ALFA_BGT_BGRP10/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_SHOWSYST5", &TAP_L1_ALFA_SHOWSYST5, "TAP_L1_ALFA_SHOWSYST5/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_SYST9", &TAP_L1_ALFA_SYST9, "TAP_L1_ALFA_SYST9/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_SYST10", &TAP_L1_ALFA_SYST10, "TAP_L1_ALFA_SYST10/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_SYST11", &TAP_L1_ALFA_SYST11, "TAP_L1_ALFA_SYST11/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_SYST12", &TAP_L1_ALFA_SYST12, "TAP_L1_ALFA_SYST12/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_SYST17", &TAP_L1_ALFA_SYST17, "TAP_L1_ALFA_SYST17/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_SYST18", &TAP_L1_ALFA_SYST18, "TAP_L1_ALFA_SYST18/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ANY", &TAP_L1_ALFA_ANY, "TAP_L1_ALFA_ANY/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ANY_EMPTY", &TAP_L1_ALFA_ANY_EMPTY, "TAP_L1_ALFA_ANY_EMPTY/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ANY_FIRSTEMPTY", &TAP_L1_ALFA_ANY_FIRSTEMPTY, "TAP_L1_ALFA_ANY_FIRSTEMPTY/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ANY_UNPAIRED_ISO", &TAP_L1_ALFA_ANY_UNPAIRED_ISO, "TAP_L1_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ANY_UNPAIRED_NONISO", &TAP_L1_ALFA_ANY_UNPAIRED_NONISO, "TAP_L1_ALFA_ANY_UNPAIRED_NONISO/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ANY_BGRP10", &TAP_L1_ALFA_ANY_BGRP10, "TAP_L1_ALFA_ANY_BGRP10/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ANY_ABORTGAPNOTCALIB", &TAP_L1_ALFA_ANY_ABORTGAPNOTCALIB, "TAP_L1_ALFA_ANY_ABORTGAPNOTCALIB/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_ANY_CALIB", &TAP_L1_ALFA_ANY_CALIB, "TAP_L1_ALFA_ANY_CALIB/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_B7L1U", &TAP_L1_ALFA_B7L1U, "TAP_L1_ALFA_B7L1U/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_B7L1L", &TAP_L1_ALFA_B7L1L, "TAP_L1_ALFA_B7L1L/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_A7L1U", &TAP_L1_ALFA_A7L1U, "TAP_L1_ALFA_A7L1U/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_A7L1L", &TAP_L1_ALFA_A7L1L, "TAP_L1_ALFA_A7L1L/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_A7R1U", &TAP_L1_ALFA_A7R1U, "TAP_L1_ALFA_A7R1U/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_A7R1L", &TAP_L1_ALFA_A7R1L, "TAP_L1_ALFA_A7R1L/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_B7R1U", &TAP_L1_ALFA_B7R1U, "TAP_L1_ALFA_B7R1U/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_B7R1L", &TAP_L1_ALFA_B7R1L, "TAP_L1_ALFA_B7R1L/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_B7L1U_OD", &TAP_L1_ALFA_B7L1U_OD, "TAP_L1_ALFA_B7L1U_OD/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_B7L1L_OD", &TAP_L1_ALFA_B7L1L_OD, "TAP_L1_ALFA_B7L1L_OD/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_A7L1U_OD", &TAP_L1_ALFA_A7L1U_OD, "TAP_L1_ALFA_A7L1U_OD/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_A7L1L_OD", &TAP_L1_ALFA_A7L1L_OD, "TAP_L1_ALFA_A7L1L_OD/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_A7R1U_OD", &TAP_L1_ALFA_A7R1U_OD, "TAP_L1_ALFA_A7R1U_OD/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_A7R1L_OD", &TAP_L1_ALFA_A7R1L_OD, "TAP_L1_ALFA_A7R1L_OD/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_B7R1U_OD", &TAP_L1_ALFA_B7R1U_OD, "TAP_L1_ALFA_B7R1U_OD/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_B7R1L_OD", &TAP_L1_ALFA_B7R1L_OD, "TAP_L1_ALFA_B7R1L_OD/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_B7L1_OD", &TAP_L1_ALFA_B7L1_OD, "TAP_L1_ALFA_B7L1_OD/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_A7L1_OD", &TAP_L1_ALFA_A7L1_OD, "TAP_L1_ALFA_A7L1_OD/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_B7R1_OD", &TAP_L1_ALFA_B7R1_OD, "TAP_L1_ALFA_B7R1_OD/O");
    TriggerKeysAP->Branch("TAP_L1_ALFA_A7R1_OD", &TAP_L1_ALFA_A7R1_OD, "TAP_L1_ALFA_A7R1_OD/O");

    TriggerKeysAV->Branch("TAV_L1_TE30", &TAV_L1_TE30, "TAV_L1_TE30/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_1_EMPTY", &TAV_L1_MBTS_1_EMPTY, "TAV_L1_MBTS_1_EMPTY/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_1_UNPAIRED_ISO", &TAV_L1_MBTS_1_UNPAIRED_ISO, "TAV_L1_MBTS_1_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_2_EMPTY", &TAV_L1_MBTS_2_EMPTY, "TAV_L1_MBTS_2_EMPTY/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_2_UNPAIRED_ISO", &TAV_L1_MBTS_2_UNPAIRED_ISO, "TAV_L1_MBTS_2_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_1_1_EMPTY", &TAV_L1_MBTS_1_1_EMPTY, "TAV_L1_MBTS_1_1_EMPTY/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_1_1_UNPAIRED_ISO", &TAV_L1_MBTS_1_1_UNPAIRED_ISO, "TAV_L1_MBTS_1_1_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_AFP_C_ALFA_A", &TAV_L1_AFP_C_ALFA_A, "TAV_L1_AFP_C_ALFA_A/O");
    TriggerKeysAV->Branch("TAV_L1_AFP_C_ANY", &TAV_L1_AFP_C_ANY, "TAV_L1_AFP_C_ANY/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_4_A_UNPAIRED_ISO", &TAV_L1_MBTS_4_A_UNPAIRED_ISO, "TAV_L1_MBTS_4_A_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_4_C_UNPAIRED_ISO", &TAV_L1_MBTS_4_C_UNPAIRED_ISO, "TAV_L1_MBTS_4_C_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_AFP_C_ALFA_C", &TAV_L1_AFP_C_ALFA_C, "TAV_L1_AFP_C_ALFA_C/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_4_A", &TAV_L1_MBTS_4_A, "TAV_L1_MBTS_4_A/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_4_C", &TAV_L1_MBTS_4_C, "TAV_L1_MBTS_4_C/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_1_BGRP9", &TAV_L1_MBTS_1_BGRP9, "TAV_L1_MBTS_1_BGRP9/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_2_BGRP9", &TAV_L1_MBTS_2_BGRP9, "TAV_L1_MBTS_2_BGRP9/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_1_BGRP11", &TAV_L1_MBTS_1_BGRP11, "TAV_L1_MBTS_1_BGRP11/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_2_BGRP11", &TAV_L1_MBTS_2_BGRP11, "TAV_L1_MBTS_2_BGRP11/O");
    TriggerKeysAV->Branch("TAV_L1_LUCID", &TAV_L1_LUCID, "TAV_L1_LUCID/O");
    TriggerKeysAV->Branch("TAV_L1_LUCID_EMPTY", &TAV_L1_LUCID_EMPTY, "TAV_L1_LUCID_EMPTY/O");
    TriggerKeysAV->Branch("TAV_L1_LUCID_UNPAIRED_ISO", &TAV_L1_LUCID_UNPAIRED_ISO, "TAV_L1_LUCID_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_LUCID_A_C_EMPTY", &TAV_L1_LUCID_A_C_EMPTY, "TAV_L1_LUCID_A_C_EMPTY/O");
    TriggerKeysAV->Branch("TAV_L1_LUCID_A_C_UNPAIRED_ISO", &TAV_L1_LUCID_A_C_UNPAIRED_ISO, "TAV_L1_LUCID_A_C_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_LUCID_A_C_UNPAIRED_NONISO", &TAV_L1_LUCID_A_C_UNPAIRED_NONISO, "TAV_L1_LUCID_A_C_UNPAIRED_NONISO/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_1", &TAV_L1_MBTS_1, "TAV_L1_MBTS_1/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_2", &TAV_L1_MBTS_2, "TAV_L1_MBTS_2/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_1_1", &TAV_L1_MBTS_1_1, "TAV_L1_MBTS_1_1/O");
    TriggerKeysAV->Branch("TAV_L1_ZDC_A", &TAV_L1_ZDC_A, "TAV_L1_ZDC_A/O");
    TriggerKeysAV->Branch("TAV_L1_ZDC_C", &TAV_L1_ZDC_C, "TAV_L1_ZDC_C/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ELAST1", &TAV_L1_ALFA_ELAST1, "TAV_L1_ALFA_ELAST1/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ELAST2", &TAV_L1_ALFA_ELAST2, "TAV_L1_ALFA_ELAST2/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ELAST11", &TAV_L1_ALFA_ELAST11, "TAV_L1_ALFA_ELAST11/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ELAST12", &TAV_L1_ALFA_ELAST12, "TAV_L1_ALFA_ELAST12/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ELAST13", &TAV_L1_ALFA_ELAST13, "TAV_L1_ALFA_ELAST13/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ELAST14", &TAV_L1_ALFA_ELAST14, "TAV_L1_ALFA_ELAST14/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ELAST15", &TAV_L1_ALFA_ELAST15, "TAV_L1_ALFA_ELAST15/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ELAST15_Calib", &TAV_L1_ALFA_ELAST15_Calib, "TAV_L1_ALFA_ELAST15_Calib/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ELAST16", &TAV_L1_ALFA_ELAST16, "TAV_L1_ALFA_ELAST16/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ELAST17", &TAV_L1_ALFA_ELAST17, "TAV_L1_ALFA_ELAST17/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ELAST18", &TAV_L1_ALFA_ELAST18, "TAV_L1_ALFA_ELAST18/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ELAST18_Calib", &TAV_L1_ALFA_ELAST18_Calib, "TAV_L1_ALFA_ELAST18_Calib/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_SDIFF5", &TAV_L1_ALFA_SDIFF5, "TAV_L1_ALFA_SDIFF5/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_SDIFF6", &TAV_L1_ALFA_SDIFF6, "TAV_L1_ALFA_SDIFF6/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_SDIFF7", &TAV_L1_ALFA_SDIFF7, "TAV_L1_ALFA_SDIFF7/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_SDIFF8", &TAV_L1_ALFA_SDIFF8, "TAV_L1_ALFA_SDIFF8/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_1_A_ALFA_C", &TAV_L1_MBTS_1_A_ALFA_C, "TAV_L1_MBTS_1_A_ALFA_C/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_1_C_ALFA_A", &TAV_L1_MBTS_1_C_ALFA_A, "TAV_L1_MBTS_1_C_ALFA_A/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO", &TAV_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO, "TAV_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO", &TAV_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO, "TAV_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_2_A_ALFA_C", &TAV_L1_MBTS_2_A_ALFA_C, "TAV_L1_MBTS_2_A_ALFA_C/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_2_C_ALFA_A", &TAV_L1_MBTS_2_C_ALFA_A, "TAV_L1_MBTS_2_C_ALFA_A/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO", &TAV_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO, "TAV_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO", &TAV_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO, "TAV_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_LUCID_A_ALFA_C", &TAV_L1_LUCID_A_ALFA_C, "TAV_L1_LUCID_A_ALFA_C/O");
    TriggerKeysAV->Branch("TAV_L1_LUCID_C_ALFA_A", &TAV_L1_LUCID_C_ALFA_A, "TAV_L1_LUCID_C_ALFA_A/O");
    TriggerKeysAV->Branch("TAV_L1_LUCID_A_ALFA_C_UNPAIRED_ISO", &TAV_L1_LUCID_A_ALFA_C_UNPAIRED_ISO, "TAV_L1_LUCID_A_ALFA_C_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_LUCID_C_ALFA_A_UNPAIRED_ISO", &TAV_L1_LUCID_C_ALFA_A_UNPAIRED_ISO, "TAV_L1_LUCID_C_ALFA_A_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_EM3_ALFA_ANY", &TAV_L1_EM3_ALFA_ANY, "TAV_L1_EM3_ALFA_ANY/O");
    TriggerKeysAV->Branch("TAV_L1_EM3_ALFA_ANY_UNPAIRED_ISO", &TAV_L1_EM3_ALFA_ANY_UNPAIRED_ISO, "TAV_L1_EM3_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_EM3_ALFA_EINE", &TAV_L1_EM3_ALFA_EINE, "TAV_L1_EM3_ALFA_EINE/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ELASTIC_UNPAIRED_ISO", &TAV_L1_ALFA_ELASTIC_UNPAIRED_ISO, "TAV_L1_ALFA_ELASTIC_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO", &TAV_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO, "TAV_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ANY_A_EMPTY", &TAV_L1_ALFA_ANY_A_EMPTY, "TAV_L1_ALFA_ANY_A_EMPTY/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ANY_C_EMPTY", &TAV_L1_ALFA_ANY_C_EMPTY, "TAV_L1_ALFA_ANY_C_EMPTY/O");
    TriggerKeysAV->Branch("TAV_L1_J12_ALFA_ANY", &TAV_L1_J12_ALFA_ANY, "TAV_L1_J12_ALFA_ANY/O");
    TriggerKeysAV->Branch("TAV_L1_J12_ALFA_ANY_UNPAIRED_ISO", &TAV_L1_J12_ALFA_ANY_UNPAIRED_ISO, "TAV_L1_J12_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_TE5_ALFA_ANY", &TAV_L1_TE5_ALFA_ANY, "TAV_L1_TE5_ALFA_ANY/O");
    TriggerKeysAV->Branch("TAV_L1_TE5_ALFA_ANY_UNPAIRED_ISO", &TAV_L1_TE5_ALFA_ANY_UNPAIRED_ISO, "TAV_L1_TE5_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_TE5_ALFA_EINE", &TAV_L1_TE5_ALFA_EINE, "TAV_L1_TE5_ALFA_EINE/O");
    TriggerKeysAV->Branch("TAV_L1_TRT_ALFA_ANY", &TAV_L1_TRT_ALFA_ANY, "TAV_L1_TRT_ALFA_ANY/O");
    TriggerKeysAV->Branch("TAV_L1_TRT_ALFA_ANY_UNPAIRED_ISO", &TAV_L1_TRT_ALFA_ANY_UNPAIRED_ISO, "TAV_L1_TRT_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_TRT_ALFA_EINE", &TAV_L1_TRT_ALFA_EINE, "TAV_L1_TRT_ALFA_EINE/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_BGT", &TAV_L1_ALFA_BGT, "TAV_L1_ALFA_BGT/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_BGT_UNPAIRED_ISO", &TAV_L1_ALFA_BGT_UNPAIRED_ISO, "TAV_L1_ALFA_BGT_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_BGT_BGRP10", &TAV_L1_ALFA_BGT_BGRP10, "TAV_L1_ALFA_BGT_BGRP10/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_SHOWSYST5", &TAV_L1_ALFA_SHOWSYST5, "TAV_L1_ALFA_SHOWSYST5/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_SYST9", &TAV_L1_ALFA_SYST9, "TAV_L1_ALFA_SYST9/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_SYST10", &TAV_L1_ALFA_SYST10, "TAV_L1_ALFA_SYST10/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_SYST11", &TAV_L1_ALFA_SYST11, "TAV_L1_ALFA_SYST11/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_SYST12", &TAV_L1_ALFA_SYST12, "TAV_L1_ALFA_SYST12/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_SYST17", &TAV_L1_ALFA_SYST17, "TAV_L1_ALFA_SYST17/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_SYST18", &TAV_L1_ALFA_SYST18, "TAV_L1_ALFA_SYST18/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ANY", &TAV_L1_ALFA_ANY, "TAV_L1_ALFA_ANY/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ANY_EMPTY", &TAV_L1_ALFA_ANY_EMPTY, "TAV_L1_ALFA_ANY_EMPTY/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ANY_FIRSTEMPTY", &TAV_L1_ALFA_ANY_FIRSTEMPTY, "TAV_L1_ALFA_ANY_FIRSTEMPTY/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ANY_UNPAIRED_ISO", &TAV_L1_ALFA_ANY_UNPAIRED_ISO, "TAV_L1_ALFA_ANY_UNPAIRED_ISO/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ANY_UNPAIRED_NONISO", &TAV_L1_ALFA_ANY_UNPAIRED_NONISO, "TAV_L1_ALFA_ANY_UNPAIRED_NONISO/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ANY_BGRP10", &TAV_L1_ALFA_ANY_BGRP10, "TAV_L1_ALFA_ANY_BGRP10/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ANY_ABORTGAPNOTCALIB", &TAV_L1_ALFA_ANY_ABORTGAPNOTCALIB, "TAV_L1_ALFA_ANY_ABORTGAPNOTCALIB/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_ANY_CALIB", &TAV_L1_ALFA_ANY_CALIB, "TAV_L1_ALFA_ANY_CALIB/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_B7L1U", &TAV_L1_ALFA_B7L1U, "TAV_L1_ALFA_B7L1U/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_B7L1L", &TAV_L1_ALFA_B7L1L, "TAV_L1_ALFA_B7L1L/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_A7L1U", &TAV_L1_ALFA_A7L1U, "TAV_L1_ALFA_A7L1U/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_A7L1L", &TAV_L1_ALFA_A7L1L, "TAV_L1_ALFA_A7L1L/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_A7R1U", &TAV_L1_ALFA_A7R1U, "TAV_L1_ALFA_A7R1U/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_A7R1L", &TAV_L1_ALFA_A7R1L, "TAV_L1_ALFA_A7R1L/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_B7R1U", &TAV_L1_ALFA_B7R1U, "TAV_L1_ALFA_B7R1U/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_B7R1L", &TAV_L1_ALFA_B7R1L, "TAV_L1_ALFA_B7R1L/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_B7L1U_OD", &TAV_L1_ALFA_B7L1U_OD, "TAV_L1_ALFA_B7L1U_OD/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_B7L1L_OD", &TAV_L1_ALFA_B7L1L_OD, "TAV_L1_ALFA_B7L1L_OD/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_A7L1U_OD", &TAV_L1_ALFA_A7L1U_OD, "TAV_L1_ALFA_A7L1U_OD/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_A7L1L_OD", &TAV_L1_ALFA_A7L1L_OD, "TAV_L1_ALFA_A7L1L_OD/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_A7R1U_OD", &TAV_L1_ALFA_A7R1U_OD, "TAV_L1_ALFA_A7R1U_OD/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_A7R1L_OD", &TAV_L1_ALFA_A7R1L_OD, "TAV_L1_ALFA_A7R1L_OD/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_B7R1U_OD", &TAV_L1_ALFA_B7R1U_OD, "TAV_L1_ALFA_B7R1U_OD/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_B7R1L_OD", &TAV_L1_ALFA_B7R1L_OD, "TAV_L1_ALFA_B7R1L_OD/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_B7TL1_OD", &TAV_L1_ALFA_B7L1_OD, "TAV_L1_ALFA_B7L1_OD/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_A7TL1_OD", &TAV_L1_ALFA_A7L1_OD, "TAV_L1_ALFA_A7L1_OD/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_B7R1_OD", &TAV_L1_ALFA_B7R1_OD, "TAV_L1_ALFA_B7R1_OD/O");
    TriggerKeysAV->Branch("TAV_L1_ALFA_A7R1_OD", &TAV_L1_ALFA_A7R1_OD, "TAV_L1_ALFA_A7R1_OD/O");

    //~
    //~
    //~
    TriggerPrescales->Branch("PS_L1_TE30", &PS_L1_TE30, "PS_L1_TE30/F");
    TriggerPrescales->Branch("PS_L1_MBTS_1_EMPTY", &PS_L1_MBTS_1_EMPTY, "PS_L1_MBTS_1_EMPTY/F");
    TriggerPrescales->Branch("PS_L1_MBTS_1_UNPAIRED_ISO", &PS_L1_MBTS_1_UNPAIRED_ISO, "PS_L1_MBTS_1_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_MBTS_2_EMPTY", &PS_L1_MBTS_2_EMPTY, "PS_L1_MBTS_2_EMPTY/F");
    TriggerPrescales->Branch("PS_L1_MBTS_2_UNPAIRED_ISO", &PS_L1_MBTS_2_UNPAIRED_ISO, "PS_L1_MBTS_2_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_MBTS_1_1_EMPTY", &PS_L1_MBTS_1_1_EMPTY, "PS_L1_MBTS_1_1_EMPTY/F");
    TriggerPrescales->Branch("PS_L1_MBTS_1_1_UNPAIRED_ISO", &PS_L1_MBTS_1_1_UNPAIRED_ISO, "PS_L1_MBTS_1_1_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_AFP_C_ALFA_A", &PS_L1_AFP_C_ALFA_A, "PS_L1_AFP_C_ALFA_A/F");
    TriggerPrescales->Branch("PS_L1_AFP_C_ANY", &PS_L1_AFP_C_ANY, "PS_L1_AFP_C_ANY/F");
    TriggerPrescales->Branch("PS_L1_MBTS_4_A_UNPAIRED_ISO", &PS_L1_MBTS_4_A_UNPAIRED_ISO, "PS_L1_MBTS_4_A_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_MBTS_4_C_UNPAIRED_ISO", &PS_L1_MBTS_4_C_UNPAIRED_ISO, "PS_L1_MBTS_4_C_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_AFP_C_ALFA_C", &PS_L1_AFP_C_ALFA_C, "PS_L1_AFP_C_ALFA_C/F");
    TriggerPrescales->Branch("PS_L1_MBTS_4_A", &PS_L1_MBTS_4_A, "PS_L1_MBTS_4_A/F");
    TriggerPrescales->Branch("PS_L1_MBTS_4_C", &PS_L1_MBTS_4_C, "PS_L1_MBTS_4_C/F");
    TriggerPrescales->Branch("PS_L1_MBTS_1_BGRP9", &PS_L1_MBTS_1_BGRP9, "PS_L1_MBTS_1_BGRP9/F");
    TriggerPrescales->Branch("PS_L1_MBTS_2_BGRP9", &PS_L1_MBTS_2_BGRP9, "PS_L1_MBTS_2_BGRP9/F");
    TriggerPrescales->Branch("PS_L1_MBTS_1_BGRP11", &PS_L1_MBTS_1_BGRP11, "PS_L1_MBTS_1_BGRP11/F");
    TriggerPrescales->Branch("PS_L1_MBTS_2_BGRP11", &PS_L1_MBTS_2_BGRP11, "PS_L1_MBTS_2_BGRP11/F");
    TriggerPrescales->Branch("PS_L1_LUCID", &PS_L1_LUCID, "PS_L1_LUCID/F");
    TriggerPrescales->Branch("PS_L1_LUCID_EMPTY", &PS_L1_LUCID_EMPTY, "PS_L1_LUCID_EMPTY/F");
    TriggerPrescales->Branch("PS_L1_LUCID_UNPAIRED_ISO", &PS_L1_LUCID_UNPAIRED_ISO, "PS_L1_LUCID_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_LUCID_A_C_EMPTY", &PS_L1_LUCID_A_C_EMPTY, "PS_L1_LUCID_A_C_EMPTY/F");
    TriggerPrescales->Branch("PS_L1_LUCID_A_C_UNPAIRED_ISO", &PS_L1_LUCID_A_C_UNPAIRED_ISO, "PS_L1_LUCID_A_C_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_LUCID_A_C_UNPAIRED_NONISO", &PS_L1_LUCID_A_C_UNPAIRED_NONISO, "PS_L1_LUCID_A_C_UNPAIRED_NONISO/F");
    TriggerPrescales->Branch("PS_L1_MBTS_1", &PS_L1_MBTS_1, "PS_L1_MBTS_1/F");
    TriggerPrescales->Branch("PS_L1_MBTS_2", &PS_L1_MBTS_2, "PS_L1_MBTS_2/F");
    TriggerPrescales->Branch("PS_L1_MBTS_1_1", &PS_L1_MBTS_1_1, "PS_L1_MBTS_1_1/F");
    TriggerPrescales->Branch("PS_L1_ZDC_A", &PS_L1_ZDC_A, "PS_L1_ZDC_A/F");
    TriggerPrescales->Branch("PS_L1_ZDC_C", &PS_L1_ZDC_C, "PS_L1_ZDC_C/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ELAST1", &PS_L1_ALFA_ELAST1, "PS_L1_ALFA_ELAST1/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ELAST2", &PS_L1_ALFA_ELAST2, "PS_L1_ALFA_ELAST2/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ELAST11", &PS_L1_ALFA_ELAST11, "PS_L1_ALFA_ELAST11/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ELAST12", &PS_L1_ALFA_ELAST12, "PS_L1_ALFA_ELAST12/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ELAST13", &PS_L1_ALFA_ELAST13, "PS_L1_ALFA_ELAST13/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ELAST14", &PS_L1_ALFA_ELAST14, "PS_L1_ALFA_ELAST14/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ELAST15", &PS_L1_ALFA_ELAST15, "PS_L1_ALFA_ELAST15/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ELAST15_Calib", &PS_L1_ALFA_ELAST15_Calib, "PS_L1_ALFA_ELAST15_Calib/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ELAST16", &PS_L1_ALFA_ELAST16, "PS_L1_ALFA_ELAST16/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ELAST17", &PS_L1_ALFA_ELAST17, "PS_L1_ALFA_ELAST17/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ELAST18", &PS_L1_ALFA_ELAST18, "PS_L1_ALFA_ELAST18/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ELAST18_Calib", &PS_L1_ALFA_ELAST18_Calib, "PS_L1_ALFA_ELAST18_Calib/F");
    TriggerPrescales->Branch("PS_L1_ALFA_SDIFF5", &PS_L1_ALFA_SDIFF5, "PS_L1_ALFA_SDIFF5/F");
    TriggerPrescales->Branch("PS_L1_ALFA_SDIFF6", &PS_L1_ALFA_SDIFF6, "PS_L1_ALFA_SDIFF6/F");
    TriggerPrescales->Branch("PS_L1_ALFA_SDIFF7", &PS_L1_ALFA_SDIFF7, "PS_L1_ALFA_SDIFF7/F");
    TriggerPrescales->Branch("PS_L1_ALFA_SDIFF8", &PS_L1_ALFA_SDIFF8, "PS_L1_ALFA_SDIFF8/F");
    TriggerPrescales->Branch("PS_L1_MBTS_1_A_ALFA_C", &PS_L1_MBTS_1_A_ALFA_C, "PS_L1_MBTS_1_A_ALFA_C/F");
    TriggerPrescales->Branch("PS_L1_MBTS_1_C_ALFA_A", &PS_L1_MBTS_1_C_ALFA_A, "PS_L1_MBTS_1_C_ALFA_A/F");
    TriggerPrescales->Branch("PS_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO", &PS_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO, "PS_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO", &PS_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO, "PS_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_MBTS_2_A_ALFA_C", &PS_L1_MBTS_2_A_ALFA_C, "PS_L1_MBTS_2_A_ALFA_C/F");
    TriggerPrescales->Branch("PS_L1_MBTS_2_C_ALFA_A", &PS_L1_MBTS_2_C_ALFA_A, "PS_L1_MBTS_2_C_ALFA_A/F");
    TriggerPrescales->Branch("PS_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO", &PS_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO, "PS_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO", &PS_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO, "PS_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_LUCID_A_ALFA_C", &PS_L1_LUCID_A_ALFA_C, "PS_L1_LUCID_A_ALFA_C/F");
    TriggerPrescales->Branch("PS_L1_LUCID_C_ALFA_A", &PS_L1_LUCID_C_ALFA_A, "PS_L1_LUCID_C_ALFA_A/F");
    TriggerPrescales->Branch("PS_L1_LUCID_A_ALFA_C_UNPAIRED_ISO", &PS_L1_LUCID_A_ALFA_C_UNPAIRED_ISO, "PS_L1_LUCID_A_ALFA_C_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_LUCID_C_ALFA_A_UNPAIRED_ISO", &PS_L1_LUCID_C_ALFA_A_UNPAIRED_ISO, "PS_L1_LUCID_C_ALFA_A_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_EM3_ALFA_ANY", &PS_L1_EM3_ALFA_ANY, "PS_L1_EM3_ALFA_ANY/F");
    TriggerPrescales->Branch("PS_L1_EM3_ALFA_ANY_UNPAIRED_ISO", &PS_L1_EM3_ALFA_ANY_UNPAIRED_ISO, "PS_L1_EM3_ALFA_ANY_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_EM3_ALFA_EINE", &PS_L1_EM3_ALFA_EINE, "PS_L1_EM3_ALFA_EINE/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ELASTIC_UNPAIRED_ISO", &PS_L1_ALFA_ELASTIC_UNPAIRED_ISO, "PS_L1_ALFA_ELASTIC_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO", &PS_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO, "PS_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ANY_A_EMPTY", &PS_L1_ALFA_ANY_A_EMPTY, "PS_L1_ALFA_ANY_A_EMPTY/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ANY_C_EMPTY", &PS_L1_ALFA_ANY_C_EMPTY, "PS_L1_ALFA_ANY_C_EMPTY/F");
    TriggerPrescales->Branch("PS_L1_J12_ALFA_ANY", &PS_L1_J12_ALFA_ANY, "PS_L1_J12_ALFA_ANY/F");
    TriggerPrescales->Branch("PS_L1_J12_ALFA_ANY_UNPAIRED_ISO", &PS_L1_J12_ALFA_ANY_UNPAIRED_ISO, "PS_L1_J12_ALFA_ANY_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_TE5_ALFA_ANY", &PS_L1_TE5_ALFA_ANY, "PS_L1_TE5_ALFA_ANY/F");
    TriggerPrescales->Branch("PS_L1_TE5_ALFA_ANY_UNPAIRED_ISO", &PS_L1_TE5_ALFA_ANY_UNPAIRED_ISO, "PS_L1_TE5_ALFA_ANY_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_TE5_ALFA_EINE", &PS_L1_TE5_ALFA_EINE, "PS_L1_TE5_ALFA_EINE/F");
    TriggerPrescales->Branch("PS_L1_TRT_ALFA_ANY", &PS_L1_TRT_ALFA_ANY, "PS_L1_TRT_ALFA_ANY/F");
    TriggerPrescales->Branch("PS_L1_TRT_ALFA_ANY_UNPAIRED_ISO", &PS_L1_TRT_ALFA_ANY_UNPAIRED_ISO, "PS_L1_TRT_ALFA_ANY_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_TRT_ALFA_EINE", &PS_L1_TRT_ALFA_EINE, "PS_L1_TRT_ALFA_EINE/F");
    TriggerPrescales->Branch("PS_L1_ALFA_BGT", &PS_L1_ALFA_BGT, "PS_L1_ALFA_BGT/F");
    TriggerPrescales->Branch("PS_L1_ALFA_BGT_UNPAIRED_ISO", &PS_L1_ALFA_BGT_UNPAIRED_ISO, "PS_L1_ALFA_BGT_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_ALFA_BGT_BGRP10", &PS_L1_ALFA_BGT_BGRP10, "PS_L1_ALFA_BGT_BGRP10/F");
    TriggerPrescales->Branch("PS_L1_ALFA_SHOWSYST5", &PS_L1_ALFA_SHOWSYST5, "PS_L1_ALFA_SHOWSYST5/F");
    TriggerPrescales->Branch("PS_L1_ALFA_SYST9", &PS_L1_ALFA_SYST9, "PS_L1_ALFA_SYST9/F");
    TriggerPrescales->Branch("PS_L1_ALFA_SYST10", &PS_L1_ALFA_SYST10, "PS_L1_ALFA_SYST10/F");
    TriggerPrescales->Branch("PS_L1_ALFA_SYST11", &PS_L1_ALFA_SYST11, "PS_L1_ALFA_SYST11/F");
    TriggerPrescales->Branch("PS_L1_ALFA_SYST12", &PS_L1_ALFA_SYST12, "PS_L1_ALFA_SYST12/F");
    TriggerPrescales->Branch("PS_L1_ALFA_SYST17", &PS_L1_ALFA_SYST17, "PS_L1_ALFA_SYST17/F");
    TriggerPrescales->Branch("PS_L1_ALFA_SYST18", &PS_L1_ALFA_SYST18, "PS_L1_ALFA_SYST18/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ANY", &PS_L1_ALFA_ANY, "PS_L1_ALFA_ANY/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ANY_EMPTY", &PS_L1_ALFA_ANY_EMPTY, "PS_L1_ALFA_ANY_EMPTY/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ANY_FIRSTEMPTY", &PS_L1_ALFA_ANY_FIRSTEMPTY, "PS_L1_ALFA_ANY_FIRSTEMPTY/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ANY_UNPAIRED_ISO", &PS_L1_ALFA_ANY_UNPAIRED_ISO, "PS_L1_ALFA_ANY_UNPAIRED_ISO/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ANY_UNPAIRED_NONISO", &PS_L1_ALFA_ANY_UNPAIRED_NONISO, "PS_L1_ALFA_ANY_UNPAIRED_NONISO/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ANY_BGRP10", &PS_L1_ALFA_ANY_BGRP10, "PS_L1_ALFA_ANY_BGRP10/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ANY_ABORTGAPNOTCALIB", &PS_L1_ALFA_ANY_ABORTGAPNOTCALIB, "PS_L1_ALFA_ANY_ABORTGAPNOTCALIB/F");
    TriggerPrescales->Branch("PS_L1_ALFA_ANY_CALIB", &PS_L1_ALFA_ANY_CALIB, "PS_L1_ALFA_ANY_CALIB/F");
    TriggerPrescales->Branch("PS_L1_ALFA_B7L1U", &PS_L1_ALFA_B7L1U, "PS_L1_ALFA_B7L1U/F");
    TriggerPrescales->Branch("PS_L1_ALFA_B7L1L", &PS_L1_ALFA_B7L1L, "PS_L1_ALFA_B7L1L/F");
    TriggerPrescales->Branch("PS_L1_ALFA_A7L1U", &PS_L1_ALFA_A7L1U, "PS_L1_ALFA_A7L1U/F");
    TriggerPrescales->Branch("PS_L1_ALFA_A7L1L", &PS_L1_ALFA_A7L1L, "PS_L1_ALFA_A7L1L/F");
    TriggerPrescales->Branch("PS_L1_ALFA_A7R1U", &PS_L1_ALFA_A7R1U, "PS_L1_ALFA_A7R1U/F");
    TriggerPrescales->Branch("PS_L1_ALFA_A7R1L", &PS_L1_ALFA_A7R1L, "PS_L1_ALFA_A7R1L/F");
    TriggerPrescales->Branch("PS_L1_ALFA_B7R1U", &PS_L1_ALFA_B7R1U, "PS_L1_ALFA_B7R1U/F");
    TriggerPrescales->Branch("PS_L1_ALFA_B7R1L", &PS_L1_ALFA_B7R1L, "PS_L1_ALFA_B7R1L/F");
    TriggerPrescales->Branch("PS_L1_ALFA_B7L1U_OD", &PS_L1_ALFA_B7L1U_OD, "PS_L1_ALFA_B7L1U_OD/F");
    TriggerPrescales->Branch("PS_L1_ALFA_B7L1L_OD", &PS_L1_ALFA_B7L1L_OD, "PS_L1_ALFA_B7L1L_OD/F");
    TriggerPrescales->Branch("PS_L1_ALFA_A7L1U_OD", &PS_L1_ALFA_A7L1U_OD, "PS_L1_ALFA_A7L1U_OD/F");
    TriggerPrescales->Branch("PS_L1_ALFA_A7L1L_OD", &PS_L1_ALFA_A7L1L_OD, "PS_L1_ALFA_A7L1L_OD/F");
    TriggerPrescales->Branch("PS_L1_ALFA_A7R1U_OD", &PS_L1_ALFA_A7R1U_OD, "PS_L1_ALFA_A7R1U_OD/F");
    TriggerPrescales->Branch("PS_L1_ALFA_A7R1L_OD", &PS_L1_ALFA_A7R1L_OD, "PS_L1_ALFA_A7R1L_OD/F");
    TriggerPrescales->Branch("PS_L1_ALFA_B7R1U_OD", &PS_L1_ALFA_B7R1U_OD, "PS_L1_ALFA_B7R1U_OD/F");
    TriggerPrescales->Branch("PS_L1_ALFA_B7R1L_OD", &PS_L1_ALFA_B7R1L_OD, "PS_L1_ALFA_B7R1L_OD/F");
    TriggerPrescales->Branch("PS_L1_ALFA_B7L1_OD", &PS_L1_ALFA_B7L1_OD, "PS_L1_ALFA_B7L1_OD/F");
    TriggerPrescales->Branch("PS_L1_ALFA_A7L1_OD", &PS_L1_ALFA_A7L1_OD, "PS_L1_ALFA_A7L1_OD/F");
    TriggerPrescales->Branch("PS_L1_ALFA_B7R1_OD", &PS_L1_ALFA_B7R1_OD, "PS_L1_ALFA_B7R1_OD/F");
    TriggerPrescales->Branch("PS_L1_ALFA_A7R1_OD", &PS_L1_ALFA_A7R1_OD, "PS_L1_ALFA_A7R1_OD/F");

    // Initialize and configure trigger tools
    m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
    EL_RETURN_CHECK("initialize", m_trigConfigTool->initialize());
    ToolHandle<TrigConf::ITrigConfigTool> trigConfigHandle(m_trigConfigTool);
    m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");

    EL_RETURN_CHECK("initialize", m_trigDecisionTool->setProperty("ConfigTool", trigConfigHandle)); // connect the TrigDecisionTool to the ConfigTool
    EL_RETURN_CHECK("initialize", m_trigDecisionTool->setProperty("TrigDecisionKey", "xTrigDecision"));
    EL_RETURN_CHECK("initialize", m_trigDecisionTool->initialize());

    return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis::execute()
{
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.

    ANA_MSG_INFO("in execute");

    // retrieve the eventInfo object from the event store

    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

    // const xAOD::TEvent *event;
    // ANA_CHECK (evtStore()->retrieve (event, "Event"));

    // print out run and event number from retrieved object
    ANA_MSG_INFO("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

    const xAOD::ALFADataContainer *alfaContainer = NULL;
    // evtStore->retrieve (alfaContainer, "ALFADataContainer");
    ANA_CHECK(evtStore()->retrieve(alfaContainer, "ALFADataContainer"));

    if (!(evtStore()->retrieve(alfaContainer, "ALFADataContainer").isSuccess()))
    {
        ANA_MSG_ERROR("Failed to retrieve ALFADataContainer. Exiting.");
        return StatusCode::FAILURE;
    }

    // ANA_CHECK (evtStore()->retrieve( alfaContainer, "ALFAData"));

    const xAOD::ALFAData *alfa = alfaContainer->at(0);
    const xAOD::ALFAData *alfa2 = alfaContainer->at(1);

    const int VARmaxTrackCnt = alfa->maxTrackCnt();
    const std::vector<int> &VARdetectorPartID = alfa->detectorPartID();
    const std::vector<float> &VARxDet = alfa->xDetCS();
    const std::vector<float> &VARyDet = alfa->yDetCS();
    const std::vector<float> &VARoverU = alfa->overU();
    const std::vector<float> &VARoverV = alfa->overV();
    const std::vector<float> &VARoverY = alfa->overY();
    const std::vector<int> &VARnumU = alfa->numU();
    const std::vector<int> &VARnumV = alfa->numV();
    const std::vector<int> &VARnumY = alfa->numY();

    INTNumTrack = VARmaxTrackCnt;

    const Int_t nStations = 8;

    const Int_t trackVectorSize = nStations * VARmaxTrackCnt;
    for (int i = 0; i < trackVectorSize; i++)
    {
        const Int_t potIndex = i / VARmaxTrackCnt;
        const Int_t trackIndex = i % VARmaxTrackCnt;

        INTx_Det[trackIndex][potIndex] = VARxDet[i];
        INTy_Det[trackIndex][potIndex] = VARyDet[i];
        INTOverU[trackIndex][potIndex] = VARoverU[i];
        INTOverV[trackIndex][potIndex] = VARoverV[i];
        INTOverY[trackIndex][potIndex] = VARoverY[i];
        INTNU[trackIndex][potIndex] = VARnumU[i];
        INTNV[trackIndex][potIndex] = VARnumV[i];
        INTNY[trackIndex][potIndex] = VARnumY[i];
        INTDetector[trackIndex][potIndex] = VARdetectorPartID[i];
    }

    const std::vector<int> &VARodFiberHitsPos = alfa2->odFiberHitsPos();
    const std::vector<int> &VARodFiberHitsNeg = alfa2->odFiberHitsNeg();
    const Int_t nODPlates = 3;
    const Int_t nODLayers = 30;
    for (int i = 0; i < nStations * nODPlates * nODLayers; i++)
    {
        const Int_t potIndex = i / (nODPlates * nODLayers);
        const Int_t plateIndex = (i / nODLayers) % nODPlates;
        const Int_t layerIndex = i % nODLayers;

        INTFiberHitsODPos[potIndex][plateIndex][layerIndex] = VARodFiberHitsPos[i];
        INTFiberHitsODNeg[potIndex][plateIndex][layerIndex] = VARodFiberHitsNeg[i];
    }

    const std::vector<int> &VARodMultiplicityPos = alfa2->odMultiplicityPos();
    const std::vector<int> &VARodMultiplicityNeg = alfa2->odMultiplicityNeg();
    for (int i = 0; i < nStations * nODPlates; i++)
    {
        const Int_t potIndex = i / nODPlates;
        const Int_t plateIndex = i % nODPlates;

        INTMultiODPos[potIndex][plateIndex] = VARodMultiplicityPos[i];
        INTMultiODNeg[potIndex][plateIndex] = VARodMultiplicityNeg[i];
    }

    const std::vector<int> &VARmdMultiplicity = alfa2->mdMultiplicity();
    const Int_t nMDPlates = 20;
    for (int i = 0; i < nStations * nMDPlates; i++)
    {
        const Int_t potIndex = i / nMDPlates;
        const Int_t plateIndex = i % nMDPlates;

        INTMultiMD[potIndex][plateIndex] = VARmdMultiplicity[i];
    }

    const std::vector<int> &VARmdFiberHits = alfa2->mdFiberHits();
    const Int_t nMDLayers = 64;
    for (int i = 0; i < nStations * nMDLayers * nMDPlates; i++)
    {
        const Int_t potIndex = i / (nMDPlates * nMDLayers);
        const Int_t plateIndex = (i / nMDLayers) % nMDPlates;
        const Int_t layerIndex = i % nMDLayers;

        INTFiberHitsMD[potIndex][plateIndex][layerIndex] = VARmdFiberHits[i];
    }

    const std::vector<int> &VARscaler = alfa2->scaler();
    for (int i = 0; i < nStations; i++)
        INTScaler[i] = VARscaler[i];

    // is it properly filled in xAODs?
    const std::vector<int> &VARtrigPat = alfa2->trigPat();
    const Int_t nTrigBits = 16;
    for (int i = 0; i < nStations * nTrigBits; i++)
    {
        const Int_t potIndex = i / nTrigBits;
        const Int_t bitIndex = i % nTrigBits;

        INTTrigPat[potIndex][bitIndex] = VARtrigPat[i];
    }

    const std::vector<int> &VARmdFibSel = alfa->mdFibSel(); // not used yet?
    for (int i = 0; i < nStations * VARmaxTrackCnt * nMDPlates; i++)
    {
        const Int_t potIndex = i / (VARmaxTrackCnt * nMDPlates);
        const Int_t trackIndex = (i / nMDPlates) % VARmaxTrackCnt;
        const Int_t plateIndex = i % nMDPlates;

        //~ INTFib_SelMD[potIndex][trackIndex][plateIndex] = VARmdFibSel[i];
        INTFib_SelMD[trackIndex][potIndex][plateIndex] = VARmdFibSel[i];
    }

    const std::vector<int> &VARodFibSel = alfa->odFibSel(); // not used yet?
    for (int i = 0; i < nStations * VARmaxTrackCnt * nODPlates; i++)
    {
        const Int_t potIndex = i / (VARmaxTrackCnt * nODPlates);
        const Int_t trackIndex = (i / nODPlates) % VARmaxTrackCnt;
        const Int_t plateIndex = i % nODPlates;

        INTFib_SelOD[trackIndex][potIndex][plateIndex] = VARodFibSel[i];
    }

    TAP_L1_EM13VH = false;
    TAP_L1_EM15 = false;
    TAP_L1_EM15VH = false;
    TAP_L1_EM18VH = false;
    TAP_L1_EM20VH = false;
    TAP_L1_EM20VHI = false;
    TAP_L1_EM22VHI = false;
    TAP_L1_EM3_EMPTY = false;
    TAP_L1_EM7_EMPTY = false;
    TAP_L1_MU4 = false;
    TAP_L1_MU6 = false;
    TAP_L1_MU10 = false;
    TAP_L1_MU15 = false;
    TAP_L1_MU20 = false;
    TAP_L1_MU4_EMPTY = false;
    TAP_L1_MU4_FIRSTEMPTY = false;
    TAP_L1_MU11_EMPTY = false;
    TAP_L1_MU4_UNPAIRED_ISO = false;
    TAP_L1_2EM3 = false;
    TAP_L1_2EM7 = false;
    TAP_L1_2EM10VH = false;
    TAP_L1_2EM13VH = false;
    TAP_L1_2EM15 = false;
    TAP_L1_2EM15VH = false;
    TAP_L1_EM7_2EM3 = false;
    TAP_L1_EM12_2EM3 = false;
    TAP_L1_EM15VH_3EM7 = false;
    TAP_L1_2MU4 = false;
    TAP_L1_2MU6 = false;
    TAP_L1_2MU10 = false;
    TAP_L1_2MU20_OVERLAY = false;
    TAP_L1_MU10_2MU6 = false;
    TAP_L1_MU11_2MU6 = false;
    TAP_L1_3MU4 = false;
    TAP_L1_MU6_2MU4 = false;
    TAP_L1_3MU6 = false;
    TAP_L1_4J15_0ETA25 = false;
    TAP_L1_EM15I_MU4 = false;
    TAP_L1_2EM8VH_MU10 = false;
    TAP_L1_EM15VH_MU10 = false;
    TAP_L1_TAU12 = false;
    TAP_L1_TAU12IL = false;
    TAP_L1_TAU12IM = false;
    TAP_L1_TAU12IT = false;
    TAP_L1_TAU20 = false;
    TAP_L1_TAU20IL = false;
    TAP_L1_TAU20IM = false;
    TAP_L1_TAU20IT = false;
    TAP_L1_TAU30 = false;
    TAP_L1_TAU40 = false;
    TAP_L1_TAU60 = false;
    TAP_L1_TAU8 = false;
    TAP_L1_TAU8_EMPTY = false;
    TAP_L1_TAU20IM_2TAU12IM = false;
    TAP_L1_TAU20_2TAU12 = false;
    TAP_L1_EM15HI_2TAU12IM = false;
    TAP_L1_EM15HI_2TAU12IM_J25_3J12 = false;
    TAP_L1_EM15HI_TAU40_2TAU15 = false;
    TAP_L1_MU10_TAU12IM = false;
    TAP_L1_MU10_TAU12IM_J25_2J12 = false;
    TAP_L1_EM7_MU10 = false;
    TAP_L1_MU6_EMPTY = false;
    TAP_L1_MU10_TAU20IM = false;
    TAP_L1_XE10 = false;
    TAP_L1_TAU20IL_2TAU12IL_J25_2J20_3J12 = false;
    TAP_L1_TAU20IM_2TAU12IM_J25_2J20_3J12 = false;
    TAP_L1_J25_2J20_3J12_BOX_TAU20ITAU12I = false;
    TAP_L1_DR_MU10TAU12I_TAU12I_J25 = false;
    TAP_L1_MU10_TAU12I_J25 = false;
    TAP_L1_TAU20IM_2J20_XE45 = false;
    TAP_L1_J15_31ETA49_UNPAIRED_ISO = false;
    TAP_L1_XE20 = false;
    TAP_L1_XE45_TAU20_J20 = false;
    TAP_L1_EM15HI_2TAU12IM_XE35 = false;
    TAP_L1_XE35_EM15_TAU12I = false;
    TAP_L1_XE40_EM15_TAU12I = false;
    TAP_L1_MU10_TAU12IM_XE35 = false;
    TAP_L1_XE25 = false;
    TAP_L1_TAU20IM_2TAU12IM_XE35 = false;
    TAP_L1_TAU20_2TAU12_XE35 = false;
    TAP_L1_XE30 = false;
    TAP_L1_EM15VH_JJ15_23ETA49 = false;
    TAP_L1_MU4_J12 = false;
    TAP_L1_MU6_J20 = false;
    TAP_L1_MU6_J40 = false;
    TAP_L1_MU6_J75 = false;
    TAP_L1_J12 = false;
    TAP_L1_J15 = false;
    TAP_L1_J20 = false;
    TAP_L1_J25 = false;
    TAP_L1_J30 = false;
    TAP_L1_J40 = false;
    TAP_L1_J50 = false;
    TAP_L1_J75 = false;
    TAP_L1_J85 = false;
    TAP_L1_J100 = false;
    TAP_L1_J120 = false;
    TAP_L1_J400 = false;
    TAP_L1_J20_31ETA49 = false;
    TAP_L1_J30_31ETA49 = false;
    TAP_L1_J50_31ETA49 = false;
    TAP_L1_J75_31ETA49 = false;
    TAP_L1_J100_31ETA49 = false;
    TAP_L1_XE65 = false;
    TAP_L1_J15_31ETA49 = false;
    TAP_L1_J20_28ETA31 = false;
    TAP_L1_J12_EMPTY = false;
    TAP_L1_J12_FIRSTEMPTY = false;
    TAP_L1_J12_UNPAIRED_ISO = false;
    TAP_L1_J12_UNPAIRED_NONISO = false;
    TAP_L1_J12_ABORTGAPNOTCALIB = false;
    TAP_L1_J30_EMPTY = false;
    TAP_L1_J30_FIRSTEMPTY = false;
    TAP_L1_J30_31ETA49_EMPTY = false;
    TAP_L1_J30_31ETA49_UNPAIRED_ISO = false;
    TAP_L1_J30_31ETA49_UNPAIRED_NONISO = false;
    TAP_L1_J50_UNPAIRED_ISO = false;
    TAP_L1_J50_UNPAIRED_NONISO = false;
    TAP_L1_J50_ABORTGAPNOTCALIB = false;
    TAP_L1_AFP_NSC = false;
    TAP_L1_J20_J20_31ETA49 = false;
    TAP_L1_3J15 = false;
    TAP_L1_3J20 = false;
    TAP_L1_3J40 = false;
    TAP_L1_3J15_0ETA25 = false;
    TAP_L1_3J50 = false;
    TAP_L1_4J15 = false;
    TAP_L1_4J20 = false;
    TAP_L1_J75_XE50 = false;
    TAP_L1_XE75 = false;
    TAP_L1_6J15 = false;
    TAP_L1_J75_3J20 = false;
    TAP_L1_J30_0ETA49_2J20_0ETA49 = false;
    TAP_L1_TE10 = false;
    TAP_L1_AFP_FSC = false;
    TAP_L1_5J15_0ETA25 = false;
    TAP_L1_2J15_XE55 = false;
    TAP_L1_J40_XE50 = false;
    TAP_L1_J75_XE40 = false;
    TAP_L1_XE35 = false;
    TAP_L1_XE40 = false;
    TAP_L1_XE45 = false;
    TAP_L1_XE50 = false;
    TAP_L1_XE55 = false;
    TAP_L1_XE60 = false;
    TAP_L1_XE70 = false;
    TAP_L1_XE80 = false;
    TAP_L1_XS20 = false;
    TAP_L1_XS30 = false;
    TAP_L1_EM12_XS20 = false;
    TAP_L1_EM15_XS30 = false;
    TAP_L1_XE150 = false;
    TAP_L1_TE30 = false;
    TAP_L1_TE40 = false;
    TAP_L1_TE70 = false;
    TAP_L1_TE30_0ETA24 = false;
    TAP_L1_BCM_Wide_UNPAIRED_NONISO = false;
    TAP_L1_BCM_AC_CA_UNPAIRED_ISO = false;
    TAP_L1_BCM_AC_UNPAIRED_ISO = false;
    TAP_L1_MBTS_1_EMPTY = false;
    TAP_L1_MBTS_1_UNPAIRED_ISO = false;
    TAP_L1_MBTS_2_EMPTY = false;
    TAP_L1_MBTS_2_UNPAIRED_ISO = false;
    TAP_L1_MBTS_1_1_EMPTY = false;
    TAP_L1_MBTS_1_1_UNPAIRED_ISO = false;
    TAP_L1_AFP_C_ALFA_A = false;
    TAP_L1_AFP_C_ANY = false;
    TAP_L1_EM13VH_3J20 = false;
    TAP_L1_MU10_3J20 = false;
    TAP_L1_AFP_C_ANY_MBTS_A = false;
    TAP_L1_2J50_XE40 = false;
    TAP_L1_J40_XE60 = false;
    TAP_L1_J40_0ETA25_XE50 = false;
    TAP_L1_BPH_2M8_2MU4 = false;
    TAP_L1_BPH_8M15_MU6MU4 = false;
    TAP_L1_BPH_8M15_2MU6 = false;
    TAP_L1_J40_0ETA25_2J15_31ETA49 = false;
    TAP_L1_J40_0ETA25_2J25_J20_31ETA49 = false;
    TAP_L1_AFP_C_MBTS_A = false;
    TAP_L1_TGC_BURST_EMPTY = false;
    TAP_L1_MBTS_4_A_UNPAIRED_ISO = false;
    TAP_L1_MBTS_4_C_UNPAIRED_ISO = false;
    TAP_L1_XE300 = false;
    TAP_L1_RD1_BGRP10 = false;
    TAP_L1_AFP_C_ZDC_C = false;
    TAP_L1_AFP_C_J12 = false;
    TAP_L1_AFP_C_EM3 = false;
    TAP_L1_AFP_C_TE5 = false;
    TAP_L1_AFP_C_ALFA_C = false;
    TAP_L1_MBTS_4_A = false;
    TAP_L1_MBTS_4_C = false;
    TAP_L1_MBTS_1_BGRP9 = false;
    TAP_L1_MBTS_2_BGRP9 = false;
    TAP_L1_MBTS_1_BGRP11 = false;
    TAP_L1_MBTS_2_BGRP11 = false;
    TAP_L1_RD0_FILLED = false;
    TAP_L1_RD0_UNPAIRED_ISO = false;
    TAP_L1_RD0_EMPTY = false;
    TAP_L1_RD1_FILLED = false;
    TAP_L1_RD1_EMPTY = false;
    TAP_L1_RD2_FILLED = false;
    TAP_L1_RD2_EMPTY = false;
    TAP_L1_RD3_FILLED = false;
    TAP_L1_RD3_EMPTY = false;
    TAP_L1_RD0_FIRSTEMPTY = false;
    TAP_L1_RD0_BGRP9 = false;
    TAP_L1_RD0_BGRP11 = false;
    TAP_L1_LUCID = false;
    TAP_L1_LUCID_EMPTY = false;
    TAP_L1_LUCID_UNPAIRED_ISO = false;
    TAP_L1_LUCID_A_C_EMPTY = false;
    TAP_L1_LUCID_A_C_UNPAIRED_ISO = false;
    TAP_L1_LUCID_A_C_UNPAIRED_NONISO = false;
    TAP_L1_TRT_FILLED = false;
    TAP_L1_TRT_EMPTY = false;
    TAP_L1_TGC_BURST = false;
    TAP_L1_LHCF = false;
    TAP_L1_BCM_Wide_BGRP0 = false;
    TAP_L1_BCM_AC_CA_BGRP0 = false;
    TAP_L1_BCM_Wide_EMPTY = false;
    TAP_L1_BCM_Wide_UNPAIRED_ISO = false;
    TAP_L1_MBTS_1 = false;
    TAP_L1_MBTS_2 = false;
    TAP_L1_MBTS_1_1 = false;
    TAP_L1_BCM_CA_UNPAIRED_ISO = false;
    TAP_L1_BCM_AC_UNPAIRED_NONISO = false;
    TAP_L1_BCM_CA_UNPAIRED_NONISO = false;
    TAP_L1_BCM_AC_ABORTGAPNOTCALIB = false;
    TAP_L1_BCM_CA_ABORTGAPNOTCALIB = false;
    TAP_L1_BCM_Wide_ABORTGAPNOTCALIB = false;
    TAP_L1_BCM_AC_CALIB = false;
    TAP_L1_BCM_CA_CALIB = false;
    TAP_L1_BCM_Wide_CALIB = false;
    TAP_L1_BTAG_MU4J15 = false;
    TAP_L1_BTAG_MU4J30 = false;
    TAP_L1_ZB = false;
    TAP_L1_BPTX0_BGRP0 = false;
    TAP_L1_BPTX1_BGRP0 = false;
    TAP_L1_BTAG_MU6J20 = false;
    TAP_L1_BTAG_MU6J25 = false;
    TAP_L1_AFP_C_ANY_UNPAIRED_ISO = false;
    TAP_L1_3J15_BTAG_MU4J15 = false;
    TAP_L1_3J15_BTAG_MU4J30 = false;
    TAP_L1_3J15_BTAG_MU6J25 = false;
    TAP_L1_3J20_BTAG_MU4J20 = false;
    TAP_L1_J40_DPHI_Js2XE50 = false;
    TAP_L1_J40_DPHI_J20s2XE50 = false;
    TAP_L1_J40_DPHI_J20XE50 = false;
    TAP_L1_J40_DPHI_CJ20XE50 = false;
    TAP_L1_TAU40_2TAU20IM = false;
    TAP_L1_MU10_2J15_J20 = false;
    TAP_L1_MU11 = false;
    TAP_L1_AFP_C_ANY_UNPAIRED_NONISO = false;
    TAP_L1_HT190_J15_ETA21 = false;
    TAP_L1_HT190_J15s5_ETA21 = false;
    TAP_L1_HT150_J20_ETA31 = false;
    TAP_L1_HT150_J20s5_ETA31 = false;
    TAP_L1_JPSI_1M5 = false;
    TAP_L1_JPSI_1M5_EM7 = false;
    TAP_L1_JPSI_1M5_EM12 = false;
    TAP_L1_KF_XE35 = false;
    TAP_L1_KF_XE45 = false;
    TAP_L1_KF_XE55 = false;
    TAP_L1_KF_XE60 = false;
    TAP_L1_KF_XE65 = false;
    TAP_L1_KF_XE75 = false;
    TAP_L1_AFP_C_ANY_EMPTY = false;
    TAP_L1_AFP_C_ANY_FIRSTEMPTY = false;
    TAP_L1_AFP_C_AND = false;
    TAP_L1_EM12_W_MT25 = false;
    TAP_L1_EM12_W_MT30 = false;
    TAP_L1_EM15_W_MT35_W_250RO2_XEHT_0_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    TAP_L1_W_05RO_XEHT_0 = false;
    TAP_L1_MU10_2J20 = false;
    TAP_L1_W_90RO2_XEHT_0 = false;
    TAP_L1_W_250RO2_XEHT_0 = false;
    TAP_L1_W_HT20_JJ15_ETA49 = false;
    TAP_L1_W_NOMATCH = false;
    TAP_L1_W_NOMATCH_W_05RO_XEEMHT = false;
    TAP_L1_EM15_W_MT35_XS60_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    TAP_L1_EM15_W_MT35_XS40_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    TAP_L1_EM15_W_MT35 = false;
    TAP_L1_EM15_W_MT35_XS60 = false;
    TAP_L1_EM15VH_W_MT35_XS60 = false;
    TAP_L1_EM20VH_W_MT35_XS60 = false;
    TAP_L1_EM22VHI_W_MT35_XS40 = false;
    TAP_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE_XS30 = false;
    TAP_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    TAP_L1_HT150_JJ15_ETA49_MJJ_400 = false;
    TAP_L1_RD2_BGRP14 = false;
    TAP_L1_BPH_1M19_2MU4_BPH_0DR34_2MU4 = false;
    TAP_L1_RD3_BGRP15 = false;
    TAP_L1_BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4 = false;
    TAP_L1_BPH_2M9_2MU6_BPH_2DR15_2MU6 = false;
    TAP_L1_MU6MU4_BO = false;
    TAP_L1_2MU4_B = false;
    TAP_L1_2MU6_B = false;
    TAP_L1_BPH_1M19_2MU4_B_BPH_0DR34_2MU4 = false;
    TAP_L1_BPH_1M19_2MU4_BO_BPH_0DR34_2MU4 = false;
    TAP_L1_BPH_2M8_MU6MU4_B_BPH_0DR15_MU6MU4 = false;
    TAP_L1_2MU4_BO = false;
    TAP_L1_2MU6_BO = false;
    TAP_L1_MU6_2MU4_B = false;
    TAP_L1_DY_DR_2MU4 = false;
    TAP_L1_DY_BOX_2MU4 = false;
    TAP_L1_DY_BOX_MU6MU4 = false;
    TAP_L1_DY_BOX_2MU6 = false;
    TAP_L1_LFV_MU = false;
    TAP_L1_LFV_EM8I = false;
    TAP_L1_LFV_EM15I = false;
    TAP_L1_DPHI_Js2XE50 = false;
    TAP_L1_DPHI_J20s2XE50 = false;
    TAP_L1_DPHI_J20XE50 = false;
    TAP_L1_DPHI_CJ20XE50 = false;
    TAP_L1_MJJ_900 = false;
    TAP_L1_MJJ_800 = false;
    TAP_L1_MJJ_700 = false;
    TAP_L1_MJJ_400 = false;
    TAP_L1_MJJ_100 = false;
    TAP_L1_HT150_JJ15_ETA49 = false;
    TAP_L1_DETA_JJ = false;
    TAP_L1_J4_MATCH = false;
    TAP_L1_LLP_NOMATCH = false;
    TAP_L1_DR_MU10TAU12I = false;
    TAP_L1_TAU30_EMPTY = false;
    TAP_L1_EM15_TAU40 = false;
    TAP_L1_TAU30_UNPAIRED_ISO = false;
    TAP_L1_EM15_TAU12I = false;
    TAP_L1_EM15TAU12I_J25 = false;
    TAP_L1_DR_EM15TAU12I_J25 = false;
    TAP_L1_TAU20ITAU12I_J25 = false;
    TAP_L1_DR_TAU20ITAU12I = false;
    TAP_L1_BOX_TAU20ITAU12I = false;
    TAP_L1_DR_TAU20ITAU12I_J25 = false;
    TAP_L1_LAR_EM = false;
    TAP_L1_LAR_J = false;
    TAP_L1_MU6_MJJ_200 = false;
    TAP_L1_MU6_MJJ_300 = false;
    TAP_L1_MU6_MJJ_400 = false;
    TAP_L1_MU6_MJJ_500 = false;
    TAP_L1_J30_2J20_4J20_0ETA49_MJJ_400 = false;
    TAP_L1_J30_2J20_4J20_0ETA49_MJJ_700 = false;
    TAP_L1_J30_2J20_4J20_0ETA49_MJJ_800 = false;
    TAP_L1_J30_2J20_4J20_0ETA49_MJJ_900 = false;
    TAP_L1_3J20_4J20_0ETA49_MJJ_400 = false;
    TAP_L1_3J20_4J20_0ETA49_MJJ_700 = false;
    TAP_L1_3J20_4J20_0ETA49_MJJ_800 = false;
    TAP_L1_3J20_4J20_0ETA49_MJJ_900 = false;
    TAP_L1_XE35_MJJ_200 = false;
    TAP_L1_EM7_FIRSTEMPTY = false;
    TAP_L1_RD0_ABORTGAPNOTCALIB = false;
    TAP_L1_3J25_0ETA23 = false;
    TAP_L1_TE20 = false;
    TAP_L1_TE10_0ETA24 = false;
    TAP_L1_TE20_0ETA24 = false;
    TAP_L1_XS40 = false;
    TAP_L1_XS50 = false;
    TAP_L1_XS60 = false;
    TAP_L1_J12_BGRP12 = false;
    TAP_L1_J30_31ETA49_BGRP12 = false;
    TAP_L1_MU6_J30_0ETA49_2J20_0ETA49 = false;
    TAP_L1_4J20_0ETA49 = false;
    TAP_L1_TAU8_UNPAIRED_ISO = false;
    TAP_L1_EM7_UNPAIRED_ISO = false;
    TAP_L1_RD2_BGRP12 = false;
    TAP_L1_TAU8_FIRSTEMPTY = false;
    TAP_L1_EM24VHI = false;
    TAP_L1_LHCF_UNPAIRED_ISO = false;
    TAP_L1_LHCF_EMPTY = false;
    TAP_L1_EM15VH_2EM10VH_3EM7 = false;
    TAP_L1_EM15VH_3EM8VH = false;
    TAP_L1_EM8I_MU10 = false;
    TAP_L1_EM15HI = false;
    TAP_L1_MU6_3MU4 = false;
    TAP_L1_2MU6_3MU4 = false;
    TAP_L1_BGRP9 = false;
    TAP_L1_TE50 = false;
    TAP_L1_TE5 = false;
    TAP_L1_TE60 = false;
    TAP_L1_TE50_0ETA24 = false;
    TAP_L1_TE5_0ETA24 = false;
    TAP_L1_EM20VH_FIRSTEMPTY = false;
    TAP_L1_EM22VHI_FIRSTEMPTY = false;
    TAP_L1_MU20_FIRSTEMPTY = false;
    TAP_L1_J100_FIRSTEMPTY = false;
    TAP_L1_J100_31ETA49_FIRSTEMPTY = false;
    TAP_L1_TE60_0ETA24 = false;
    TAP_L1_TE70_0ETA24 = false;
    TAP_L1_TE40_0ETA24 = false;
    TAP_L1_ZDC_A = false;
    TAP_L1_ZDC_C = false;
    TAP_L1_ZDC_AND = false;
    TAP_L1_ZDC_A_C = false;
    TAP_L1_ALFA_ELAST1 = false;
    TAP_L1_ALFA_ELAST2 = false;
    TAP_L1_ALFA_ELAST11 = false;
    TAP_L1_ALFA_ELAST12 = false;
    TAP_L1_ALFA_ELAST13 = false;
    TAP_L1_ALFA_ELAST14 = false;
    TAP_L1_ALFA_ELAST15 = false;
    TAP_L1_ALFA_ELAST15_Calib = false;
    TAP_L1_ALFA_ELAST16 = false;
    TAP_L1_ALFA_ELAST17 = false;
    TAP_L1_ALFA_ELAST18 = false;
    TAP_L1_ALFA_ELAST18_Calib = false;
    TAP_L1_ALFA_SDIFF5 = false;
    TAP_L1_ALFA_SDIFF6 = false;
    TAP_L1_ALFA_SDIFF7 = false;
    TAP_L1_ALFA_SDIFF8 = false;
    TAP_L1_MBTS_1_A_ALFA_C = false;
    TAP_L1_MBTS_1_C_ALFA_A = false;
    TAP_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO = false;
    TAP_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO = false;
    TAP_L1_MBTS_2_A_ALFA_C = false;
    TAP_L1_MBTS_2_C_ALFA_A = false;
    TAP_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO = false;
    TAP_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO = false;
    TAP_L1_LUCID_A_ALFA_C = false;
    TAP_L1_LUCID_C_ALFA_A = false;
    TAP_L1_LUCID_A_ALFA_C_UNPAIRED_ISO = false;
    TAP_L1_LUCID_C_ALFA_A_UNPAIRED_ISO = false;
    TAP_L1_EM3_ALFA_ANY = false;
    TAP_L1_EM3_ALFA_ANY_UNPAIRED_ISO = false;
    TAP_L1_EM3_ALFA_EINE = false;
    TAP_L1_ALFA_ELASTIC_UNPAIRED_ISO = false;
    TAP_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO = false;
    TAP_L1_ALFA_ANY_A_EMPTY = false;
    TAP_L1_ALFA_ANY_C_EMPTY = false;
    TAP_L1_J12_ALFA_ANY = false;
    TAP_L1_J12_ALFA_ANY_UNPAIRED_ISO = false;
    TAP_L1_TE5_ALFA_ANY = false;
    TAP_L1_TE5_ALFA_ANY_UNPAIRED_ISO = false;
    TAP_L1_TE5_ALFA_EINE = false;
    TAP_L1_TRT_ALFA_ANY = false;
    TAP_L1_TRT_ALFA_ANY_UNPAIRED_ISO = false;
    TAP_L1_TRT_ALFA_EINE = false;
    TAP_L1_TAU8_UNPAIRED_NONISO = false;
    TAP_L1_EM7_UNPAIRED_NONISO = false;
    TAP_L1_MU4_UNPAIRED_NONISO = false;
    TAP_L1_ALFA_BGT = false;
    TAP_L1_ALFA_BGT_UNPAIRED_ISO = false;
    TAP_L1_ALFA_BGT_BGRP10 = false;
    TAP_L1_ALFA_SHOWSYST5 = false;
    TAP_L1_ALFA_SYST9 = false;
    TAP_L1_ALFA_SYST10 = false;
    TAP_L1_ALFA_SYST11 = false;
    TAP_L1_ALFA_SYST12 = false;
    TAP_L1_ALFA_SYST17 = false;
    TAP_L1_ALFA_SYST18 = false;
    TAP_L1_ALFA_ANY = false;
    TAP_L1_ALFA_ANY_EMPTY = false;
    TAP_L1_ALFA_ANY_FIRSTEMPTY = false;
    TAP_L1_ALFA_ANY_UNPAIRED_ISO = false;
    TAP_L1_ALFA_ANY_UNPAIRED_NONISO = false;
    TAP_L1_ALFA_ANY_BGRP10 = false;
    TAP_L1_ALFA_ANY_ABORTGAPNOTCALIB = false;
    TAP_L1_ALFA_ANY_CALIB = false;
    TAP_L1_ALFA_B7L1U = false;
    TAP_L1_ALFA_B7L1L = false;
    TAP_L1_ALFA_A7L1U = false;
    TAP_L1_ALFA_A7L1L = false;
    TAP_L1_ALFA_A7R1U = false;
    TAP_L1_ALFA_A7R1L = false;
    TAP_L1_ALFA_B7R1U = false;
    TAP_L1_ALFA_B7R1L = false;
    TAP_L1_ALFA_B7L1U_OD = false;
    TAP_L1_ALFA_B7L1L_OD = false;
    TAP_L1_ALFA_A7L1U_OD = false;
    TAP_L1_ALFA_A7L1L_OD = false;
    TAP_L1_ALFA_A7R1U_OD = false;
    TAP_L1_ALFA_A7R1L_OD = false;
    TAP_L1_ALFA_B7R1U_OD = false;
    TAP_L1_ALFA_B7R1L_OD = false;
    TAP_L1_ALFA_B7L1_OD = false;
    TAP_L1_ALFA_A7L1_OD = false;
    TAP_L1_ALFA_B7R1_OD = false;
    TAP_L1_ALFA_A7R1_OD = false;
    TAP_L1_CALREQ2 = false;

    TBP_L1_EM13VH = false;
    TBP_L1_EM15 = false;
    TBP_L1_EM15VH = false;
    TBP_L1_EM18VH = false;
    TBP_L1_EM20VH = false;
    TBP_L1_EM20VHI = false;
    TBP_L1_EM22VHI = false;
    TBP_L1_EM3_EMPTY = false;
    TBP_L1_EM7_EMPTY = false;
    TBP_L1_MU4 = false;
    TBP_L1_MU6 = false;
    TBP_L1_MU10 = false;
    TBP_L1_MU15 = false;
    TBP_L1_MU20 = false;
    TBP_L1_MU4_EMPTY = false;
    TBP_L1_MU4_FIRSTEMPTY = false;
    TBP_L1_MU11_EMPTY = false;
    TBP_L1_MU4_UNPAIRED_ISO = false;
    TBP_L1_2EM3 = false;
    TBP_L1_2EM7 = false;
    TBP_L1_2EM10VH = false;
    TBP_L1_2EM13VH = false;
    TBP_L1_2EM15 = false;
    TBP_L1_2EM15VH = false;
    TBP_L1_EM7_2EM3 = false;
    TBP_L1_EM12_2EM3 = false;
    TBP_L1_EM15VH_3EM7 = false;
    TBP_L1_2MU4 = false;
    TBP_L1_2MU6 = false;
    TBP_L1_2MU10 = false;
    TBP_L1_2MU20_OVERLAY = false;
    TBP_L1_MU10_2MU6 = false;
    TBP_L1_MU11_2MU6 = false;
    TBP_L1_3MU4 = false;
    TBP_L1_MU6_2MU4 = false;
    TBP_L1_3MU6 = false;
    TBP_L1_4J15_0ETA25 = false;
    TBP_L1_EM15I_MU4 = false;
    TBP_L1_2EM8VH_MU10 = false;
    TBP_L1_EM15VH_MU10 = false;
    TBP_L1_TAU12 = false;
    TBP_L1_TAU12IL = false;
    TBP_L1_TAU12IM = false;
    TBP_L1_TAU12IT = false;
    TBP_L1_TAU20 = false;
    TBP_L1_TAU20IL = false;
    TBP_L1_TAU20IM = false;
    TBP_L1_TAU20IT = false;
    TBP_L1_TAU30 = false;
    TBP_L1_TAU40 = false;
    TBP_L1_TAU60 = false;
    TBP_L1_TAU8 = false;
    TBP_L1_TAU8_EMPTY = false;
    TBP_L1_TAU20IM_2TAU12IM = false;
    TBP_L1_TAU20_2TAU12 = false;
    TBP_L1_EM15HI_2TAU12IM = false;
    TBP_L1_EM15HI_2TAU12IM_J25_3J12 = false;
    TBP_L1_EM15HI_TAU40_2TAU15 = false;
    TBP_L1_MU10_TAU12IM = false;
    TBP_L1_MU10_TAU12IM_J25_2J12 = false;
    TBP_L1_EM7_MU10 = false;
    TBP_L1_MU6_EMPTY = false;
    TBP_L1_MU10_TAU20IM = false;
    TBP_L1_XE10 = false;
    TBP_L1_TAU20IL_2TAU12IL_J25_2J20_3J12 = false;
    TBP_L1_TAU20IM_2TAU12IM_J25_2J20_3J12 = false;
    TBP_L1_J25_2J20_3J12_BOX_TAU20ITAU12I = false;
    TBP_L1_DR_MU10TAU12I_TAU12I_J25 = false;
    TBP_L1_MU10_TAU12I_J25 = false;
    TBP_L1_TAU20IM_2J20_XE45 = false;
    TBP_L1_J15_31ETA49_UNPAIRED_ISO = false;
    TBP_L1_XE20 = false;
    TBP_L1_XE45_TAU20_J20 = false;
    TBP_L1_EM15HI_2TAU12IM_XE35 = false;
    TBP_L1_XE35_EM15_TAU12I = false;
    TBP_L1_XE40_EM15_TAU12I = false;
    TBP_L1_MU10_TAU12IM_XE35 = false;
    TBP_L1_XE25 = false;
    TBP_L1_TAU20IM_2TAU12IM_XE35 = false;
    TBP_L1_TAU20_2TAU12_XE35 = false;
    TBP_L1_XE30 = false;
    TBP_L1_EM15VH_JJ15_23ETA49 = false;
    TBP_L1_MU4_J12 = false;
    TBP_L1_MU6_J20 = false;
    TBP_L1_MU6_J40 = false;
    TBP_L1_MU6_J75 = false;
    TBP_L1_J12 = false;
    TBP_L1_J15 = false;
    TBP_L1_J20 = false;
    TBP_L1_J25 = false;
    TBP_L1_J30 = false;
    TBP_L1_J40 = false;
    TBP_L1_J50 = false;
    TBP_L1_J75 = false;
    TBP_L1_J85 = false;
    TBP_L1_J100 = false;
    TBP_L1_J120 = false;
    TBP_L1_J400 = false;
    TBP_L1_J20_31ETA49 = false;
    TBP_L1_J30_31ETA49 = false;
    TBP_L1_J50_31ETA49 = false;
    TBP_L1_J75_31ETA49 = false;
    TBP_L1_J100_31ETA49 = false;
    TBP_L1_XE65 = false;
    TBP_L1_J15_31ETA49 = false;
    TBP_L1_J20_28ETA31 = false;
    TBP_L1_J12_EMPTY = false;
    TBP_L1_J12_FIRSTEMPTY = false;
    TBP_L1_J12_UNPAIRED_ISO = false;
    TBP_L1_J12_UNPAIRED_NONISO = false;
    TBP_L1_J12_ABORTGAPNOTCALIB = false;
    TBP_L1_J30_EMPTY = false;
    TBP_L1_J30_FIRSTEMPTY = false;
    TBP_L1_J30_31ETA49_EMPTY = false;
    TBP_L1_J30_31ETA49_UNPAIRED_ISO = false;
    TBP_L1_J30_31ETA49_UNPAIRED_NONISO = false;
    TBP_L1_J50_UNPAIRED_ISO = false;
    TBP_L1_J50_UNPAIRED_NONISO = false;
    TBP_L1_J50_ABORTGAPNOTCALIB = false;
    TBP_L1_AFP_NSC = false;
    TBP_L1_J20_J20_31ETA49 = false;
    TBP_L1_3J15 = false;
    TBP_L1_3J20 = false;
    TBP_L1_3J40 = false;
    TBP_L1_3J15_0ETA25 = false;
    TBP_L1_3J50 = false;
    TBP_L1_4J15 = false;
    TBP_L1_4J20 = false;
    TBP_L1_J75_XE50 = false;
    TBP_L1_XE75 = false;
    TBP_L1_6J15 = false;
    TBP_L1_J75_3J20 = false;
    TBP_L1_J30_0ETA49_2J20_0ETA49 = false;
    TBP_L1_TE10 = false;
    TBP_L1_AFP_FSC = false;
    TBP_L1_5J15_0ETA25 = false;
    TBP_L1_2J15_XE55 = false;
    TBP_L1_J40_XE50 = false;
    TBP_L1_J75_XE40 = false;
    TBP_L1_XE35 = false;
    TBP_L1_XE40 = false;
    TBP_L1_XE45 = false;
    TBP_L1_XE50 = false;
    TBP_L1_XE55 = false;
    TBP_L1_XE60 = false;
    TBP_L1_XE70 = false;
    TBP_L1_XE80 = false;
    TBP_L1_XS20 = false;
    TBP_L1_XS30 = false;
    TBP_L1_EM12_XS20 = false;
    TBP_L1_EM15_XS30 = false;
    TBP_L1_XE150 = false;
    TBP_L1_TE30 = false;
    TBP_L1_TE40 = false;
    TBP_L1_TE70 = false;
    TBP_L1_TE30_0ETA24 = false;
    TBP_L1_BCM_Wide_UNPAIRED_NONISO = false;
    TBP_L1_BCM_AC_CA_UNPAIRED_ISO = false;
    TBP_L1_BCM_AC_UNPAIRED_ISO = false;
    TBP_L1_MBTS_1_EMPTY = false;
    TBP_L1_MBTS_1_UNPAIRED_ISO = false;
    TBP_L1_MBTS_2_EMPTY = false;
    TBP_L1_MBTS_2_UNPAIRED_ISO = false;
    TBP_L1_MBTS_1_1_EMPTY = false;
    TBP_L1_MBTS_1_1_UNPAIRED_ISO = false;
    TBP_L1_AFP_C_ALFA_A = false;
    TBP_L1_AFP_C_ANY = false;
    TBP_L1_EM13VH_3J20 = false;
    TBP_L1_MU10_3J20 = false;
    TBP_L1_AFP_C_ANY_MBTS_A = false;
    TBP_L1_2J50_XE40 = false;
    TBP_L1_J40_XE60 = false;
    TBP_L1_J40_0ETA25_XE50 = false;
    TBP_L1_BPH_2M8_2MU4 = false;
    TBP_L1_BPH_8M15_MU6MU4 = false;
    TBP_L1_BPH_8M15_2MU6 = false;
    TBP_L1_J40_0ETA25_2J15_31ETA49 = false;
    TBP_L1_J40_0ETA25_2J25_J20_31ETA49 = false;
    TBP_L1_AFP_C_MBTS_A = false;
    TBP_L1_TGC_BURST_EMPTY = false;
    TBP_L1_MBTS_4_A_UNPAIRED_ISO = false;
    TBP_L1_MBTS_4_C_UNPAIRED_ISO = false;
    TBP_L1_XE300 = false;
    TBP_L1_RD1_BGRP10 = false;
    TBP_L1_AFP_C_ZDC_C = false;
    TBP_L1_AFP_C_J12 = false;
    TBP_L1_AFP_C_EM3 = false;
    TBP_L1_AFP_C_TE5 = false;
    TBP_L1_AFP_C_ALFA_C = false;
    TBP_L1_MBTS_4_A = false;
    TBP_L1_MBTS_4_C = false;
    TBP_L1_MBTS_1_BGRP9 = false;
    TBP_L1_MBTS_2_BGRP9 = false;
    TBP_L1_MBTS_1_BGRP11 = false;
    TBP_L1_MBTS_2_BGRP11 = false;
    TBP_L1_RD0_FILLED = false;
    TBP_L1_RD0_UNPAIRED_ISO = false;
    TBP_L1_RD0_EMPTY = false;
    TBP_L1_RD1_FILLED = false;
    TBP_L1_RD1_EMPTY = false;
    TBP_L1_RD2_FILLED = false;
    TBP_L1_RD2_EMPTY = false;
    TBP_L1_RD3_FILLED = false;
    TBP_L1_RD3_EMPTY = false;
    TBP_L1_RD0_FIRSTEMPTY = false;
    TBP_L1_RD0_BGRP9 = false;
    TBP_L1_RD0_BGRP11 = false;
    TBP_L1_LUCID = false;
    TBP_L1_LUCID_EMPTY = false;
    TBP_L1_LUCID_UNPAIRED_ISO = false;
    TBP_L1_LUCID_A_C_EMPTY = false;
    TBP_L1_LUCID_A_C_UNPAIRED_ISO = false;
    TBP_L1_LUCID_A_C_UNPAIRED_NONISO = false;
    TBP_L1_TRT_FILLED = false;
    TBP_L1_TRT_EMPTY = false;
    TBP_L1_TGC_BURST = false;
    TBP_L1_LHCF = false;
    TBP_L1_BCM_Wide_BGRP0 = false;
    TBP_L1_BCM_AC_CA_BGRP0 = false;
    TBP_L1_BCM_Wide_EMPTY = false;
    TBP_L1_BCM_Wide_UNPAIRED_ISO = false;
    TBP_L1_MBTS_1 = false;
    TBP_L1_MBTS_2 = false;
    TBP_L1_MBTS_1_1 = false;
    TBP_L1_BCM_CA_UNPAIRED_ISO = false;
    TBP_L1_BCM_AC_UNPAIRED_NONISO = false;
    TBP_L1_BCM_CA_UNPAIRED_NONISO = false;
    TBP_L1_BCM_AC_ABORTGAPNOTCALIB = false;
    TBP_L1_BCM_CA_ABORTGAPNOTCALIB = false;
    TBP_L1_BCM_Wide_ABORTGAPNOTCALIB = false;
    TBP_L1_BCM_AC_CALIB = false;
    TBP_L1_BCM_CA_CALIB = false;
    TBP_L1_BCM_Wide_CALIB = false;
    TBP_L1_BTAG_MU4J15 = false;
    TBP_L1_BTAG_MU4J30 = false;
    TBP_L1_ZB = false;
    TBP_L1_BPTX0_BGRP0 = false;
    TBP_L1_BPTX1_BGRP0 = false;
    TBP_L1_BTAG_MU6J20 = false;
    TBP_L1_BTAG_MU6J25 = false;
    TBP_L1_AFP_C_ANY_UNPAIRED_ISO = false;
    TBP_L1_3J15_BTAG_MU4J15 = false;
    TBP_L1_3J15_BTAG_MU4J30 = false;
    TBP_L1_3J15_BTAG_MU6J25 = false;
    TBP_L1_3J20_BTAG_MU4J20 = false;
    TBP_L1_J40_DPHI_Js2XE50 = false;
    TBP_L1_J40_DPHI_J20s2XE50 = false;
    TBP_L1_J40_DPHI_J20XE50 = false;
    TBP_L1_J40_DPHI_CJ20XE50 = false;
    TBP_L1_TAU40_2TAU20IM = false;
    TBP_L1_MU10_2J15_J20 = false;
    TBP_L1_MU11 = false;
    TBP_L1_AFP_C_ANY_UNPAIRED_NONISO = false;
    TBP_L1_HT190_J15_ETA21 = false;
    TBP_L1_HT190_J15s5_ETA21 = false;
    TBP_L1_HT150_J20_ETA31 = false;
    TBP_L1_HT150_J20s5_ETA31 = false;
    TBP_L1_JPSI_1M5 = false;
    TBP_L1_JPSI_1M5_EM7 = false;
    TBP_L1_JPSI_1M5_EM12 = false;
    TBP_L1_KF_XE35 = false;
    TBP_L1_KF_XE45 = false;
    TBP_L1_KF_XE55 = false;
    TBP_L1_KF_XE60 = false;
    TBP_L1_KF_XE65 = false;
    TBP_L1_KF_XE75 = false;
    TBP_L1_AFP_C_ANY_EMPTY = false;
    TBP_L1_AFP_C_ANY_FIRSTEMPTY = false;
    TBP_L1_AFP_C_AND = false;
    TBP_L1_EM12_W_MT25 = false;
    TBP_L1_EM12_W_MT30 = false;
    TBP_L1_EM15_W_MT35_W_250RO2_XEHT_0_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    TBP_L1_W_05RO_XEHT_0 = false;
    TBP_L1_MU10_2J20 = false;
    TBP_L1_W_90RO2_XEHT_0 = false;
    TBP_L1_W_250RO2_XEHT_0 = false;
    TBP_L1_W_HT20_JJ15_ETA49 = false;
    TBP_L1_W_NOMATCH = false;
    TBP_L1_W_NOMATCH_W_05RO_XEEMHT = false;
    TBP_L1_EM15_W_MT35_XS60_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    TBP_L1_EM15_W_MT35_XS40_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    TBP_L1_EM15_W_MT35 = false;
    TBP_L1_EM15_W_MT35_XS60 = false;
    TBP_L1_EM15VH_W_MT35_XS60 = false;
    TBP_L1_EM20VH_W_MT35_XS60 = false;
    TBP_L1_EM22VHI_W_MT35_XS40 = false;
    TBP_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE_XS30 = false;
    TBP_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    TBP_L1_HT150_JJ15_ETA49_MJJ_400 = false;
    TBP_L1_RD2_BGRP14 = false;
    TBP_L1_BPH_1M19_2MU4_BPH_0DR34_2MU4 = false;
    TBP_L1_RD3_BGRP15 = false;
    TBP_L1_BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4 = false;
    TBP_L1_BPH_2M9_2MU6_BPH_2DR15_2MU6 = false;
    TBP_L1_MU6MU4_BO = false;
    TBP_L1_2MU4_B = false;
    TBP_L1_2MU6_B = false;
    TBP_L1_BPH_1M19_2MU4_B_BPH_0DR34_2MU4 = false;
    TBP_L1_BPH_1M19_2MU4_BO_BPH_0DR34_2MU4 = false;
    TBP_L1_BPH_2M8_MU6MU4_B_BPH_0DR15_MU6MU4 = false;
    TBP_L1_2MU4_BO = false;
    TBP_L1_2MU6_BO = false;
    TBP_L1_MU6_2MU4_B = false;
    TBP_L1_DY_DR_2MU4 = false;
    TBP_L1_DY_BOX_2MU4 = false;
    TBP_L1_DY_BOX_MU6MU4 = false;
    TBP_L1_DY_BOX_2MU6 = false;
    TBP_L1_LFV_MU = false;
    TBP_L1_LFV_EM8I = false;
    TBP_L1_LFV_EM15I = false;
    TBP_L1_DPHI_Js2XE50 = false;
    TBP_L1_DPHI_J20s2XE50 = false;
    TBP_L1_DPHI_J20XE50 = false;
    TBP_L1_DPHI_CJ20XE50 = false;
    TBP_L1_MJJ_900 = false;
    TBP_L1_MJJ_800 = false;
    TBP_L1_MJJ_700 = false;
    TBP_L1_MJJ_400 = false;
    TBP_L1_MJJ_100 = false;
    TBP_L1_HT150_JJ15_ETA49 = false;
    TBP_L1_DETA_JJ = false;
    TBP_L1_J4_MATCH = false;
    TBP_L1_LLP_NOMATCH = false;
    TBP_L1_DR_MU10TAU12I = false;
    TBP_L1_TAU30_EMPTY = false;
    TBP_L1_EM15_TAU40 = false;
    TBP_L1_TAU30_UNPAIRED_ISO = false;
    TBP_L1_EM15_TAU12I = false;
    TBP_L1_EM15TAU12I_J25 = false;
    TBP_L1_DR_EM15TAU12I_J25 = false;
    TBP_L1_TAU20ITAU12I_J25 = false;
    TBP_L1_DR_TAU20ITAU12I = false;
    TBP_L1_BOX_TAU20ITAU12I = false;
    TBP_L1_DR_TAU20ITAU12I_J25 = false;
    TBP_L1_LAR_EM = false;
    TBP_L1_LAR_J = false;
    TBP_L1_MU6_MJJ_200 = false;
    TBP_L1_MU6_MJJ_300 = false;
    TBP_L1_MU6_MJJ_400 = false;
    TBP_L1_MU6_MJJ_500 = false;
    TBP_L1_J30_2J20_4J20_0ETA49_MJJ_400 = false;
    TBP_L1_J30_2J20_4J20_0ETA49_MJJ_700 = false;
    TBP_L1_J30_2J20_4J20_0ETA49_MJJ_800 = false;
    TBP_L1_J30_2J20_4J20_0ETA49_MJJ_900 = false;
    TBP_L1_3J20_4J20_0ETA49_MJJ_400 = false;
    TBP_L1_3J20_4J20_0ETA49_MJJ_700 = false;
    TBP_L1_3J20_4J20_0ETA49_MJJ_800 = false;
    TBP_L1_3J20_4J20_0ETA49_MJJ_900 = false;
    TBP_L1_XE35_MJJ_200 = false;
    TBP_L1_EM7_FIRSTEMPTY = false;
    TBP_L1_RD0_ABORTGAPNOTCALIB = false;
    TBP_L1_3J25_0ETA23 = false;
    TBP_L1_TE20 = false;
    TBP_L1_TE10_0ETA24 = false;
    TBP_L1_TE20_0ETA24 = false;
    TBP_L1_XS40 = false;
    TBP_L1_XS50 = false;
    TBP_L1_XS60 = false;
    TBP_L1_J12_BGRP12 = false;
    TBP_L1_J30_31ETA49_BGRP12 = false;
    TBP_L1_MU6_J30_0ETA49_2J20_0ETA49 = false;
    TBP_L1_4J20_0ETA49 = false;
    TBP_L1_TAU8_UNPAIRED_ISO = false;
    TBP_L1_EM7_UNPAIRED_ISO = false;
    TBP_L1_RD2_BGRP12 = false;
    TBP_L1_TAU8_FIRSTEMPTY = false;
    TBP_L1_EM24VHI = false;
    TBP_L1_LHCF_UNPAIRED_ISO = false;
    TBP_L1_LHCF_EMPTY = false;
    TBP_L1_EM15VH_2EM10VH_3EM7 = false;
    TBP_L1_EM15VH_3EM8VH = false;
    TBP_L1_EM8I_MU10 = false;
    TBP_L1_EM15HI = false;
    TBP_L1_MU6_3MU4 = false;
    TBP_L1_2MU6_3MU4 = false;
    TBP_L1_BGRP9 = false;
    TBP_L1_TE50 = false;
    TBP_L1_TE5 = false;
    TBP_L1_TE60 = false;
    TBP_L1_TE50_0ETA24 = false;
    TBP_L1_TE5_0ETA24 = false;
    TBP_L1_EM20VH_FIRSTEMPTY = false;
    TBP_L1_EM22VHI_FIRSTEMPTY = false;
    TBP_L1_MU20_FIRSTEMPTY = false;
    TBP_L1_J100_FIRSTEMPTY = false;
    TBP_L1_J100_31ETA49_FIRSTEMPTY = false;
    TBP_L1_TE60_0ETA24 = false;
    TBP_L1_TE70_0ETA24 = false;
    TBP_L1_TE40_0ETA24 = false;
    TBP_L1_ZDC_A = false;
    TBP_L1_ZDC_C = false;
    TBP_L1_ZDC_AND = false;
    TBP_L1_ZDC_A_C = false;
    TBP_L1_ALFA_ELAST1 = false;
    TBP_L1_ALFA_ELAST2 = false;
    TBP_L1_ALFA_ELAST11 = false;
    TBP_L1_ALFA_ELAST12 = false;
    TBP_L1_ALFA_ELAST13 = false;
    TBP_L1_ALFA_ELAST14 = false;
    TBP_L1_ALFA_ELAST15 = false;
    TBP_L1_ALFA_ELAST15_Calib = false;
    TBP_L1_ALFA_ELAST16 = false;
    TBP_L1_ALFA_ELAST17 = false;
    TBP_L1_ALFA_ELAST18 = false;
    TBP_L1_ALFA_ELAST18_Calib = false;
    TBP_L1_ALFA_SDIFF5 = false;
    TBP_L1_ALFA_SDIFF6 = false;
    TBP_L1_ALFA_SDIFF7 = false;
    TBP_L1_ALFA_SDIFF8 = false;
    TBP_L1_MBTS_1_A_ALFA_C = false;
    TBP_L1_MBTS_1_C_ALFA_A = false;
    TBP_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO = false;
    TBP_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO = false;
    TBP_L1_MBTS_2_A_ALFA_C = false;
    TBP_L1_MBTS_2_C_ALFA_A = false;
    TBP_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO = false;
    TBP_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO = false;
    TBP_L1_LUCID_A_ALFA_C = false;
    TBP_L1_LUCID_C_ALFA_A = false;
    TBP_L1_LUCID_A_ALFA_C_UNPAIRED_ISO = false;
    TBP_L1_LUCID_C_ALFA_A_UNPAIRED_ISO = false;
    TBP_L1_EM3_ALFA_ANY = false;
    TBP_L1_EM3_ALFA_ANY_UNPAIRED_ISO = false;
    TBP_L1_EM3_ALFA_EINE = false;
    TBP_L1_ALFA_ELASTIC_UNPAIRED_ISO = false;
    TBP_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO = false;
    TBP_L1_ALFA_ANY_A_EMPTY = false;
    TBP_L1_ALFA_ANY_C_EMPTY = false;
    TBP_L1_J12_ALFA_ANY = false;
    TBP_L1_J12_ALFA_ANY_UNPAIRED_ISO = false;
    TBP_L1_TE5_ALFA_ANY = false;
    TBP_L1_TE5_ALFA_ANY_UNPAIRED_ISO = false;
    TBP_L1_TE5_ALFA_EINE = false;
    TBP_L1_TRT_ALFA_ANY = false;
    TBP_L1_TRT_ALFA_ANY_UNPAIRED_ISO = false;
    TBP_L1_TRT_ALFA_EINE = false;
    TBP_L1_TAU8_UNPAIRED_NONISO = false;
    TBP_L1_EM7_UNPAIRED_NONISO = false;
    TBP_L1_MU4_UNPAIRED_NONISO = false;
    TBP_L1_ALFA_BGT = false;
    TBP_L1_ALFA_BGT_UNPAIRED_ISO = false;
    TBP_L1_ALFA_BGT_BGRP10 = false;
    TBP_L1_ALFA_SHOWSYST5 = false;
    TBP_L1_ALFA_SYST9 = false;
    TBP_L1_ALFA_SYST10 = false;
    TBP_L1_ALFA_SYST11 = false;
    TBP_L1_ALFA_SYST12 = false;
    TBP_L1_ALFA_SYST17 = false;
    TBP_L1_ALFA_SYST18 = false;
    TBP_L1_ALFA_ANY = false;
    TBP_L1_ALFA_ANY_EMPTY = false;
    TBP_L1_ALFA_ANY_FIRSTEMPTY = false;
    TBP_L1_ALFA_ANY_UNPAIRED_ISO = false;
    TBP_L1_ALFA_ANY_UNPAIRED_NONISO = false;
    TBP_L1_ALFA_ANY_BGRP10 = false;
    TBP_L1_ALFA_ANY_ABORTGAPNOTCALIB = false;
    TBP_L1_ALFA_ANY_CALIB = false;
    TBP_L1_ALFA_B7L1U = false;
    TBP_L1_ALFA_B7L1L = false;
    TBP_L1_ALFA_A7L1U = false;
    TBP_L1_ALFA_A7L1L = false;
    TBP_L1_ALFA_A7R1U = false;
    TBP_L1_ALFA_A7R1L = false;
    TBP_L1_ALFA_B7R1U = false;
    TBP_L1_ALFA_B7R1L = false;
    TBP_L1_ALFA_B7L1U_OD = false;
    TBP_L1_ALFA_B7L1L_OD = false;
    TBP_L1_ALFA_A7L1U_OD = false;
    TBP_L1_ALFA_A7L1L_OD = false;
    TBP_L1_ALFA_A7R1U_OD = false;
    TBP_L1_ALFA_A7R1L_OD = false;
    TBP_L1_ALFA_B7R1U_OD = false;
    TBP_L1_ALFA_B7R1L_OD = false;
    TBP_L1_ALFA_B7L1_OD = false;
    TBP_L1_ALFA_A7L1_OD = false;
    TBP_L1_ALFA_B7R1_OD = false;
    TBP_L1_ALFA_A7R1_OD = false;
    TBP_L1_CALREQ2 = false;

    L1_EM13VH = false;
    L1_EM15 = false;
    L1_EM15VH = false;
    L1_EM18VH = false;
    L1_EM20VH = false;
    L1_EM20VHI = false;
    L1_EM22VHI = false;
    L1_EM3_EMPTY = false;
    L1_EM7_EMPTY = false;
    L1_MU4 = false;
    L1_MU6 = false;
    L1_MU10 = false;
    L1_MU15 = false;
    L1_MU20 = false;
    L1_MU4_EMPTY = false;
    L1_MU4_FIRSTEMPTY = false;
    L1_MU11_EMPTY = false;
    L1_MU4_UNPAIRED_ISO = false;
    L1_2EM3 = false;
    L1_2EM7 = false;
    L1_2EM10VH = false;
    L1_2EM13VH = false;
    L1_2EM15 = false;
    L1_2EM15VH = false;
    L1_EM7_2EM3 = false;
    L1_EM12_2EM3 = false;
    L1_EM15VH_3EM7 = false;
    L1_2MU4 = false;
    L1_2MU6 = false;
    L1_2MU10 = false;
    L1_2MU20_OVERLAY = false;
    L1_MU10_2MU6 = false;
    L1_MU11_2MU6 = false;
    L1_3MU4 = false;
    L1_MU6_2MU4 = false;
    L1_3MU6 = false;
    L1_4J15_0ETA25 = false;
    L1_EM15I_MU4 = false;
    L1_2EM8VH_MU10 = false;
    L1_EM15VH_MU10 = false;
    L1_TAU12 = false;
    L1_TAU12IL = false;
    L1_TAU12IM = false;
    L1_TAU12IT = false;
    L1_TAU20 = false;
    L1_TAU20IL = false;
    L1_TAU20IM = false;
    L1_TAU20IT = false;
    L1_TAU30 = false;
    L1_TAU40 = false;
    L1_TAU60 = false;
    L1_TAU8 = false;
    L1_TAU8_EMPTY = false;
    L1_TAU20IM_2TAU12IM = false;
    L1_TAU20_2TAU12 = false;
    L1_EM15HI_2TAU12IM = false;
    L1_EM15HI_2TAU12IM_J25_3J12 = false;
    L1_EM15HI_TAU40_2TAU15 = false;
    L1_MU10_TAU12IM = false;
    L1_MU10_TAU12IM_J25_2J12 = false;
    L1_EM7_MU10 = false;
    L1_MU6_EMPTY = false;
    L1_MU10_TAU20IM = false;
    L1_XE10 = false;
    L1_TAU20IL_2TAU12IL_J25_2J20_3J12 = false;
    L1_TAU20IM_2TAU12IM_J25_2J20_3J12 = false;
    L1_J25_2J20_3J12_BOX_TAU20ITAU12I = false;
    L1_DR_MU10TAU12I_TAU12I_J25 = false;
    L1_MU10_TAU12I_J25 = false;
    L1_TAU20IM_2J20_XE45 = false;
    L1_J15_31ETA49_UNPAIRED_ISO = false;
    L1_XE20 = false;
    L1_XE45_TAU20_J20 = false;
    L1_EM15HI_2TAU12IM_XE35 = false;
    L1_XE35_EM15_TAU12I = false;
    L1_XE40_EM15_TAU12I = false;
    L1_MU10_TAU12IM_XE35 = false;
    L1_XE25 = false;
    L1_TAU20IM_2TAU12IM_XE35 = false;
    L1_TAU20_2TAU12_XE35 = false;
    L1_XE30 = false;
    L1_EM15VH_JJ15_23ETA49 = false;
    L1_MU4_J12 = false;
    L1_MU6_J20 = false;
    L1_MU6_J40 = false;
    L1_MU6_J75 = false;
    L1_J12 = false;
    L1_J15 = false;
    L1_J20 = false;
    L1_J25 = false;
    L1_J30 = false;
    L1_J40 = false;
    L1_J50 = false;
    L1_J75 = false;
    L1_J85 = false;
    L1_J100 = false;
    L1_J120 = false;
    L1_J400 = false;
    L1_J20_31ETA49 = false;
    L1_J30_31ETA49 = false;
    L1_J50_31ETA49 = false;
    L1_J75_31ETA49 = false;
    L1_J100_31ETA49 = false;
    L1_XE65 = false;
    L1_J15_31ETA49 = false;
    L1_J20_28ETA31 = false;
    L1_J12_EMPTY = false;
    L1_J12_FIRSTEMPTY = false;
    L1_J12_UNPAIRED_ISO = false;
    L1_J12_UNPAIRED_NONISO = false;
    L1_J12_ABORTGAPNOTCALIB = false;
    L1_J30_EMPTY = false;
    L1_J30_FIRSTEMPTY = false;
    L1_J30_31ETA49_EMPTY = false;
    L1_J30_31ETA49_UNPAIRED_ISO = false;
    L1_J30_31ETA49_UNPAIRED_NONISO = false;
    L1_J50_UNPAIRED_ISO = false;
    L1_J50_UNPAIRED_NONISO = false;
    L1_J50_ABORTGAPNOTCALIB = false;
    L1_AFP_NSC = false;
    L1_J20_J20_31ETA49 = false;
    L1_3J15 = false;
    L1_3J20 = false;
    L1_3J40 = false;
    L1_3J15_0ETA25 = false;
    L1_3J50 = false;
    L1_4J15 = false;
    L1_4J20 = false;
    L1_J75_XE50 = false;
    L1_XE75 = false;
    L1_6J15 = false;
    L1_J75_3J20 = false;
    L1_J30_0ETA49_2J20_0ETA49 = false;
    L1_TE10 = false;
    L1_AFP_FSC = false;
    L1_5J15_0ETA25 = false;
    L1_2J15_XE55 = false;
    L1_J40_XE50 = false;
    L1_J75_XE40 = false;
    L1_XE35 = false;
    L1_XE40 = false;
    L1_XE45 = false;
    L1_XE50 = false;
    L1_XE55 = false;
    L1_XE60 = false;
    L1_XE70 = false;
    L1_XE80 = false;
    L1_XS20 = false;
    L1_XS30 = false;
    L1_EM12_XS20 = false;
    L1_EM15_XS30 = false;
    L1_XE150 = false;
    L1_TE30 = false;
    L1_TE40 = false;
    L1_TE70 = false;
    L1_TE30_0ETA24 = false;
    L1_BCM_Wide_UNPAIRED_NONISO = false;
    L1_BCM_AC_CA_UNPAIRED_ISO = false;
    L1_BCM_AC_UNPAIRED_ISO = false;
    L1_MBTS_1_EMPTY = false;
    L1_MBTS_1_UNPAIRED_ISO = false;
    L1_MBTS_2_EMPTY = false;
    L1_MBTS_2_UNPAIRED_ISO = false;
    L1_MBTS_1_1_EMPTY = false;
    L1_MBTS_1_1_UNPAIRED_ISO = false;
    L1_AFP_C_ALFA_A = false;
    L1_AFP_C_ANY = false;
    L1_EM13VH_3J20 = false;
    L1_MU10_3J20 = false;
    L1_AFP_C_ANY_MBTS_A = false;
    L1_2J50_XE40 = false;
    L1_J40_XE60 = false;
    L1_J40_0ETA25_XE50 = false;
    L1_BPH_2M8_2MU4 = false;
    L1_BPH_8M15_MU6MU4 = false;
    L1_BPH_8M15_2MU6 = false;
    L1_J40_0ETA25_2J15_31ETA49 = false;
    L1_J40_0ETA25_2J25_J20_31ETA49 = false;
    L1_AFP_C_MBTS_A = false;
    L1_TGC_BURST_EMPTY = false;
    L1_MBTS_4_A_UNPAIRED_ISO = false;
    L1_MBTS_4_C_UNPAIRED_ISO = false;
    L1_XE300 = false;
    L1_RD1_BGRP10 = false;
    L1_AFP_C_ZDC_C = false;
    L1_AFP_C_J12 = false;
    L1_AFP_C_EM3 = false;
    L1_AFP_C_TE5 = false;
    L1_AFP_C_ALFA_C = false;
    L1_MBTS_4_A = false;
    L1_MBTS_4_C = false;
    L1_MBTS_1_BGRP9 = false;
    L1_MBTS_2_BGRP9 = false;
    L1_MBTS_1_BGRP11 = false;
    L1_MBTS_2_BGRP11 = false;
    L1_RD0_FILLED = false;
    L1_RD0_UNPAIRED_ISO = false;
    L1_RD0_EMPTY = false;
    L1_RD1_FILLED = false;
    L1_RD1_EMPTY = false;
    L1_RD2_FILLED = false;
    L1_RD2_EMPTY = false;
    L1_RD3_FILLED = false;
    L1_RD3_EMPTY = false;
    L1_RD0_FIRSTEMPTY = false;
    L1_RD0_BGRP9 = false;
    L1_RD0_BGRP11 = false;
    L1_LUCID = false;
    L1_LUCID_EMPTY = false;
    L1_LUCID_UNPAIRED_ISO = false;
    L1_LUCID_A_C_EMPTY = false;
    L1_LUCID_A_C_UNPAIRED_ISO = false;
    L1_LUCID_A_C_UNPAIRED_NONISO = false;
    L1_TRT_FILLED = false;
    L1_TRT_EMPTY = false;
    L1_TGC_BURST = false;
    L1_LHCF = false;
    L1_BCM_Wide_BGRP0 = false;
    L1_BCM_AC_CA_BGRP0 = false;
    L1_BCM_Wide_EMPTY = false;
    L1_BCM_Wide_UNPAIRED_ISO = false;
    L1_MBTS_1 = false;
    L1_MBTS_2 = false;
    L1_MBTS_1_1 = false;
    L1_BCM_CA_UNPAIRED_ISO = false;
    L1_BCM_AC_UNPAIRED_NONISO = false;
    L1_BCM_CA_UNPAIRED_NONISO = false;
    L1_BCM_AC_ABORTGAPNOTCALIB = false;
    L1_BCM_CA_ABORTGAPNOTCALIB = false;
    L1_BCM_Wide_ABORTGAPNOTCALIB = false;
    L1_BCM_AC_CALIB = false;
    L1_BCM_CA_CALIB = false;
    L1_BCM_Wide_CALIB = false;
    L1_BTAG_MU4J15 = false;
    L1_BTAG_MU4J30 = false;
    L1_ZB = false;
    L1_BPTX0_BGRP0 = false;
    L1_BPTX1_BGRP0 = false;
    L1_BTAG_MU6J20 = false;
    L1_BTAG_MU6J25 = false;
    L1_AFP_C_ANY_UNPAIRED_ISO = false;
    L1_3J15_BTAG_MU4J15 = false;
    L1_3J15_BTAG_MU4J30 = false;
    L1_3J15_BTAG_MU6J25 = false;
    L1_3J20_BTAG_MU4J20 = false;
    L1_J40_DPHI_Js2XE50 = false;
    L1_J40_DPHI_J20s2XE50 = false;
    L1_J40_DPHI_J20XE50 = false;
    L1_J40_DPHI_CJ20XE50 = false;
    L1_TAU40_2TAU20IM = false;
    L1_MU10_2J15_J20 = false;
    L1_MU11 = false;
    L1_AFP_C_ANY_UNPAIRED_NONISO = false;
    L1_HT190_J15_ETA21 = false;
    L1_HT190_J15s5_ETA21 = false;
    L1_HT150_J20_ETA31 = false;
    L1_HT150_J20s5_ETA31 = false;
    L1_JPSI_1M5 = false;
    L1_JPSI_1M5_EM7 = false;
    L1_JPSI_1M5_EM12 = false;
    L1_KF_XE35 = false;
    L1_KF_XE45 = false;
    L1_KF_XE55 = false;
    L1_KF_XE60 = false;
    L1_KF_XE65 = false;
    L1_KF_XE75 = false;
    L1_AFP_C_ANY_EMPTY = false;
    L1_AFP_C_ANY_FIRSTEMPTY = false;
    L1_AFP_C_AND = false;
    L1_EM12_W_MT25 = false;
    L1_EM12_W_MT30 = false;
    L1_EM15_W_MT35_W_250RO2_XEHT_0_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    L1_W_05RO_XEHT_0 = false;
    L1_MU10_2J20 = false;
    L1_W_90RO2_XEHT_0 = false;
    L1_W_250RO2_XEHT_0 = false;
    L1_W_HT20_JJ15_ETA49 = false;
    L1_W_NOMATCH = false;
    L1_W_NOMATCH_W_05RO_XEEMHT = false;
    L1_EM15_W_MT35_XS60_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    L1_EM15_W_MT35_XS40_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    L1_EM15_W_MT35 = false;
    L1_EM15_W_MT35_XS60 = false;
    L1_EM15VH_W_MT35_XS60 = false;
    L1_EM20VH_W_MT35_XS60 = false;
    L1_EM22VHI_W_MT35_XS40 = false;
    L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE_XS30 = false;
    L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    L1_HT150_JJ15_ETA49_MJJ_400 = false;
    L1_RD2_BGRP14 = false;
    L1_BPH_1M19_2MU4_BPH_0DR34_2MU4 = false;
    L1_RD3_BGRP15 = false;
    L1_BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4 = false;
    L1_BPH_2M9_2MU6_BPH_2DR15_2MU6 = false;
    L1_MU6MU4_BO = false;
    L1_2MU4_B = false;
    L1_2MU6_B = false;
    L1_BPH_1M19_2MU4_B_BPH_0DR34_2MU4 = false;
    L1_BPH_1M19_2MU4_BO_BPH_0DR34_2MU4 = false;
    L1_BPH_2M8_MU6MU4_B_BPH_0DR15_MU6MU4 = false;
    L1_2MU4_BO = false;
    L1_2MU6_BO = false;
    L1_MU6_2MU4_B = false;
    L1_DY_DR_2MU4 = false;
    L1_DY_BOX_2MU4 = false;
    L1_DY_BOX_MU6MU4 = false;
    L1_DY_BOX_2MU6 = false;
    L1_LFV_MU = false;
    L1_LFV_EM8I = false;
    L1_LFV_EM15I = false;
    L1_DPHI_Js2XE50 = false;
    L1_DPHI_J20s2XE50 = false;
    L1_DPHI_J20XE50 = false;
    L1_DPHI_CJ20XE50 = false;
    L1_MJJ_900 = false;
    L1_MJJ_800 = false;
    L1_MJJ_700 = false;
    L1_MJJ_400 = false;
    L1_MJJ_100 = false;
    L1_HT150_JJ15_ETA49 = false;
    L1_DETA_JJ = false;
    L1_J4_MATCH = false;
    L1_LLP_NOMATCH = false;
    L1_DR_MU10TAU12I = false;
    L1_TAU30_EMPTY = false;
    L1_EM15_TAU40 = false;
    L1_TAU30_UNPAIRED_ISO = false;
    L1_EM15_TAU12I = false;
    L1_EM15TAU12I_J25 = false;
    L1_DR_EM15TAU12I_J25 = false;
    L1_TAU20ITAU12I_J25 = false;
    L1_DR_TAU20ITAU12I = false;
    L1_BOX_TAU20ITAU12I = false;
    L1_DR_TAU20ITAU12I_J25 = false;
    L1_LAR_EM = false;
    L1_LAR_J = false;
    L1_MU6_MJJ_200 = false;
    L1_MU6_MJJ_300 = false;
    L1_MU6_MJJ_400 = false;
    L1_MU6_MJJ_500 = false;
    L1_J30_2J20_4J20_0ETA49_MJJ_400 = false;
    L1_J30_2J20_4J20_0ETA49_MJJ_700 = false;
    L1_J30_2J20_4J20_0ETA49_MJJ_800 = false;
    L1_J30_2J20_4J20_0ETA49_MJJ_900 = false;
    L1_3J20_4J20_0ETA49_MJJ_400 = false;
    L1_3J20_4J20_0ETA49_MJJ_700 = false;
    L1_3J20_4J20_0ETA49_MJJ_800 = false;
    L1_3J20_4J20_0ETA49_MJJ_900 = false;
    L1_XE35_MJJ_200 = false;
    L1_EM7_FIRSTEMPTY = false;
    L1_RD0_ABORTGAPNOTCALIB = false;
    L1_3J25_0ETA23 = false;
    L1_TE20 = false;
    L1_TE10_0ETA24 = false;
    L1_TE20_0ETA24 = false;
    L1_XS40 = false;
    L1_XS50 = false;
    L1_XS60 = false;
    L1_J12_BGRP12 = false;
    L1_J30_31ETA49_BGRP12 = false;
    L1_MU6_J30_0ETA49_2J20_0ETA49 = false;
    L1_4J20_0ETA49 = false;
    L1_TAU8_UNPAIRED_ISO = false;
    L1_EM7_UNPAIRED_ISO = false;
    L1_RD2_BGRP12 = false;
    L1_TAU8_FIRSTEMPTY = false;
    L1_EM24VHI = false;
    L1_LHCF_UNPAIRED_ISO = false;
    L1_LHCF_EMPTY = false;
    L1_EM15VH_2EM10VH_3EM7 = false;
    L1_EM15VH_3EM8VH = false;
    L1_EM8I_MU10 = false;
    L1_EM15HI = false;
    L1_MU6_3MU4 = false;
    L1_2MU6_3MU4 = false;
    L1_BGRP9 = false;
    L1_TE50 = false;
    L1_TE5 = false;
    L1_TE60 = false;
    L1_TE50_0ETA24 = false;
    L1_TE5_0ETA24 = false;
    L1_EM20VH_FIRSTEMPTY = false;
    L1_EM22VHI_FIRSTEMPTY = false;
    L1_MU20_FIRSTEMPTY = false;
    L1_J100_FIRSTEMPTY = false;
    L1_J100_31ETA49_FIRSTEMPTY = false;
    L1_TE60_0ETA24 = false;
    L1_TE70_0ETA24 = false;
    L1_TE40_0ETA24 = false;
    L1_ZDC_A = false;
    L1_ZDC_C = false;
    L1_ZDC_AND = false;
    L1_ZDC_A_C = false;
    L1_ALFA_ELAST1 = false;
    L1_ALFA_ELAST2 = false;
    L1_ALFA_ELAST11 = false;
    L1_ALFA_ELAST12 = false;
    L1_ALFA_ELAST13 = false;
    L1_ALFA_ELAST14 = false;
    L1_ALFA_ELAST15 = false;
    L1_ALFA_ELAST15_Calib = false;
    L1_ALFA_ELAST16 = false;
    L1_ALFA_ELAST17 = false;
    L1_ALFA_ELAST18 = false;
    L1_ALFA_ELAST18_Calib = false;
    L1_ALFA_SDIFF5 = false;
    L1_ALFA_SDIFF6 = false;
    L1_ALFA_SDIFF7 = false;
    L1_ALFA_SDIFF8 = false;
    L1_MBTS_1_A_ALFA_C = false;
    L1_MBTS_1_C_ALFA_A = false;
    L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO = false;
    L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO = false;
    L1_MBTS_2_A_ALFA_C = false;
    L1_MBTS_2_C_ALFA_A = false;
    L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO = false;
    L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO = false;
    L1_LUCID_A_ALFA_C = false;
    L1_LUCID_C_ALFA_A = false;
    L1_LUCID_A_ALFA_C_UNPAIRED_ISO = false;
    L1_LUCID_C_ALFA_A_UNPAIRED_ISO = false;
    L1_EM3_ALFA_ANY = false;
    L1_EM3_ALFA_ANY_UNPAIRED_ISO = false;
    L1_EM3_ALFA_EINE = false;
    L1_ALFA_ELASTIC_UNPAIRED_ISO = false;
    L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO = false;
    L1_ALFA_ANY_A_EMPTY = false;
    L1_ALFA_ANY_C_EMPTY = false;
    L1_J12_ALFA_ANY = false;
    L1_J12_ALFA_ANY_UNPAIRED_ISO = false;
    L1_TE5_ALFA_ANY = false;
    L1_TE5_ALFA_ANY_UNPAIRED_ISO = false;
    L1_TE5_ALFA_EINE = false;
    L1_TRT_ALFA_ANY = false;
    L1_TRT_ALFA_ANY_UNPAIRED_ISO = false;
    L1_TRT_ALFA_EINE = false;
    L1_TAU8_UNPAIRED_NONISO = false;
    L1_EM7_UNPAIRED_NONISO = false;
    L1_MU4_UNPAIRED_NONISO = false;
    L1_ALFA_BGT = false;
    L1_ALFA_BGT_UNPAIRED_ISO = false;
    L1_ALFA_BGT_BGRP10 = false;
    L1_ALFA_SHOWSYST5 = false;
    L1_ALFA_SYST9 = false;
    L1_ALFA_SYST10 = false;
    L1_ALFA_SYST11 = false;
    L1_ALFA_SYST12 = false;
    L1_ALFA_SYST17 = false;
    L1_ALFA_SYST18 = false;
    L1_ALFA_ANY = false;
    L1_ALFA_ANY_EMPTY = false;
    L1_ALFA_ANY_FIRSTEMPTY = false;
    L1_ALFA_ANY_UNPAIRED_ISO = false;
    L1_ALFA_ANY_UNPAIRED_NONISO = false;
    L1_ALFA_ANY_BGRP10 = false;
    L1_ALFA_ANY_ABORTGAPNOTCALIB = false;
    L1_ALFA_ANY_CALIB = false;
    L1_ALFA_B7L1U = false;
    L1_ALFA_B7L1L = false;
    L1_ALFA_A7L1U = false;
    L1_ALFA_A7L1L = false;
    L1_ALFA_A7R1U = false;
    L1_ALFA_A7R1L = false;
    L1_ALFA_B7R1U = false;
    L1_ALFA_B7R1L = false;
    L1_ALFA_B7L1U_OD = false;
    L1_ALFA_B7L1L_OD = false;
    L1_ALFA_A7L1U_OD = false;
    L1_ALFA_A7L1L_OD = false;
    L1_ALFA_A7R1U_OD = false;
    L1_ALFA_A7R1L_OD = false;
    L1_ALFA_B7R1U_OD = false;
    L1_ALFA_B7R1L_OD = false;
    L1_ALFA_B7L1_OD = false;
    L1_ALFA_A7L1_OD = false;
    L1_ALFA_B7R1_OD = false;
    L1_ALFA_A7R1_OD = false;
    L1_CALREQ2 = false;

    TAV_L1_EM13VH = false;
    TAV_L1_EM15 = false;
    TAV_L1_EM15VH = false;
    TAV_L1_EM18VH = false;
    TAV_L1_EM20VH = false;
    TAV_L1_EM20VHI = false;
    TAV_L1_EM22VHI = false;
    TAV_L1_EM3_EMPTY = false;
    TAV_L1_EM7_EMPTY = false;
    TAV_L1_MU4 = false;
    TAV_L1_MU6 = false;
    TAV_L1_MU10 = false;
    TAV_L1_MU15 = false;
    TAV_L1_MU20 = false;
    TAV_L1_MU4_EMPTY = false;
    TAV_L1_MU4_FIRSTEMPTY = false;
    TAV_L1_MU11_EMPTY = false;
    TAV_L1_MU4_UNPAIRED_ISO = false;
    TAV_L1_2EM3 = false;
    TAV_L1_2EM7 = false;
    TAV_L1_2EM10VH = false;
    TAV_L1_2EM13VH = false;
    TAV_L1_2EM15 = false;
    TAV_L1_2EM15VH = false;
    TAV_L1_EM7_2EM3 = false;
    TAV_L1_EM12_2EM3 = false;
    TAV_L1_EM15VH_3EM7 = false;
    TAV_L1_2MU4 = false;
    TAV_L1_2MU6 = false;
    TAV_L1_2MU10 = false;
    TAV_L1_2MU20_OVERLAY = false;
    TAV_L1_MU10_2MU6 = false;
    TAV_L1_MU11_2MU6 = false;
    TAV_L1_3MU4 = false;
    TAV_L1_MU6_2MU4 = false;
    TAV_L1_3MU6 = false;
    TAV_L1_4J15_0ETA25 = false;
    TAV_L1_EM15I_MU4 = false;
    TAV_L1_2EM8VH_MU10 = false;
    TAV_L1_EM15VH_MU10 = false;
    TAV_L1_TAU12 = false;
    TAV_L1_TAU12IL = false;
    TAV_L1_TAU12IM = false;
    TAV_L1_TAU12IT = false;
    TAV_L1_TAU20 = false;
    TAV_L1_TAU20IL = false;
    TAV_L1_TAU20IM = false;
    TAV_L1_TAU20IT = false;
    TAV_L1_TAU30 = false;
    TAV_L1_TAU40 = false;
    TAV_L1_TAU60 = false;
    TAV_L1_TAU8 = false;
    TAV_L1_TAU8_EMPTY = false;
    TAV_L1_TAU20IM_2TAU12IM = false;
    TAV_L1_TAU20_2TAU12 = false;
    TAV_L1_EM15HI_2TAU12IM = false;
    TAV_L1_EM15HI_2TAU12IM_J25_3J12 = false;
    TAV_L1_EM15HI_TAU40_2TAU15 = false;
    TAV_L1_MU10_TAU12IM = false;
    TAV_L1_MU10_TAU12IM_J25_2J12 = false;
    TAV_L1_EM7_MU10 = false;
    TAV_L1_MU6_EMPTY = false;
    TAV_L1_MU10_TAU20IM = false;
    TAV_L1_XE10 = false;
    TAV_L1_TAU20IL_2TAU12IL_J25_2J20_3J12 = false;
    TAV_L1_TAU20IM_2TAU12IM_J25_2J20_3J12 = false;
    TAV_L1_J25_2J20_3J12_BOX_TAU20ITAU12I = false;
    TAV_L1_DR_MU10TAU12I_TAU12I_J25 = false;
    TAV_L1_MU10_TAU12I_J25 = false;
    TAV_L1_TAU20IM_2J20_XE45 = false;
    TAV_L1_J15_31ETA49_UNPAIRED_ISO = false;
    TAV_L1_XE20 = false;
    TAV_L1_XE45_TAU20_J20 = false;
    TAV_L1_EM15HI_2TAU12IM_XE35 = false;
    TAV_L1_XE35_EM15_TAU12I = false;
    TAV_L1_XE40_EM15_TAU12I = false;
    TAV_L1_MU10_TAU12IM_XE35 = false;
    TAV_L1_XE25 = false;
    TAV_L1_TAU20IM_2TAU12IM_XE35 = false;
    TAV_L1_TAU20_2TAU12_XE35 = false;
    TAV_L1_XE30 = false;
    TAV_L1_EM15VH_JJ15_23ETA49 = false;
    TAV_L1_MU4_J12 = false;
    TAV_L1_MU6_J20 = false;
    TAV_L1_MU6_J40 = false;
    TAV_L1_MU6_J75 = false;
    TAV_L1_J12 = false;
    TAV_L1_J15 = false;
    TAV_L1_J20 = false;
    TAV_L1_J25 = false;
    TAV_L1_J30 = false;
    TAV_L1_J40 = false;
    TAV_L1_J50 = false;
    TAV_L1_J75 = false;
    TAV_L1_J85 = false;
    TAV_L1_J100 = false;
    TAV_L1_J120 = false;
    TAV_L1_J400 = false;
    TAV_L1_J20_31ETA49 = false;
    TAV_L1_J30_31ETA49 = false;
    TAV_L1_J50_31ETA49 = false;
    TAV_L1_J75_31ETA49 = false;
    TAV_L1_J100_31ETA49 = false;
    TAV_L1_XE65 = false;
    TAV_L1_J15_31ETA49 = false;
    TAV_L1_J20_28ETA31 = false;
    TAV_L1_J12_EMPTY = false;
    TAV_L1_J12_FIRSTEMPTY = false;
    TAV_L1_J12_UNPAIRED_ISO = false;
    TAV_L1_J12_UNPAIRED_NONISO = false;
    TAV_L1_J12_ABORTGAPNOTCALIB = false;
    TAV_L1_J30_EMPTY = false;
    TAV_L1_J30_FIRSTEMPTY = false;
    TAV_L1_J30_31ETA49_EMPTY = false;
    TAV_L1_J30_31ETA49_UNPAIRED_ISO = false;
    TAV_L1_J30_31ETA49_UNPAIRED_NONISO = false;
    TAV_L1_J50_UNPAIRED_ISO = false;
    TAV_L1_J50_UNPAIRED_NONISO = false;
    TAV_L1_J50_ABORTGAPNOTCALIB = false;
    TAV_L1_AFP_NSC = false;
    TAV_L1_J20_J20_31ETA49 = false;
    TAV_L1_3J15 = false;
    TAV_L1_3J20 = false;
    TAV_L1_3J40 = false;
    TAV_L1_3J15_0ETA25 = false;
    TAV_L1_3J50 = false;
    TAV_L1_4J15 = false;
    TAV_L1_4J20 = false;
    TAV_L1_J75_XE50 = false;
    TAV_L1_XE75 = false;
    TAV_L1_6J15 = false;
    TAV_L1_J75_3J20 = false;
    TAV_L1_J30_0ETA49_2J20_0ETA49 = false;
    TAV_L1_TE10 = false;
    TAV_L1_AFP_FSC = false;
    TAV_L1_5J15_0ETA25 = false;
    TAV_L1_2J15_XE55 = false;
    TAV_L1_J40_XE50 = false;
    TAV_L1_J75_XE40 = false;
    TAV_L1_XE35 = false;
    TAV_L1_XE40 = false;
    TAV_L1_XE45 = false;
    TAV_L1_XE50 = false;
    TAV_L1_XE55 = false;
    TAV_L1_XE60 = false;
    TAV_L1_XE70 = false;
    TAV_L1_XE80 = false;
    TAV_L1_XS20 = false;
    TAV_L1_XS30 = false;
    TAV_L1_EM12_XS20 = false;
    TAV_L1_EM15_XS30 = false;
    TAV_L1_XE150 = false;
    TAV_L1_TE30 = false;
    TAV_L1_TE40 = false;
    TAV_L1_TE70 = false;
    TAV_L1_TE30_0ETA24 = false;
    TAV_L1_BCM_Wide_UNPAIRED_NONISO = false;
    TAV_L1_BCM_AC_CA_UNPAIRED_ISO = false;
    TAV_L1_BCM_AC_UNPAIRED_ISO = false;
    TAV_L1_MBTS_1_EMPTY = false;
    TAV_L1_MBTS_1_UNPAIRED_ISO = false;
    TAV_L1_MBTS_2_EMPTY = false;
    TAV_L1_MBTS_2_UNPAIRED_ISO = false;
    TAV_L1_MBTS_1_1_EMPTY = false;
    TAV_L1_MBTS_1_1_UNPAIRED_ISO = false;
    TAV_L1_AFP_C_ALFA_A = false;
    TAV_L1_AFP_C_ANY = false;
    TAV_L1_EM13VH_3J20 = false;
    TAV_L1_MU10_3J20 = false;
    TAV_L1_AFP_C_ANY_MBTS_A = false;
    TAV_L1_2J50_XE40 = false;
    TAV_L1_J40_XE60 = false;
    TAV_L1_J40_0ETA25_XE50 = false;
    TAV_L1_BPH_2M8_2MU4 = false;
    TAV_L1_BPH_8M15_MU6MU4 = false;
    TAV_L1_BPH_8M15_2MU6 = false;
    TAV_L1_J40_0ETA25_2J15_31ETA49 = false;
    TAV_L1_J40_0ETA25_2J25_J20_31ETA49 = false;
    TAV_L1_AFP_C_MBTS_A = false;
    TAV_L1_TGC_BURST_EMPTY = false;
    TAV_L1_MBTS_4_A_UNPAIRED_ISO = false;
    TAV_L1_MBTS_4_C_UNPAIRED_ISO = false;
    TAV_L1_XE300 = false;
    TAV_L1_RD1_BGRP10 = false;
    TAV_L1_AFP_C_ZDC_C = false;
    TAV_L1_AFP_C_J12 = false;
    TAV_L1_AFP_C_EM3 = false;
    TAV_L1_AFP_C_TE5 = false;
    TAV_L1_AFP_C_ALFA_C = false;
    TAV_L1_MBTS_4_A = false;
    TAV_L1_MBTS_4_C = false;
    TAV_L1_MBTS_1_BGRP9 = false;
    TAV_L1_MBTS_2_BGRP9 = false;
    TAV_L1_MBTS_1_BGRP11 = false;
    TAV_L1_MBTS_2_BGRP11 = false;
    TAV_L1_RD0_FILLED = false;
    TAV_L1_RD0_UNPAIRED_ISO = false;
    TAV_L1_RD0_EMPTY = false;
    TAV_L1_RD1_FILLED = false;
    TAV_L1_RD1_EMPTY = false;
    TAV_L1_RD2_FILLED = false;
    TAV_L1_RD2_EMPTY = false;
    TAV_L1_RD3_FILLED = false;
    TAV_L1_RD3_EMPTY = false;
    TAV_L1_RD0_FIRSTEMPTY = false;
    TAV_L1_RD0_BGRP9 = false;
    TAV_L1_RD0_BGRP11 = false;
    TAV_L1_LUCID = false;
    TAV_L1_LUCID_EMPTY = false;
    TAV_L1_LUCID_UNPAIRED_ISO = false;
    TAV_L1_LUCID_A_C_EMPTY = false;
    TAV_L1_LUCID_A_C_UNPAIRED_ISO = false;
    TAV_L1_LUCID_A_C_UNPAIRED_NONISO = false;
    TAV_L1_TRT_FILLED = false;
    TAV_L1_TRT_EMPTY = false;
    TAV_L1_TGC_BURST = false;
    TAV_L1_LHCF = false;
    TAV_L1_BCM_Wide_BGRP0 = false;
    TAV_L1_BCM_AC_CA_BGRP0 = false;
    TAV_L1_BCM_Wide_EMPTY = false;
    TAV_L1_BCM_Wide_UNPAIRED_ISO = false;
    TAV_L1_MBTS_1 = false;
    TAV_L1_MBTS_2 = false;
    TAV_L1_MBTS_1_1 = false;
    TAV_L1_BCM_CA_UNPAIRED_ISO = false;
    TAV_L1_BCM_AC_UNPAIRED_NONISO = false;
    TAV_L1_BCM_CA_UNPAIRED_NONISO = false;
    TAV_L1_BCM_AC_ABORTGAPNOTCALIB = false;
    TAV_L1_BCM_CA_ABORTGAPNOTCALIB = false;
    TAV_L1_BCM_Wide_ABORTGAPNOTCALIB = false;
    TAV_L1_BCM_AC_CALIB = false;
    TAV_L1_BCM_CA_CALIB = false;
    TAV_L1_BCM_Wide_CALIB = false;
    TAV_L1_BTAG_MU4J15 = false;
    TAV_L1_BTAG_MU4J30 = false;
    TAV_L1_ZB = false;
    TAV_L1_BPTX0_BGRP0 = false;
    TAV_L1_BPTX1_BGRP0 = false;
    TAV_L1_BTAG_MU6J20 = false;
    TAV_L1_BTAG_MU6J25 = false;
    TAV_L1_AFP_C_ANY_UNPAIRED_ISO = false;
    TAV_L1_3J15_BTAG_MU4J15 = false;
    TAV_L1_3J15_BTAG_MU4J30 = false;
    TAV_L1_3J15_BTAG_MU6J25 = false;
    TAV_L1_3J20_BTAG_MU4J20 = false;
    TAV_L1_J40_DPHI_Js2XE50 = false;
    TAV_L1_J40_DPHI_J20s2XE50 = false;
    TAV_L1_J40_DPHI_J20XE50 = false;
    TAV_L1_J40_DPHI_CJ20XE50 = false;
    TAV_L1_TAU40_2TAU20IM = false;
    TAV_L1_MU10_2J15_J20 = false;
    TAV_L1_MU11 = false;
    TAV_L1_AFP_C_ANY_UNPAIRED_NONISO = false;
    TAV_L1_HT190_J15_ETA21 = false;
    TAV_L1_HT190_J15s5_ETA21 = false;
    TAV_L1_HT150_J20_ETA31 = false;
    TAV_L1_HT150_J20s5_ETA31 = false;
    TAV_L1_JPSI_1M5 = false;
    TAV_L1_JPSI_1M5_EM7 = false;
    TAV_L1_JPSI_1M5_EM12 = false;
    TAV_L1_KF_XE35 = false;
    TAV_L1_KF_XE45 = false;
    TAV_L1_KF_XE55 = false;
    TAV_L1_KF_XE60 = false;
    TAV_L1_KF_XE65 = false;
    TAV_L1_KF_XE75 = false;
    TAV_L1_AFP_C_ANY_EMPTY = false;
    TAV_L1_AFP_C_ANY_FIRSTEMPTY = false;
    TAV_L1_AFP_C_AND = false;
    TAV_L1_EM12_W_MT25 = false;
    TAV_L1_EM12_W_MT30 = false;
    TAV_L1_EM15_W_MT35_W_250RO2_XEHT_0_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    TAV_L1_W_05RO_XEHT_0 = false;
    TAV_L1_MU10_2J20 = false;
    TAV_L1_W_90RO2_XEHT_0 = false;
    TAV_L1_W_250RO2_XEHT_0 = false;
    TAV_L1_W_HT20_JJ15_ETA49 = false;
    TAV_L1_W_NOMATCH = false;
    TAV_L1_W_NOMATCH_W_05RO_XEEMHT = false;
    TAV_L1_EM15_W_MT35_XS60_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    TAV_L1_EM15_W_MT35_XS40_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    TAV_L1_EM15_W_MT35 = false;
    TAV_L1_EM15_W_MT35_XS60 = false;
    TAV_L1_EM15VH_W_MT35_XS60 = false;
    TAV_L1_EM20VH_W_MT35_XS60 = false;
    TAV_L1_EM22VHI_W_MT35_XS40 = false;
    TAV_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE_XS30 = false;
    TAV_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE = false;
    TAV_L1_HT150_JJ15_ETA49_MJJ_400 = false;
    TAV_L1_RD2_BGRP14 = false;
    TAV_L1_BPH_1M19_2MU4_BPH_0DR34_2MU4 = false;
    TAV_L1_RD3_BGRP15 = false;
    TAV_L1_BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4 = false;
    TAV_L1_BPH_2M9_2MU6_BPH_2DR15_2MU6 = false;
    TAV_L1_MU6MU4_BO = false;
    TAV_L1_2MU4_B = false;
    TAV_L1_2MU6_B = false;
    TAV_L1_BPH_1M19_2MU4_B_BPH_0DR34_2MU4 = false;
    TAV_L1_BPH_1M19_2MU4_BO_BPH_0DR34_2MU4 = false;
    TAV_L1_BPH_2M8_MU6MU4_B_BPH_0DR15_MU6MU4 = false;
    TAV_L1_2MU4_BO = false;
    TAV_L1_2MU6_BO = false;
    TAV_L1_MU6_2MU4_B = false;
    TAV_L1_DY_DR_2MU4 = false;
    TAV_L1_DY_BOX_2MU4 = false;
    TAV_L1_DY_BOX_MU6MU4 = false;
    TAV_L1_DY_BOX_2MU6 = false;
    TAV_L1_LFV_MU = false;
    TAV_L1_LFV_EM8I = false;
    TAV_L1_LFV_EM15I = false;
    TAV_L1_DPHI_Js2XE50 = false;
    TAV_L1_DPHI_J20s2XE50 = false;
    TAV_L1_DPHI_J20XE50 = false;
    TAV_L1_DPHI_CJ20XE50 = false;
    TAV_L1_MJJ_900 = false;
    TAV_L1_MJJ_800 = false;
    TAV_L1_MJJ_700 = false;
    TAV_L1_MJJ_400 = false;
    TAV_L1_MJJ_100 = false;
    TAV_L1_HT150_JJ15_ETA49 = false;
    TAV_L1_DETA_JJ = false;
    TAV_L1_J4_MATCH = false;
    TAV_L1_LLP_NOMATCH = false;
    TAV_L1_DR_MU10TAU12I = false;
    TAV_L1_TAU30_EMPTY = false;
    TAV_L1_EM15_TAU40 = false;
    TAV_L1_TAU30_UNPAIRED_ISO = false;
    TAV_L1_EM15_TAU12I = false;
    TAV_L1_EM15TAU12I_J25 = false;
    TAV_L1_DR_EM15TAU12I_J25 = false;
    TAV_L1_TAU20ITAU12I_J25 = false;
    TAV_L1_DR_TAU20ITAU12I = false;
    TAV_L1_BOX_TAU20ITAU12I = false;
    TAV_L1_DR_TAU20ITAU12I_J25 = false;
    TAV_L1_LAR_EM = false;
    TAV_L1_LAR_J = false;
    TAV_L1_MU6_MJJ_200 = false;
    TAV_L1_MU6_MJJ_300 = false;
    TAV_L1_MU6_MJJ_400 = false;
    TAV_L1_MU6_MJJ_500 = false;
    TAV_L1_J30_2J20_4J20_0ETA49_MJJ_400 = false;
    TAV_L1_J30_2J20_4J20_0ETA49_MJJ_700 = false;
    TAV_L1_J30_2J20_4J20_0ETA49_MJJ_800 = false;
    TAV_L1_J30_2J20_4J20_0ETA49_MJJ_900 = false;
    TAV_L1_3J20_4J20_0ETA49_MJJ_400 = false;
    TAV_L1_3J20_4J20_0ETA49_MJJ_700 = false;
    TAV_L1_3J20_4J20_0ETA49_MJJ_800 = false;
    TAV_L1_3J20_4J20_0ETA49_MJJ_900 = false;
    TAV_L1_XE35_MJJ_200 = false;
    TAV_L1_EM7_FIRSTEMPTY = false;
    TAV_L1_RD0_ABORTGAPNOTCALIB = false;
    TAV_L1_3J25_0ETA23 = false;
    TAV_L1_TE20 = false;
    TAV_L1_TE10_0ETA24 = false;
    TAV_L1_TE20_0ETA24 = false;
    TAV_L1_XS40 = false;
    TAV_L1_XS50 = false;
    TAV_L1_XS60 = false;
    TAV_L1_J12_BGRP12 = false;
    TAV_L1_J30_31ETA49_BGRP12 = false;
    TAV_L1_MU6_J30_0ETA49_2J20_0ETA49 = false;
    TAV_L1_4J20_0ETA49 = false;
    TAV_L1_TAU8_UNPAIRED_ISO = false;
    TAV_L1_EM7_UNPAIRED_ISO = false;
    TAV_L1_RD2_BGRP12 = false;
    TAV_L1_TAU8_FIRSTEMPTY = false;
    TAV_L1_EM24VHI = false;
    TAV_L1_LHCF_UNPAIRED_ISO = false;
    TAV_L1_LHCF_EMPTY = false;
    TAV_L1_EM15VH_2EM10VH_3EM7 = false;
    TAV_L1_EM15VH_3EM8VH = false;
    TAV_L1_EM8I_MU10 = false;
    TAV_L1_EM15HI = false;
    TAV_L1_MU6_3MU4 = false;
    TAV_L1_2MU6_3MU4 = false;
    TAV_L1_BGRP9 = false;
    TAV_L1_TE50 = false;
    TAV_L1_TE5 = false;
    TAV_L1_TE60 = false;
    TAV_L1_TE50_0ETA24 = false;
    TAV_L1_TE5_0ETA24 = false;
    TAV_L1_EM20VH_FIRSTEMPTY = false;
    TAV_L1_EM22VHI_FIRSTEMPTY = false;
    TAV_L1_MU20_FIRSTEMPTY = false;
    TAV_L1_J100_FIRSTEMPTY = false;
    TAV_L1_J100_31ETA49_FIRSTEMPTY = false;
    TAV_L1_TE60_0ETA24 = false;
    TAV_L1_TE70_0ETA24 = false;
    TAV_L1_TE40_0ETA24 = false;
    TAV_L1_ZDC_A = false;
    TAV_L1_ZDC_C = false;
    TAV_L1_ZDC_AND = false;
    TAV_L1_ZDC_A_C = false;
    TAV_L1_ALFA_ELAST1 = false;
    TAV_L1_ALFA_ELAST2 = false;
    TAV_L1_ALFA_ELAST11 = false;
    TAV_L1_ALFA_ELAST12 = false;
    TAV_L1_ALFA_ELAST13 = false;
    TAV_L1_ALFA_ELAST14 = false;
    TAV_L1_ALFA_ELAST15 = false;
    TAV_L1_ALFA_ELAST15_Calib = false;
    TAV_L1_ALFA_ELAST16 = false;
    TAV_L1_ALFA_ELAST17 = false;
    TAV_L1_ALFA_ELAST18 = false;
    TAV_L1_ALFA_ELAST18_Calib = false;
    TAV_L1_ALFA_SDIFF5 = false;
    TAV_L1_ALFA_SDIFF6 = false;
    TAV_L1_ALFA_SDIFF7 = false;
    TAV_L1_ALFA_SDIFF8 = false;
    TAV_L1_MBTS_1_A_ALFA_C = false;
    TAV_L1_MBTS_1_C_ALFA_A = false;
    TAV_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO = false;
    TAV_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO = false;
    TAV_L1_MBTS_2_A_ALFA_C = false;
    TAV_L1_MBTS_2_C_ALFA_A = false;
    TAV_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO = false;
    TAV_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO = false;
    TAV_L1_LUCID_A_ALFA_C = false;
    TAV_L1_LUCID_C_ALFA_A = false;
    TAV_L1_LUCID_A_ALFA_C_UNPAIRED_ISO = false;
    TAV_L1_LUCID_C_ALFA_A_UNPAIRED_ISO = false;
    TAV_L1_EM3_ALFA_ANY = false;
    TAV_L1_EM3_ALFA_ANY_UNPAIRED_ISO = false;
    TAV_L1_EM3_ALFA_EINE = false;
    TAV_L1_ALFA_ELASTIC_UNPAIRED_ISO = false;
    TAV_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO = false;
    TAV_L1_ALFA_ANY_A_EMPTY = false;
    TAV_L1_ALFA_ANY_C_EMPTY = false;
    TAV_L1_J12_ALFA_ANY = false;
    TAV_L1_J12_ALFA_ANY_UNPAIRED_ISO = false;
    TAV_L1_TE5_ALFA_ANY = false;
    TAV_L1_TE5_ALFA_ANY_UNPAIRED_ISO = false;
    TAV_L1_TE5_ALFA_EINE = false;
    TAV_L1_TRT_ALFA_ANY = false;
    TAV_L1_TRT_ALFA_ANY_UNPAIRED_ISO = false;
    TAV_L1_TRT_ALFA_EINE = false;
    TAV_L1_TAU8_UNPAIRED_NONISO = false;
    TAV_L1_EM7_UNPAIRED_NONISO = false;
    TAV_L1_MU4_UNPAIRED_NONISO = false;
    TAV_L1_ALFA_BGT = false;
    TAV_L1_ALFA_BGT_UNPAIRED_ISO = false;
    TAV_L1_ALFA_BGT_BGRP10 = false;
    TAV_L1_ALFA_SHOWSYST5 = false;
    TAV_L1_ALFA_SYST9 = false;
    TAV_L1_ALFA_SYST10 = false;
    TAV_L1_ALFA_SYST11 = false;
    TAV_L1_ALFA_SYST12 = false;
    TAV_L1_ALFA_SYST17 = false;
    TAV_L1_ALFA_SYST18 = false;
    TAV_L1_ALFA_ANY = false;
    TAV_L1_ALFA_ANY_EMPTY = false;
    TAV_L1_ALFA_ANY_FIRSTEMPTY = false;
    TAV_L1_ALFA_ANY_UNPAIRED_ISO = false;
    TAV_L1_ALFA_ANY_UNPAIRED_NONISO = false;
    TAV_L1_ALFA_ANY_BGRP10 = false;
    TAV_L1_ALFA_ANY_ABORTGAPNOTCALIB = false;
    TAV_L1_ALFA_ANY_CALIB = false;
    TAV_L1_ALFA_B7L1U = false;
    TAV_L1_ALFA_B7L1L = false;
    TAV_L1_ALFA_A7L1U = false;
    TAV_L1_ALFA_A7L1L = false;
    TAV_L1_ALFA_A7R1U = false;
    TAV_L1_ALFA_A7R1L = false;
    TAV_L1_ALFA_B7R1U = false;
    TAV_L1_ALFA_B7R1L = false;
    TAV_L1_ALFA_B7L1U_OD = false;
    TAV_L1_ALFA_B7L1L_OD = false;
    TAV_L1_ALFA_A7L1U_OD = false;
    TAV_L1_ALFA_A7L1L_OD = false;
    TAV_L1_ALFA_A7R1U_OD = false;
    TAV_L1_ALFA_A7R1L_OD = false;
    TAV_L1_ALFA_B7R1U_OD = false;
    TAV_L1_ALFA_B7R1L_OD = false;
    TAV_L1_ALFA_B7L1_OD = false;
    TAV_L1_ALFA_A7L1_OD = false;
    TAV_L1_ALFA_B7R1_OD = false;
    TAV_L1_ALFA_A7R1_OD = false;
    TAV_L1_CALREQ2 = false;

    PS_L1_EM13VH = -42;
    PS_L1_EM15 = -42;
    PS_L1_EM15VH = -42;
    PS_L1_EM18VH = -42;
    PS_L1_EM20VH = -42;
    PS_L1_EM20VHI = -42;
    PS_L1_EM22VHI = -42;
    PS_L1_EM3_EMPTY = -42;
    PS_L1_EM7_EMPTY = -42;
    PS_L1_MU4 = -42;
    PS_L1_MU6 = -42;
    PS_L1_MU10 = -42;
    PS_L1_MU15 = -42;
    PS_L1_MU20 = -42;
    PS_L1_MU4_EMPTY = -42;
    PS_L1_MU4_FIRSTEMPTY = -42;
    PS_L1_MU11_EMPTY = -42;
    PS_L1_MU4_UNPAIRED_ISO = -42;
    PS_L1_2EM3 = -42;
    PS_L1_2EM7 = -42;
    PS_L1_2EM10VH = -42;
    PS_L1_2EM13VH = -42;
    PS_L1_2EM15 = -42;
    PS_L1_2EM15VH = -42;
    PS_L1_EM7_2EM3 = -42;
    PS_L1_EM12_2EM3 = -42;
    PS_L1_EM15VH_3EM7 = -42;
    PS_L1_2MU4 = -42;
    PS_L1_2MU6 = -42;
    PS_L1_2MU10 = -42;
    PS_L1_2MU20_OVERLAY = -42;
    PS_L1_MU10_2MU6 = -42;
    PS_L1_MU11_2MU6 = -42;
    PS_L1_3MU4 = -42;
    PS_L1_MU6_2MU4 = -42;
    PS_L1_3MU6 = -42;
    PS_L1_4J15_0ETA25 = -42;
    PS_L1_EM15I_MU4 = -42;
    PS_L1_2EM8VH_MU10 = -42;
    PS_L1_EM15VH_MU10 = -42;
    PS_L1_TAU12 = -42;
    PS_L1_TAU12IL = -42;
    PS_L1_TAU12IM = -42;
    PS_L1_TAU12IT = -42;
    PS_L1_TAU20 = -42;
    PS_L1_TAU20IL = -42;
    PS_L1_TAU20IM = -42;
    PS_L1_TAU20IT = -42;
    PS_L1_TAU30 = -42;
    PS_L1_TAU40 = -42;
    PS_L1_TAU60 = -42;
    PS_L1_TAU8 = -42;
    PS_L1_TAU8_EMPTY = -42;
    PS_L1_TAU20IM_2TAU12IM = -42;
    PS_L1_TAU20_2TAU12 = -42;
    PS_L1_EM15HI_2TAU12IM = -42;
    PS_L1_EM15HI_2TAU12IM_J25_3J12 = -42;
    PS_L1_EM15HI_TAU40_2TAU15 = -42;
    PS_L1_MU10_TAU12IM = -42;
    PS_L1_MU10_TAU12IM_J25_2J12 = -42;
    PS_L1_EM7_MU10 = -42;
    PS_L1_MU6_EMPTY = -42;
    PS_L1_MU10_TAU20IM = -42;
    PS_L1_XE10 = -42;
    PS_L1_TAU20IL_2TAU12IL_J25_2J20_3J12 = -42;
    PS_L1_TAU20IM_2TAU12IM_J25_2J20_3J12 = -42;
    PS_L1_J25_2J20_3J12_BOX_TAU20ITAU12I = -42;
    PS_L1_DR_MU10TAU12I_TAU12I_J25 = -42;
    PS_L1_MU10_TAU12I_J25 = -42;
    PS_L1_TAU20IM_2J20_XE45 = -42;
    PS_L1_J15_31ETA49_UNPAIRED_ISO = -42;
    PS_L1_XE20 = -42;
    PS_L1_XE45_TAU20_J20 = -42;
    PS_L1_EM15HI_2TAU12IM_XE35 = -42;
    PS_L1_XE35_EM15_TAU12I = -42;
    PS_L1_XE40_EM15_TAU12I = -42;
    PS_L1_MU10_TAU12IM_XE35 = -42;
    PS_L1_XE25 = -42;
    PS_L1_TAU20IM_2TAU12IM_XE35 = -42;
    PS_L1_TAU20_2TAU12_XE35 = -42;
    PS_L1_XE30 = -42;
    PS_L1_EM15VH_JJ15_23ETA49 = -42;
    PS_L1_MU4_J12 = -42;
    PS_L1_MU6_J20 = -42;
    PS_L1_MU6_J40 = -42;
    PS_L1_MU6_J75 = -42;
    PS_L1_J12 = -42;
    PS_L1_J15 = -42;
    PS_L1_J20 = -42;
    PS_L1_J25 = -42;
    PS_L1_J30 = -42;
    PS_L1_J40 = -42;
    PS_L1_J50 = -42;
    PS_L1_J75 = -42;
    PS_L1_J85 = -42;
    PS_L1_J100 = -42;
    PS_L1_J120 = -42;
    PS_L1_J400 = -42;
    PS_L1_J20_31ETA49 = -42;
    PS_L1_J30_31ETA49 = -42;
    PS_L1_J50_31ETA49 = -42;
    PS_L1_J75_31ETA49 = -42;
    PS_L1_J100_31ETA49 = -42;
    PS_L1_XE65 = -42;
    PS_L1_J15_31ETA49 = -42;
    PS_L1_J20_28ETA31 = -42;
    PS_L1_J12_EMPTY = -42;
    PS_L1_J12_FIRSTEMPTY = -42;
    PS_L1_J12_UNPAIRED_ISO = -42;
    PS_L1_J12_UNPAIRED_NONISO = -42;
    PS_L1_J12_ABORTGAPNOTCALIB = -42;
    PS_L1_J30_EMPTY = -42;
    PS_L1_J30_FIRSTEMPTY = -42;
    PS_L1_J30_31ETA49_EMPTY = -42;
    PS_L1_J30_31ETA49_UNPAIRED_ISO = -42;
    PS_L1_J30_31ETA49_UNPAIRED_NONISO = -42;
    PS_L1_J50_UNPAIRED_ISO = -42;
    PS_L1_J50_UNPAIRED_NONISO = -42;
    PS_L1_J50_ABORTGAPNOTCALIB = -42;
    PS_L1_AFP_NSC = -42;
    PS_L1_J20_J20_31ETA49 = -42;
    PS_L1_3J15 = -42;
    PS_L1_3J20 = -42;
    PS_L1_3J40 = -42;
    PS_L1_3J15_0ETA25 = -42;
    PS_L1_3J50 = -42;
    PS_L1_4J15 = -42;
    PS_L1_4J20 = -42;
    PS_L1_J75_XE50 = -42;
    PS_L1_XE75 = -42;
    PS_L1_6J15 = -42;
    PS_L1_J75_3J20 = -42;
    PS_L1_J30_0ETA49_2J20_0ETA49 = -42;
    PS_L1_TE10 = -42;
    PS_L1_AFP_FSC = -42;
    PS_L1_5J15_0ETA25 = -42;
    PS_L1_2J15_XE55 = -42;
    PS_L1_J40_XE50 = -42;
    PS_L1_J75_XE40 = -42;
    PS_L1_XE35 = -42;
    PS_L1_XE40 = -42;
    PS_L1_XE45 = -42;
    PS_L1_XE50 = -42;
    PS_L1_XE55 = -42;
    PS_L1_XE60 = -42;
    PS_L1_XE70 = -42;
    PS_L1_XE80 = -42;
    PS_L1_XS20 = -42;
    PS_L1_XS30 = -42;
    PS_L1_EM12_XS20 = -42;
    PS_L1_EM15_XS30 = -42;
    PS_L1_XE150 = -42;
    PS_L1_TE30 = -42;
    PS_L1_TE40 = -42;
    PS_L1_TE70 = -42;
    PS_L1_TE30_0ETA24 = -42;
    PS_L1_BCM_Wide_UNPAIRED_NONISO = -42;
    PS_L1_BCM_AC_CA_UNPAIRED_ISO = -42;
    PS_L1_BCM_AC_UNPAIRED_ISO = -42;
    PS_L1_MBTS_1_EMPTY = -42;
    PS_L1_MBTS_1_UNPAIRED_ISO = -42;
    PS_L1_MBTS_2_EMPTY = -42;
    PS_L1_MBTS_2_UNPAIRED_ISO = -42;
    PS_L1_MBTS_1_1_EMPTY = -42;
    PS_L1_MBTS_1_1_UNPAIRED_ISO = -42;
    PS_L1_AFP_C_ALFA_A = -42;
    PS_L1_AFP_C_ANY = -42;
    PS_L1_EM13VH_3J20 = -42;
    PS_L1_MU10_3J20 = -42;
    PS_L1_AFP_C_ANY_MBTS_A = -42;
    PS_L1_2J50_XE40 = -42;
    PS_L1_J40_XE60 = -42;
    PS_L1_J40_0ETA25_XE50 = -42;
    PS_L1_BPH_2M8_2MU4 = -42;
    PS_L1_BPH_8M15_MU6MU4 = -42;
    PS_L1_BPH_8M15_2MU6 = -42;
    PS_L1_J40_0ETA25_2J15_31ETA49 = -42;
    PS_L1_J40_0ETA25_2J25_J20_31ETA49 = -42;
    PS_L1_AFP_C_MBTS_A = -42;
    PS_L1_TGC_BURST_EMPTY = -42;
    PS_L1_MBTS_4_A_UNPAIRED_ISO = -42;
    PS_L1_MBTS_4_C_UNPAIRED_ISO = -42;
    PS_L1_XE300 = -42;
    PS_L1_RD1_BGRP10 = -42;
    PS_L1_AFP_C_ZDC_C = -42;
    PS_L1_AFP_C_J12 = -42;
    PS_L1_AFP_C_EM3 = -42;
    PS_L1_AFP_C_TE5 = -42;
    PS_L1_AFP_C_ALFA_C = -42;
    PS_L1_MBTS_4_A = -42;
    PS_L1_MBTS_4_C = -42;
    PS_L1_MBTS_1_BGRP9 = -42;
    PS_L1_MBTS_2_BGRP9 = -42;
    PS_L1_MBTS_1_BGRP11 = -42;
    PS_L1_MBTS_2_BGRP11 = -42;
    PS_L1_RD0_FILLED = -42;
    PS_L1_RD0_UNPAIRED_ISO = -42;
    PS_L1_RD0_EMPTY = -42;
    PS_L1_RD1_FILLED = -42;
    PS_L1_RD1_EMPTY = -42;
    PS_L1_RD2_FILLED = -42;
    PS_L1_RD2_EMPTY = -42;
    PS_L1_RD3_FILLED = -42;
    PS_L1_RD3_EMPTY = -42;
    PS_L1_RD0_FIRSTEMPTY = -42;
    PS_L1_RD0_BGRP9 = -42;
    PS_L1_RD0_BGRP11 = -42;
    PS_L1_LUCID = -42;
    PS_L1_LUCID_EMPTY = -42;
    PS_L1_LUCID_UNPAIRED_ISO = -42;
    PS_L1_LUCID_A_C_EMPTY = -42;
    PS_L1_LUCID_A_C_UNPAIRED_ISO = -42;
    PS_L1_LUCID_A_C_UNPAIRED_NONISO = -42;
    PS_L1_TRT_FILLED = -42;
    PS_L1_TRT_EMPTY = -42;
    PS_L1_TGC_BURST = -42;
    PS_L1_LHCF = -42;
    PS_L1_BCM_Wide_BGRP0 = -42;
    PS_L1_BCM_AC_CA_BGRP0 = -42;
    PS_L1_BCM_Wide_EMPTY = -42;
    PS_L1_BCM_Wide_UNPAIRED_ISO = -42;
    PS_L1_MBTS_1 = -42;
    PS_L1_MBTS_2 = -42;
    PS_L1_MBTS_1_1 = -42;
    PS_L1_BCM_CA_UNPAIRED_ISO = -42;
    PS_L1_BCM_AC_UNPAIRED_NONISO = -42;
    PS_L1_BCM_CA_UNPAIRED_NONISO = -42;
    PS_L1_BCM_AC_ABORTGAPNOTCALIB = -42;
    PS_L1_BCM_CA_ABORTGAPNOTCALIB = -42;
    PS_L1_BCM_Wide_ABORTGAPNOTCALIB = -42;
    PS_L1_BCM_AC_CALIB = -42;
    PS_L1_BCM_CA_CALIB = -42;
    PS_L1_BCM_Wide_CALIB = -42;
    PS_L1_BTAG_MU4J15 = -42;
    PS_L1_BTAG_MU4J30 = -42;
    PS_L1_ZB = -42;
    PS_L1_BPTX0_BGRP0 = -42;
    PS_L1_BPTX1_BGRP0 = -42;
    PS_L1_BTAG_MU6J20 = -42;
    PS_L1_BTAG_MU6J25 = -42;
    PS_L1_AFP_C_ANY_UNPAIRED_ISO = -42;
    PS_L1_3J15_BTAG_MU4J15 = -42;
    PS_L1_3J15_BTAG_MU4J30 = -42;
    PS_L1_3J15_BTAG_MU6J25 = -42;
    PS_L1_3J20_BTAG_MU4J20 = -42;
    PS_L1_J40_DPHI_Js2XE50 = -42;
    PS_L1_J40_DPHI_J20s2XE50 = -42;
    PS_L1_J40_DPHI_J20XE50 = -42;
    PS_L1_J40_DPHI_CJ20XE50 = -42;
    PS_L1_TAU40_2TAU20IM = -42;
    PS_L1_MU10_2J15_J20 = -42;
    PS_L1_MU11 = -42;
    PS_L1_AFP_C_ANY_UNPAIRED_NONISO = -42;
    PS_L1_HT190_J15_ETA21 = -42;
    PS_L1_HT190_J15s5_ETA21 = -42;
    PS_L1_HT150_J20_ETA31 = -42;
    PS_L1_HT150_J20s5_ETA31 = -42;
    PS_L1_JPSI_1M5 = -42;
    PS_L1_JPSI_1M5_EM7 = -42;
    PS_L1_JPSI_1M5_EM12 = -42;
    PS_L1_KF_XE35 = -42;
    PS_L1_KF_XE45 = -42;
    PS_L1_KF_XE55 = -42;
    PS_L1_KF_XE60 = -42;
    PS_L1_KF_XE65 = -42;
    PS_L1_KF_XE75 = -42;
    PS_L1_AFP_C_ANY_EMPTY = -42;
    PS_L1_AFP_C_ANY_FIRSTEMPTY = -42;
    PS_L1_AFP_C_AND = -42;
    PS_L1_EM12_W_MT25 = -42;
    PS_L1_EM12_W_MT30 = -42;
    PS_L1_EM15_W_MT35_W_250RO2_XEHT_0_W_05DPHI_JXE_0_W_05DPHI_EM15XE = -42;
    PS_L1_W_05RO_XEHT_0 = -42;
    PS_L1_MU10_2J20 = -42;
    PS_L1_W_90RO2_XEHT_0 = -42;
    PS_L1_W_250RO2_XEHT_0 = -42;
    PS_L1_W_HT20_JJ15_ETA49 = -42;
    PS_L1_W_NOMATCH = -42;
    PS_L1_W_NOMATCH_W_05RO_XEEMHT = -42;
    PS_L1_EM15_W_MT35_XS60_W_05DPHI_JXE_0_W_05DPHI_EM15XE = -42;
    PS_L1_EM15_W_MT35_XS40_W_05DPHI_JXE_0_W_05DPHI_EM15XE = -42;
    PS_L1_EM15_W_MT35 = -42;
    PS_L1_EM15_W_MT35_XS60 = -42;
    PS_L1_EM15VH_W_MT35_XS60 = -42;
    PS_L1_EM20VH_W_MT35_XS60 = -42;
    PS_L1_EM22VHI_W_MT35_XS40 = -42;
    PS_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE_XS30 = -42;
    PS_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE = -42;
    PS_L1_HT150_JJ15_ETA49_MJJ_400 = -42;
    PS_L1_RD2_BGRP14 = -42;
    PS_L1_BPH_1M19_2MU4_BPH_0DR34_2MU4 = -42;
    PS_L1_RD3_BGRP15 = -42;
    PS_L1_BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4 = -42;
    PS_L1_BPH_2M9_2MU6_BPH_2DR15_2MU6 = -42;
    PS_L1_MU6MU4_BO = -42;
    PS_L1_2MU4_B = -42;
    PS_L1_2MU6_B = -42;
    PS_L1_BPH_1M19_2MU4_B_BPH_0DR34_2MU4 = -42;
    PS_L1_BPH_1M19_2MU4_BO_BPH_0DR34_2MU4 = -42;
    PS_L1_BPH_2M8_MU6MU4_B_BPH_0DR15_MU6MU4 = -42;
    PS_L1_2MU4_BO = -42;
    PS_L1_2MU6_BO = -42;
    PS_L1_MU6_2MU4_B = -42;
    PS_L1_DY_DR_2MU4 = -42;
    PS_L1_DY_BOX_2MU4 = -42;
    PS_L1_DY_BOX_MU6MU4 = -42;
    PS_L1_DY_BOX_2MU6 = -42;
    PS_L1_LFV_MU = -42;
    PS_L1_LFV_EM8I = -42;
    PS_L1_LFV_EM15I = -42;
    PS_L1_DPHI_Js2XE50 = -42;
    PS_L1_DPHI_J20s2XE50 = -42;
    PS_L1_DPHI_J20XE50 = -42;
    PS_L1_DPHI_CJ20XE50 = -42;
    PS_L1_MJJ_900 = -42;
    PS_L1_MJJ_800 = -42;
    PS_L1_MJJ_700 = -42;
    PS_L1_MJJ_400 = -42;
    PS_L1_MJJ_100 = -42;
    PS_L1_HT150_JJ15_ETA49 = -42;
    PS_L1_DETA_JJ = -42;
    PS_L1_J4_MATCH = -42;
    PS_L1_LLP_NOMATCH = -42;
    PS_L1_DR_MU10TAU12I = -42;
    PS_L1_TAU30_EMPTY = -42;
    PS_L1_EM15_TAU40 = -42;
    PS_L1_TAU30_UNPAIRED_ISO = -42;
    PS_L1_EM15_TAU12I = -42;
    PS_L1_EM15TAU12I_J25 = -42;
    PS_L1_DR_EM15TAU12I_J25 = -42;
    PS_L1_TAU20ITAU12I_J25 = -42;
    PS_L1_DR_TAU20ITAU12I = -42;
    PS_L1_BOX_TAU20ITAU12I = -42;
    PS_L1_DR_TAU20ITAU12I_J25 = -42;
    PS_L1_LAR_EM = -42;
    PS_L1_LAR_J = -42;
    PS_L1_MU6_MJJ_200 = -42;
    PS_L1_MU6_MJJ_300 = -42;
    PS_L1_MU6_MJJ_400 = -42;
    PS_L1_MU6_MJJ_500 = -42;
    PS_L1_J30_2J20_4J20_0ETA49_MJJ_400 = -42;
    PS_L1_J30_2J20_4J20_0ETA49_MJJ_700 = -42;
    PS_L1_J30_2J20_4J20_0ETA49_MJJ_800 = -42;
    PS_L1_J30_2J20_4J20_0ETA49_MJJ_900 = -42;
    PS_L1_3J20_4J20_0ETA49_MJJ_400 = -42;
    PS_L1_3J20_4J20_0ETA49_MJJ_700 = -42;
    PS_L1_3J20_4J20_0ETA49_MJJ_800 = -42;
    PS_L1_3J20_4J20_0ETA49_MJJ_900 = -42;
    PS_L1_XE35_MJJ_200 = -42;
    PS_L1_EM7_FIRSTEMPTY = -42;
    PS_L1_RD0_ABORTGAPNOTCALIB = -42;
    PS_L1_3J25_0ETA23 = -42;
    PS_L1_TE20 = -42;
    PS_L1_TE10_0ETA24 = -42;
    PS_L1_TE20_0ETA24 = -42;
    PS_L1_XS40 = -42;
    PS_L1_XS50 = -42;
    PS_L1_XS60 = -42;
    PS_L1_J12_BGRP12 = -42;
    PS_L1_J30_31ETA49_BGRP12 = -42;
    PS_L1_MU6_J30_0ETA49_2J20_0ETA49 = -42;
    PS_L1_4J20_0ETA49 = -42;
    PS_L1_TAU8_UNPAIRED_ISO = -42;
    PS_L1_EM7_UNPAIRED_ISO = -42;
    PS_L1_RD2_BGRP12 = -42;
    PS_L1_TAU8_FIRSTEMPTY = -42;
    PS_L1_EM24VHI = -42;
    PS_L1_LHCF_UNPAIRED_ISO = -42;
    PS_L1_LHCF_EMPTY = -42;
    PS_L1_EM15VH_2EM10VH_3EM7 = -42;
    PS_L1_EM15VH_3EM8VH = -42;
    PS_L1_EM8I_MU10 = -42;
    PS_L1_EM15HI = -42;
    PS_L1_MU6_3MU4 = -42;
    PS_L1_2MU6_3MU4 = -42;
    PS_L1_BGRP9 = -42;
    PS_L1_TE50 = -42;
    PS_L1_TE5 = -42;
    PS_L1_TE60 = -42;
    PS_L1_TE50_0ETA24 = -42;
    PS_L1_TE5_0ETA24 = -42;
    PS_L1_EM20VH_FIRSTEMPTY = -42;
    PS_L1_EM22VHI_FIRSTEMPTY = -42;
    PS_L1_MU20_FIRSTEMPTY = -42;
    PS_L1_J100_FIRSTEMPTY = -42;
    PS_L1_J100_31ETA49_FIRSTEMPTY = -42;
    PS_L1_TE60_0ETA24 = -42;
    PS_L1_TE70_0ETA24 = -42;
    PS_L1_TE40_0ETA24 = -42;
    PS_L1_ZDC_A = -42;
    PS_L1_ZDC_C = -42;
    PS_L1_ZDC_AND = -42;
    PS_L1_ZDC_A_C = -42;
    PS_L1_ALFA_ELAST1 = -42;
    PS_L1_ALFA_ELAST2 = -42;
    PS_L1_ALFA_ELAST11 = -42;
    PS_L1_ALFA_ELAST12 = -42;
    PS_L1_ALFA_ELAST13 = -42;
    PS_L1_ALFA_ELAST14 = -42;
    PS_L1_ALFA_ELAST15 = -42;
    PS_L1_ALFA_ELAST15_Calib = -42;
    PS_L1_ALFA_ELAST16 = -42;
    PS_L1_ALFA_ELAST17 = -42;
    PS_L1_ALFA_ELAST18 = -42;
    PS_L1_ALFA_ELAST18_Calib = -42;
    PS_L1_ALFA_SDIFF5 = -42;
    PS_L1_ALFA_SDIFF6 = -42;
    PS_L1_ALFA_SDIFF7 = -42;
    PS_L1_ALFA_SDIFF8 = -42;
    PS_L1_MBTS_1_A_ALFA_C = -42;
    PS_L1_MBTS_1_C_ALFA_A = -42;
    PS_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO = -42;
    PS_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO = -42;
    PS_L1_MBTS_2_A_ALFA_C = -42;
    PS_L1_MBTS_2_C_ALFA_A = -42;
    PS_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO = -42;
    PS_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO = -42;
    PS_L1_LUCID_A_ALFA_C = -42;
    PS_L1_LUCID_C_ALFA_A = -42;
    PS_L1_LUCID_A_ALFA_C_UNPAIRED_ISO = -42;
    PS_L1_LUCID_C_ALFA_A_UNPAIRED_ISO = -42;
    PS_L1_EM3_ALFA_ANY = -42;
    PS_L1_EM3_ALFA_ANY_UNPAIRED_ISO = -42;
    PS_L1_EM3_ALFA_EINE = -42;
    PS_L1_ALFA_ELASTIC_UNPAIRED_ISO = -42;
    PS_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO = -42;
    PS_L1_ALFA_ANY_A_EMPTY = -42;
    PS_L1_ALFA_ANY_C_EMPTY = -42;
    PS_L1_J12_ALFA_ANY = -42;
    PS_L1_J12_ALFA_ANY_UNPAIRED_ISO = -42;
    PS_L1_TE5_ALFA_ANY = -42;
    PS_L1_TE5_ALFA_ANY_UNPAIRED_ISO = -42;
    PS_L1_TE5_ALFA_EINE = -42;
    PS_L1_TRT_ALFA_ANY = -42;
    PS_L1_TRT_ALFA_ANY_UNPAIRED_ISO = -42;
    PS_L1_TRT_ALFA_EINE = -42;
    PS_L1_TAU8_UNPAIRED_NONISO = -42;
    PS_L1_EM7_UNPAIRED_NONISO = -42;
    PS_L1_MU4_UNPAIRED_NONISO = -42;
    PS_L1_ALFA_BGT = -42;
    PS_L1_ALFA_BGT_UNPAIRED_ISO = -42;
    PS_L1_ALFA_BGT_BGRP10 = -42;
    PS_L1_ALFA_SHOWSYST5 = -42;
    PS_L1_ALFA_SYST9 = -42;
    PS_L1_ALFA_SYST10 = -42;
    PS_L1_ALFA_SYST11 = -42;
    PS_L1_ALFA_SYST12 = -42;
    PS_L1_ALFA_SYST17 = -42;
    PS_L1_ALFA_SYST18 = -42;
    PS_L1_ALFA_ANY = -42;
    PS_L1_ALFA_ANY_EMPTY = -42;
    PS_L1_ALFA_ANY_FIRSTEMPTY = -42;
    PS_L1_ALFA_ANY_UNPAIRED_ISO = -42;
    PS_L1_ALFA_ANY_UNPAIRED_NONISO = -42;
    PS_L1_ALFA_ANY_BGRP10 = -42;
    PS_L1_ALFA_ANY_ABORTGAPNOTCALIB = -42;
    PS_L1_ALFA_ANY_CALIB = -42;
    PS_L1_ALFA_B7L1U = -42;
    PS_L1_ALFA_B7L1L = -42;
    PS_L1_ALFA_A7L1U = -42;
    PS_L1_ALFA_A7L1L = -42;
    PS_L1_ALFA_A7R1U = -42;
    PS_L1_ALFA_A7R1L = -42;
    PS_L1_ALFA_B7R1U = -42;
    PS_L1_ALFA_B7R1L = -42;
    PS_L1_ALFA_B7L1U_OD = -42;
    PS_L1_ALFA_B7L1L_OD = -42;
    PS_L1_ALFA_A7L1U_OD = -42;
    PS_L1_ALFA_A7L1L_OD = -42;
    PS_L1_ALFA_A7R1U_OD = -42;
    PS_L1_ALFA_A7R1L_OD = -42;
    PS_L1_ALFA_B7R1U_OD = -42;
    PS_L1_ALFA_B7R1L_OD = -42;
    PS_L1_ALFA_B7L1_OD = -42;
    PS_L1_ALFA_A7L1_OD = -42;
    PS_L1_ALFA_B7R1_OD = -42;
    PS_L1_ALFA_A7R1_OD = -42;
    PS_L1_CALREQ2 = -42;

    // these things here are the bitmask for the trigger bit-variables. The trigger bits are stored compactly on bit level within a larger variable and need to be filtered out with that.
    const unsigned int TBPmask = 0x1 << 17;
    const unsigned int TAPmask = 0x1 << 16;
    const unsigned int TAVmask = 0x1 << 18;

    auto chainGroup = m_trigDecisionTool->getChainGroup("L1.*");

    std::map<std::string, int> triggerCounts;
    for (auto &trig : chainGroup->getListOfTriggers())
    {
        auto cg = m_trigDecisionTool->getChainGroup(trig);
        //~ std::cout << "test2 \n";
        std::string thisTrig = trig;
        //~ Info( "execute()", "%30s chain passed(1)/failed(0): %d total chain prescale (L1*HLT): %.1f", thisTrig.c_str(), cg->isPassed(), cg->getPrescale() );
        //~ std::cout << thisTrig.c_str() << "\n";
        //~ std::cout << thisTrig.c_str() << " " << cg->isPassed() << " " << cg->getPrescale() << "\n";
        //~ std::cout << "trigger after veto bit: " << cg->isPassed(TrigDefs::L1_isPassedAfterVeto) << "\n";
        //~ std::cout << "TBP : " << cg->isPassed(TrigDefs::L1_isPassedBeforePrescale) << "\n";
        //~ std::cout << "TAP : " << cg->isPassed(TrigDefs::L1_isPassedAfterPrescale) << "\n\n";
        //~ std::cout << "DEBUG PS_L1_ALFA_B7L1U : " << PS_L1_ALFA_B7L1U << "\n\n";

        {

            //~ if(thisTrig == "L1_EM10VH"							)                           L1_EM10VH  = cg->isPassed();   																			 cg->getPrescale()																cg->isPassed(TrigDefs::L1_isPassedAfterVeto)																					cg->isPassed(TrigDefs::L1_isPassedBeforePrescale)						cg->isPassed(TrigDefs::L1_isPassedAfterPrescale)

            if (thisTrig == "L1_EM13VH")
            {
                L1_EM13VH = cg->isPassed();
                PS_L1_EM13VH = cg->getPrescale();
                TAV_L1_EM13VH = cg->isPassedBits() & TAVmask;
                TBP_L1_EM13VH = cg->isPassedBits() & TBPmask;
                TAP_L1_EM13VH = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15")
            {
                L1_EM15 = cg->isPassed();
                PS_L1_EM15 = cg->getPrescale();
                TAV_L1_EM15 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15VH")
            {
                L1_EM15VH = cg->isPassed();
                PS_L1_EM15VH = cg->getPrescale();
                TAV_L1_EM15VH = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15VH = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15VH = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM18VH")
            {
                L1_EM18VH = cg->isPassed();
                PS_L1_EM18VH = cg->getPrescale();
                TAV_L1_EM18VH = cg->isPassedBits() & TAVmask;
                TBP_L1_EM18VH = cg->isPassedBits() & TBPmask;
                TAP_L1_EM18VH = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM20VH")
            {
                L1_EM20VH = cg->isPassed();
                PS_L1_EM20VH = cg->getPrescale();
                TAV_L1_EM20VH = cg->isPassedBits() & TAVmask;
                TBP_L1_EM20VH = cg->isPassedBits() & TBPmask;
                TAP_L1_EM20VH = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM20VHI")
            {
                L1_EM20VHI = cg->isPassed();
                PS_L1_EM20VHI = cg->getPrescale();
                TAV_L1_EM20VHI = cg->isPassedBits() & TAVmask;
                TBP_L1_EM20VHI = cg->isPassedBits() & TBPmask;
                TAP_L1_EM20VHI = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM22VHI")
            {
                L1_EM22VHI = cg->isPassed();
                PS_L1_EM22VHI = cg->getPrescale();
                TAV_L1_EM22VHI = cg->isPassedBits() & TAVmask;
                TBP_L1_EM22VHI = cg->isPassedBits() & TBPmask;
                TAP_L1_EM22VHI = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM3_EMPTY")
            {
                L1_EM3_EMPTY = cg->isPassed();
                PS_L1_EM3_EMPTY = cg->getPrescale();
                TAV_L1_EM3_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_EM3_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_EM3_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM7_EMPTY")
            {
                L1_EM7_EMPTY = cg->isPassed();
                PS_L1_EM7_EMPTY = cg->getPrescale();
                TAV_L1_EM7_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_EM7_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_EM7_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU4")
            {
                L1_MU4 = cg->isPassed();
                PS_L1_MU4 = cg->getPrescale();
                TAV_L1_MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU6")
            {
                L1_MU6 = cg->isPassed();
                PS_L1_MU6 = cg->getPrescale();
                TAV_L1_MU6 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU6 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU6 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU10")
            {
                L1_MU10 = cg->isPassed();
                PS_L1_MU10 = cg->getPrescale();
                TAV_L1_MU10 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU10 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU10 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU15")
            {
                L1_MU15 = cg->isPassed();
                PS_L1_MU15 = cg->getPrescale();
                TAV_L1_MU15 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU15 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU15 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU20")
            {
                L1_MU20 = cg->isPassed();
                PS_L1_MU20 = cg->getPrescale();
                TAV_L1_MU20 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU20 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU4_EMPTY")
            {
                L1_MU4_EMPTY = cg->isPassed();
                PS_L1_MU4_EMPTY = cg->getPrescale();
                TAV_L1_MU4_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_MU4_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_MU4_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU4_FIRSTEMPTY")
            {
                L1_MU4_FIRSTEMPTY = cg->isPassed();
                PS_L1_MU4_FIRSTEMPTY = cg->getPrescale();
                TAV_L1_MU4_FIRSTEMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_MU4_FIRSTEMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_MU4_FIRSTEMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU11_EMPTY")
            {
                L1_MU11_EMPTY = cg->isPassed();
                PS_L1_MU11_EMPTY = cg->getPrescale();
                TAV_L1_MU11_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_MU11_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_MU11_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU4_UNPAIRED_ISO")
            {
                L1_MU4_UNPAIRED_ISO = cg->isPassed();
                PS_L1_MU4_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_MU4_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_MU4_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_MU4_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2EM3")
            {
                L1_2EM3 = cg->isPassed();
                PS_L1_2EM3 = cg->getPrescale();
                TAV_L1_2EM3 = cg->isPassedBits() & TAVmask;
                TBP_L1_2EM3 = cg->isPassedBits() & TBPmask;
                TAP_L1_2EM3 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2EM7")
            {
                L1_2EM7 = cg->isPassed();
                PS_L1_2EM7 = cg->getPrescale();
                TAV_L1_2EM7 = cg->isPassedBits() & TAVmask;
                TBP_L1_2EM7 = cg->isPassedBits() & TBPmask;
                TAP_L1_2EM7 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2EM10VH")
            {
                L1_2EM10VH = cg->isPassed();
                PS_L1_2EM10VH = cg->getPrescale();
                TAV_L1_2EM10VH = cg->isPassedBits() & TAVmask;
                TBP_L1_2EM10VH = cg->isPassedBits() & TBPmask;
                TAP_L1_2EM10VH = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2EM13VH")
            {
                L1_2EM13VH = cg->isPassed();
                PS_L1_2EM13VH = cg->getPrescale();
                TAV_L1_2EM13VH = cg->isPassedBits() & TAVmask;
                TBP_L1_2EM13VH = cg->isPassedBits() & TBPmask;
                TAP_L1_2EM13VH = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2EM15")
            {
                L1_2EM15 = cg->isPassed();
                PS_L1_2EM15 = cg->getPrescale();
                TAV_L1_2EM15 = cg->isPassedBits() & TAVmask;
                TBP_L1_2EM15 = cg->isPassedBits() & TBPmask;
                TAP_L1_2EM15 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2EM15VH")
            {
                L1_2EM15VH = cg->isPassed();
                PS_L1_2EM15VH = cg->getPrescale();
                TAV_L1_2EM15VH = cg->isPassedBits() & TAVmask;
                TBP_L1_2EM15VH = cg->isPassedBits() & TBPmask;
                TAP_L1_2EM15VH = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM7_2EM3")
            {
                L1_EM7_2EM3 = cg->isPassed();
                PS_L1_EM7_2EM3 = cg->getPrescale();
                TAV_L1_EM7_2EM3 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM7_2EM3 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM7_2EM3 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM12_2EM3")
            {
                L1_EM12_2EM3 = cg->isPassed();
                PS_L1_EM12_2EM3 = cg->getPrescale();
                TAV_L1_EM12_2EM3 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM12_2EM3 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM12_2EM3 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15VH_3EM7")
            {
                L1_EM15VH_3EM7 = cg->isPassed();
                PS_L1_EM15VH_3EM7 = cg->getPrescale();
                TAV_L1_EM15VH_3EM7 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15VH_3EM7 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15VH_3EM7 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2MU4")
            {
                L1_2MU4 = cg->isPassed();
                PS_L1_2MU4 = cg->getPrescale();
                TAV_L1_2MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_2MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_2MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2MU6")
            {
                L1_2MU6 = cg->isPassed();
                PS_L1_2MU6 = cg->getPrescale();
                TAV_L1_2MU6 = cg->isPassedBits() & TAVmask;
                TBP_L1_2MU6 = cg->isPassedBits() & TBPmask;
                TAP_L1_2MU6 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2MU10")
            {
                L1_2MU10 = cg->isPassed();
                PS_L1_2MU10 = cg->getPrescale();
                TAV_L1_2MU10 = cg->isPassedBits() & TAVmask;
                TBP_L1_2MU10 = cg->isPassedBits() & TBPmask;
                TAP_L1_2MU10 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2MU20_OVERLAY")
            {
                L1_2MU20_OVERLAY = cg->isPassed();
                PS_L1_2MU20_OVERLAY = cg->getPrescale();
                TAV_L1_2MU20_OVERLAY = cg->isPassedBits() & TAVmask;
                TBP_L1_2MU20_OVERLAY = cg->isPassedBits() & TBPmask;
                TAP_L1_2MU20_OVERLAY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU10_2MU6")
            {
                L1_MU10_2MU6 = cg->isPassed();
                PS_L1_MU10_2MU6 = cg->getPrescale();
                TAV_L1_MU10_2MU6 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU10_2MU6 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU10_2MU6 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU11_2MU6")
            {
                L1_MU11_2MU6 = cg->isPassed();
                PS_L1_MU11_2MU6 = cg->getPrescale();
                TAV_L1_MU11_2MU6 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU11_2MU6 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU11_2MU6 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3MU4")
            {
                L1_3MU4 = cg->isPassed();
                PS_L1_3MU4 = cg->getPrescale();
                TAV_L1_3MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_3MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_3MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU6_2MU4")
            {
                L1_MU6_2MU4 = cg->isPassed();
                PS_L1_MU6_2MU4 = cg->getPrescale();
                TAV_L1_MU6_2MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU6_2MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU6_2MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3MU6")
            {
                L1_3MU6 = cg->isPassed();
                PS_L1_3MU6 = cg->getPrescale();
                TAV_L1_3MU6 = cg->isPassedBits() & TAVmask;
                TBP_L1_3MU6 = cg->isPassedBits() & TBPmask;
                TAP_L1_3MU6 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_4J15.0ETA25")
            {
                L1_4J15_0ETA25 = cg->isPassed();
                PS_L1_4J15_0ETA25 = cg->getPrescale();
                TAV_L1_4J15_0ETA25 = cg->isPassedBits() & TAVmask;
                TBP_L1_4J15_0ETA25 = cg->isPassedBits() & TBPmask;
                TAP_L1_4J15_0ETA25 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15I_MU4")
            {
                L1_EM15I_MU4 = cg->isPassed();
                PS_L1_EM15I_MU4 = cg->getPrescale();
                TAV_L1_EM15I_MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15I_MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15I_MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2EM8VH_MU10")
            {
                L1_2EM8VH_MU10 = cg->isPassed();
                PS_L1_2EM8VH_MU10 = cg->getPrescale();
                TAV_L1_2EM8VH_MU10 = cg->isPassedBits() & TAVmask;
                TBP_L1_2EM8VH_MU10 = cg->isPassedBits() & TBPmask;
                TAP_L1_2EM8VH_MU10 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15VH_MU10")
            {
                L1_EM15VH_MU10 = cg->isPassed();
                PS_L1_EM15VH_MU10 = cg->getPrescale();
                TAV_L1_EM15VH_MU10 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15VH_MU10 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15VH_MU10 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU12")
            {
                L1_TAU12 = cg->isPassed();
                PS_L1_TAU12 = cg->getPrescale();
                TAV_L1_TAU12 = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU12 = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU12IL")
            {
                L1_TAU12IL = cg->isPassed();
                PS_L1_TAU12IL = cg->getPrescale();
                TAV_L1_TAU12IL = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU12IL = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU12IL = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU12IM")
            {
                L1_TAU12IM = cg->isPassed();
                PS_L1_TAU12IM = cg->getPrescale();
                TAV_L1_TAU12IM = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU12IM = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU12IM = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU12IT")
            {
                L1_TAU12IT = cg->isPassed();
                PS_L1_TAU12IT = cg->getPrescale();
                TAV_L1_TAU12IT = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU12IT = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU12IT = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU20")
            {
                L1_TAU20 = cg->isPassed();
                PS_L1_TAU20 = cg->getPrescale();
                TAV_L1_TAU20 = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU20 = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU20IL")
            {
                L1_TAU20IL = cg->isPassed();
                PS_L1_TAU20IL = cg->getPrescale();
                TAV_L1_TAU20IL = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU20IL = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU20IL = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU20IM")
            {
                L1_TAU20IM = cg->isPassed();
                PS_L1_TAU20IM = cg->getPrescale();
                TAV_L1_TAU20IM = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU20IM = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU20IM = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU20IT")
            {
                L1_TAU20IT = cg->isPassed();
                PS_L1_TAU20IT = cg->getPrescale();
                TAV_L1_TAU20IT = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU20IT = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU20IT = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU30")
            {
                L1_TAU30 = cg->isPassed();
                PS_L1_TAU30 = cg->getPrescale();
                TAV_L1_TAU30 = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU30 = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU30 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU40")
            {
                L1_TAU40 = cg->isPassed();
                PS_L1_TAU40 = cg->getPrescale();
                TAV_L1_TAU40 = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU40 = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU40 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU60")
            {
                L1_TAU60 = cg->isPassed();
                PS_L1_TAU60 = cg->getPrescale();
                TAV_L1_TAU60 = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU60 = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU60 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU8")
            {
                L1_TAU8 = cg->isPassed();
                PS_L1_TAU8 = cg->getPrescale();
                TAV_L1_TAU8 = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU8 = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU8 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU8_EMPTY")
            {
                L1_TAU8_EMPTY = cg->isPassed();
                PS_L1_TAU8_EMPTY = cg->getPrescale();
                TAV_L1_TAU8_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU8_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU8_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU20IM_2TAU12IM")
            {
                L1_TAU20IM_2TAU12IM = cg->isPassed();
                PS_L1_TAU20IM_2TAU12IM = cg->getPrescale();
                TAV_L1_TAU20IM_2TAU12IM = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU20IM_2TAU12IM = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU20IM_2TAU12IM = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU20_2TAU12")
            {
                L1_TAU20_2TAU12 = cg->isPassed();
                PS_L1_TAU20_2TAU12 = cg->getPrescale();
                TAV_L1_TAU20_2TAU12 = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU20_2TAU12 = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU20_2TAU12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15HI_2TAU12IM")
            {
                L1_EM15HI_2TAU12IM = cg->isPassed();
                PS_L1_EM15HI_2TAU12IM = cg->getPrescale();
                TAV_L1_EM15HI_2TAU12IM = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15HI_2TAU12IM = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15HI_2TAU12IM = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15HI_2TAU12IM_J25_3J12")
            {
                L1_EM15HI_2TAU12IM_J25_3J12 = cg->isPassed();
                PS_L1_EM15HI_2TAU12IM_J25_3J12 = cg->getPrescale();
                TAV_L1_EM15HI_2TAU12IM_J25_3J12 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15HI_2TAU12IM_J25_3J12 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15HI_2TAU12IM_J25_3J12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15HI_TAU40_2TAU15")
            {
                L1_EM15HI_TAU40_2TAU15 = cg->isPassed();
                PS_L1_EM15HI_TAU40_2TAU15 = cg->getPrescale();
                TAV_L1_EM15HI_TAU40_2TAU15 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15HI_TAU40_2TAU15 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15HI_TAU40_2TAU15 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU10_TAU12IM")
            {
                L1_MU10_TAU12IM = cg->isPassed();
                PS_L1_MU10_TAU12IM = cg->getPrescale();
                TAV_L1_MU10_TAU12IM = cg->isPassedBits() & TAVmask;
                TBP_L1_MU10_TAU12IM = cg->isPassedBits() & TBPmask;
                TAP_L1_MU10_TAU12IM = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU10_TAU12IM_J25_2J12")
            {
                L1_MU10_TAU12IM_J25_2J12 = cg->isPassed();
                PS_L1_MU10_TAU12IM_J25_2J12 = cg->getPrescale();
                TAV_L1_MU10_TAU12IM_J25_2J12 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU10_TAU12IM_J25_2J12 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU10_TAU12IM_J25_2J12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM7_MU10")
            {
                L1_EM7_MU10 = cg->isPassed();
                PS_L1_EM7_MU10 = cg->getPrescale();
                TAV_L1_EM7_MU10 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM7_MU10 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM7_MU10 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU6_EMPTY")
            {
                L1_MU6_EMPTY = cg->isPassed();
                PS_L1_MU6_EMPTY = cg->getPrescale();
                TAV_L1_MU6_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_MU6_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_MU6_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU10_TAU20IM")
            {
                L1_MU10_TAU20IM = cg->isPassed();
                PS_L1_MU10_TAU20IM = cg->getPrescale();
                TAV_L1_MU10_TAU20IM = cg->isPassedBits() & TAVmask;
                TBP_L1_MU10_TAU20IM = cg->isPassedBits() & TBPmask;
                TAP_L1_MU10_TAU20IM = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE10")
            {
                L1_XE10 = cg->isPassed();
                PS_L1_XE10 = cg->getPrescale();
                TAV_L1_XE10 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE10 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE10 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU20IL_2TAU12IL_J25_2J20_3J12")
            {
                L1_TAU20IL_2TAU12IL_J25_2J20_3J12 = cg->isPassed();
                PS_L1_TAU20IL_2TAU12IL_J25_2J20_3J12 = cg->getPrescale();
                TAV_L1_TAU20IL_2TAU12IL_J25_2J20_3J12 = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU20IL_2TAU12IL_J25_2J20_3J12 = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU20IL_2TAU12IL_J25_2J20_3J12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU20IM_2TAU12IM_J25_2J20_3J12")
            {
                L1_TAU20IM_2TAU12IM_J25_2J20_3J12 = cg->isPassed();
                PS_L1_TAU20IM_2TAU12IM_J25_2J20_3J12 = cg->getPrescale();
                TAV_L1_TAU20IM_2TAU12IM_J25_2J20_3J12 = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU20IM_2TAU12IM_J25_2J20_3J12 = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU20IM_2TAU12IM_J25_2J20_3J12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J25_2J20_3J12_BOX-TAU20ITAU12I")
            {
                L1_J25_2J20_3J12_BOX_TAU20ITAU12I = cg->isPassed();
                PS_L1_J25_2J20_3J12_BOX_TAU20ITAU12I = cg->getPrescale();
                TAV_L1_J25_2J20_3J12_BOX_TAU20ITAU12I = cg->isPassedBits() & TAVmask;
                TBP_L1_J25_2J20_3J12_BOX_TAU20ITAU12I = cg->isPassedBits() & TBPmask;
                TAP_L1_J25_2J20_3J12_BOX_TAU20ITAU12I = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_DR-MU10TAU12I_TAU12I-J25")
            {
                L1_DR_MU10TAU12I_TAU12I_J25 = cg->isPassed();
                PS_L1_DR_MU10TAU12I_TAU12I_J25 = cg->getPrescale();
                TAV_L1_DR_MU10TAU12I_TAU12I_J25 = cg->isPassedBits() & TAVmask;
                TBP_L1_DR_MU10TAU12I_TAU12I_J25 = cg->isPassedBits() & TBPmask;
                TAP_L1_DR_MU10TAU12I_TAU12I_J25 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU10_TAU12I-J25")
            {
                L1_MU10_TAU12I_J25 = cg->isPassed();
                PS_L1_MU10_TAU12I_J25 = cg->getPrescale();
                TAV_L1_MU10_TAU12I_J25 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU10_TAU12I_J25 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU10_TAU12I_J25 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU20IM_2J20_XE45")
            {
                L1_TAU20IM_2J20_XE45 = cg->isPassed();
                PS_L1_TAU20IM_2J20_XE45 = cg->getPrescale();
                TAV_L1_TAU20IM_2J20_XE45 = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU20IM_2J20_XE45 = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU20IM_2J20_XE45 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J15.31ETA49_UNPAIRED_ISO")
            {
                L1_J15_31ETA49_UNPAIRED_ISO = cg->isPassed();
                PS_L1_J15_31ETA49_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_J15_31ETA49_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_J15_31ETA49_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_J15_31ETA49_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE20")
            {
                L1_XE20 = cg->isPassed();
                PS_L1_XE20 = cg->getPrescale();
                TAV_L1_XE20 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE20 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE45_TAU20-J20")
            {
                L1_XE45_TAU20_J20 = cg->isPassed();
                PS_L1_XE45_TAU20_J20 = cg->getPrescale();
                TAV_L1_XE45_TAU20_J20 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE45_TAU20_J20 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE45_TAU20_J20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15HI_2TAU12IM_XE35")
            {
                L1_EM15HI_2TAU12IM_XE35 = cg->isPassed();
                PS_L1_EM15HI_2TAU12IM_XE35 = cg->getPrescale();
                TAV_L1_EM15HI_2TAU12IM_XE35 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15HI_2TAU12IM_XE35 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15HI_2TAU12IM_XE35 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE35_EM15-TAU12I")
            {
                L1_XE35_EM15_TAU12I = cg->isPassed();
                PS_L1_XE35_EM15_TAU12I = cg->getPrescale();
                TAV_L1_XE35_EM15_TAU12I = cg->isPassedBits() & TAVmask;
                TBP_L1_XE35_EM15_TAU12I = cg->isPassedBits() & TBPmask;
                TAP_L1_XE35_EM15_TAU12I = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE40_EM15-TAU12I")
            {
                L1_XE40_EM15_TAU12I = cg->isPassed();
                PS_L1_XE40_EM15_TAU12I = cg->getPrescale();
                TAV_L1_XE40_EM15_TAU12I = cg->isPassedBits() & TAVmask;
                TBP_L1_XE40_EM15_TAU12I = cg->isPassedBits() & TBPmask;
                TAP_L1_XE40_EM15_TAU12I = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU10_TAU12IM_XE35")
            {
                L1_MU10_TAU12IM_XE35 = cg->isPassed();
                PS_L1_MU10_TAU12IM_XE35 = cg->getPrescale();
                TAV_L1_MU10_TAU12IM_XE35 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU10_TAU12IM_XE35 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU10_TAU12IM_XE35 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE25")
            {
                L1_XE25 = cg->isPassed();
                PS_L1_XE25 = cg->getPrescale();
                TAV_L1_XE25 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE25 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE25 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU20IM_2TAU12IM_XE35")
            {
                L1_TAU20IM_2TAU12IM_XE35 = cg->isPassed();
                PS_L1_TAU20IM_2TAU12IM_XE35 = cg->getPrescale();
                TAV_L1_TAU20IM_2TAU12IM_XE35 = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU20IM_2TAU12IM_XE35 = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU20IM_2TAU12IM_XE35 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU20_2TAU12_XE35")
            {
                L1_TAU20_2TAU12_XE35 = cg->isPassed();
                PS_L1_TAU20_2TAU12_XE35 = cg->getPrescale();
                TAV_L1_TAU20_2TAU12_XE35 = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU20_2TAU12_XE35 = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU20_2TAU12_XE35 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE30")
            {
                L1_XE30 = cg->isPassed();
                PS_L1_XE30 = cg->getPrescale();
                TAV_L1_XE30 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE30 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE30 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15VH_JJ15.23ETA49")
            {
                L1_EM15VH_JJ15_23ETA49 = cg->isPassed();
                PS_L1_EM15VH_JJ15_23ETA49 = cg->getPrescale();
                TAV_L1_EM15VH_JJ15_23ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15VH_JJ15_23ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15VH_JJ15_23ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU4_J12")
            {
                L1_MU4_J12 = cg->isPassed();
                PS_L1_MU4_J12 = cg->getPrescale();
                TAV_L1_MU4_J12 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU4_J12 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU4_J12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU6_J20")
            {
                L1_MU6_J20 = cg->isPassed();
                PS_L1_MU6_J20 = cg->getPrescale();
                TAV_L1_MU6_J20 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU6_J20 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU6_J20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU6_J40")
            {
                L1_MU6_J40 = cg->isPassed();
                PS_L1_MU6_J40 = cg->getPrescale();
                TAV_L1_MU6_J40 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU6_J40 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU6_J40 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU6_J75")
            {
                L1_MU6_J75 = cg->isPassed();
                PS_L1_MU6_J75 = cg->getPrescale();
                TAV_L1_MU6_J75 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU6_J75 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU6_J75 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J12")
            {
                L1_J12 = cg->isPassed();
                PS_L1_J12 = cg->getPrescale();
                TAV_L1_J12 = cg->isPassedBits() & TAVmask;
                TBP_L1_J12 = cg->isPassedBits() & TBPmask;
                TAP_L1_J12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J15")
            {
                L1_J15 = cg->isPassed();
                PS_L1_J15 = cg->getPrescale();
                TAV_L1_J15 = cg->isPassedBits() & TAVmask;
                TBP_L1_J15 = cg->isPassedBits() & TBPmask;
                TAP_L1_J15 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J20")
            {
                L1_J20 = cg->isPassed();
                PS_L1_J20 = cg->getPrescale();
                TAV_L1_J20 = cg->isPassedBits() & TAVmask;
                TBP_L1_J20 = cg->isPassedBits() & TBPmask;
                TAP_L1_J20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J25")
            {
                L1_J25 = cg->isPassed();
                PS_L1_J25 = cg->getPrescale();
                TAV_L1_J25 = cg->isPassedBits() & TAVmask;
                TBP_L1_J25 = cg->isPassedBits() & TBPmask;
                TAP_L1_J25 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J30")
            {
                L1_J30 = cg->isPassed();
                PS_L1_J30 = cg->getPrescale();
                TAV_L1_J30 = cg->isPassedBits() & TAVmask;
                TBP_L1_J30 = cg->isPassedBits() & TBPmask;
                TAP_L1_J30 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J40")
            {
                L1_J40 = cg->isPassed();
                PS_L1_J40 = cg->getPrescale();
                TAV_L1_J40 = cg->isPassedBits() & TAVmask;
                TBP_L1_J40 = cg->isPassedBits() & TBPmask;
                TAP_L1_J40 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J50")
            {
                L1_J50 = cg->isPassed();
                PS_L1_J50 = cg->getPrescale();
                TAV_L1_J50 = cg->isPassedBits() & TAVmask;
                TBP_L1_J50 = cg->isPassedBits() & TBPmask;
                TAP_L1_J50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J75")
            {
                L1_J75 = cg->isPassed();
                PS_L1_J75 = cg->getPrescale();
                TAV_L1_J75 = cg->isPassedBits() & TAVmask;
                TBP_L1_J75 = cg->isPassedBits() & TBPmask;
                TAP_L1_J75 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J85")
            {
                L1_J85 = cg->isPassed();
                PS_L1_J85 = cg->getPrescale();
                TAV_L1_J85 = cg->isPassedBits() & TAVmask;
                TBP_L1_J85 = cg->isPassedBits() & TBPmask;
                TAP_L1_J85 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J100")
            {
                L1_J100 = cg->isPassed();
                PS_L1_J100 = cg->getPrescale();
                TAV_L1_J100 = cg->isPassedBits() & TAVmask;
                TBP_L1_J100 = cg->isPassedBits() & TBPmask;
                TAP_L1_J100 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J120")
            {
                L1_J120 = cg->isPassed();
                PS_L1_J120 = cg->getPrescale();
                TAV_L1_J120 = cg->isPassedBits() & TAVmask;
                TBP_L1_J120 = cg->isPassedBits() & TBPmask;
                TAP_L1_J120 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J400")
            {
                L1_J400 = cg->isPassed();
                PS_L1_J400 = cg->getPrescale();
                TAV_L1_J400 = cg->isPassedBits() & TAVmask;
                TBP_L1_J400 = cg->isPassedBits() & TBPmask;
                TAP_L1_J400 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J20.31ETA49")
            {
                L1_J20_31ETA49 = cg->isPassed();
                PS_L1_J20_31ETA49 = cg->getPrescale();
                TAV_L1_J20_31ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_J20_31ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_J20_31ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J30.31ETA49")
            {
                L1_J30_31ETA49 = cg->isPassed();
                PS_L1_J30_31ETA49 = cg->getPrescale();
                TAV_L1_J30_31ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_J30_31ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_J30_31ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J50.31ETA49")
            {
                L1_J50_31ETA49 = cg->isPassed();
                PS_L1_J50_31ETA49 = cg->getPrescale();
                TAV_L1_J50_31ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_J50_31ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_J50_31ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J75.31ETA49")
            {
                L1_J75_31ETA49 = cg->isPassed();
                PS_L1_J75_31ETA49 = cg->getPrescale();
                TAV_L1_J75_31ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_J75_31ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_J75_31ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J100.31ETA49")
            {
                L1_J100_31ETA49 = cg->isPassed();
                PS_L1_J100_31ETA49 = cg->getPrescale();
                TAV_L1_J100_31ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_J100_31ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_J100_31ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE65")
            {
                L1_XE65 = cg->isPassed();
                PS_L1_XE65 = cg->getPrescale();
                TAV_L1_XE65 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE65 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE65 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J15.31ETA49")
            {
                L1_J15_31ETA49 = cg->isPassed();
                PS_L1_J15_31ETA49 = cg->getPrescale();
                TAV_L1_J15_31ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_J15_31ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_J15_31ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J20.28ETA31")
            {
                L1_J20_28ETA31 = cg->isPassed();
                PS_L1_J20_28ETA31 = cg->getPrescale();
                TAV_L1_J20_28ETA31 = cg->isPassedBits() & TAVmask;
                TBP_L1_J20_28ETA31 = cg->isPassedBits() & TBPmask;
                TAP_L1_J20_28ETA31 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J12_EMPTY")
            {
                L1_J12_EMPTY = cg->isPassed();
                PS_L1_J12_EMPTY = cg->getPrescale();
                TAV_L1_J12_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_J12_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_J12_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J12_FIRSTEMPTY")
            {
                L1_J12_FIRSTEMPTY = cg->isPassed();
                PS_L1_J12_FIRSTEMPTY = cg->getPrescale();
                TAV_L1_J12_FIRSTEMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_J12_FIRSTEMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_J12_FIRSTEMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J12_UNPAIRED_ISO")
            {
                L1_J12_UNPAIRED_ISO = cg->isPassed();
                PS_L1_J12_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_J12_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_J12_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_J12_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J12_UNPAIRED_NONISO")
            {
                L1_J12_UNPAIRED_NONISO = cg->isPassed();
                PS_L1_J12_UNPAIRED_NONISO = cg->getPrescale();
                TAV_L1_J12_UNPAIRED_NONISO = cg->isPassedBits() & TAVmask;
                TBP_L1_J12_UNPAIRED_NONISO = cg->isPassedBits() & TBPmask;
                TAP_L1_J12_UNPAIRED_NONISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J12_ABORTGAPNOTCALIB")
            {
                L1_J12_ABORTGAPNOTCALIB = cg->isPassed();
                PS_L1_J12_ABORTGAPNOTCALIB = cg->getPrescale();
                TAV_L1_J12_ABORTGAPNOTCALIB = cg->isPassedBits() & TAVmask;
                TBP_L1_J12_ABORTGAPNOTCALIB = cg->isPassedBits() & TBPmask;
                TAP_L1_J12_ABORTGAPNOTCALIB = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J30_EMPTY")
            {
                L1_J30_EMPTY = cg->isPassed();
                PS_L1_J30_EMPTY = cg->getPrescale();
                TAV_L1_J30_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_J30_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_J30_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J30_FIRSTEMPTY")
            {
                L1_J30_FIRSTEMPTY = cg->isPassed();
                PS_L1_J30_FIRSTEMPTY = cg->getPrescale();
                TAV_L1_J30_FIRSTEMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_J30_FIRSTEMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_J30_FIRSTEMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J30.31ETA49_EMPTY")
            {
                L1_J30_31ETA49_EMPTY = cg->isPassed();
                PS_L1_J30_31ETA49_EMPTY = cg->getPrescale();
                TAV_L1_J30_31ETA49_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_J30_31ETA49_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_J30_31ETA49_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J30.31ETA49_UNPAIRED_ISO")
            {
                L1_J30_31ETA49_UNPAIRED_ISO = cg->isPassed();
                PS_L1_J30_31ETA49_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_J30_31ETA49_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_J30_31ETA49_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_J30_31ETA49_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J30.31ETA49_UNPAIRED_NONISO")
            {
                L1_J30_31ETA49_UNPAIRED_NONISO = cg->isPassed();
                PS_L1_J30_31ETA49_UNPAIRED_NONISO = cg->getPrescale();
                TAV_L1_J30_31ETA49_UNPAIRED_NONISO = cg->isPassedBits() & TAVmask;
                TBP_L1_J30_31ETA49_UNPAIRED_NONISO = cg->isPassedBits() & TBPmask;
                TAP_L1_J30_31ETA49_UNPAIRED_NONISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J50_UNPAIRED_ISO")
            {
                L1_J50_UNPAIRED_ISO = cg->isPassed();
                PS_L1_J50_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_J50_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_J50_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_J50_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J50_UNPAIRED_NONISO")
            {
                L1_J50_UNPAIRED_NONISO = cg->isPassed();
                PS_L1_J50_UNPAIRED_NONISO = cg->getPrescale();
                TAV_L1_J50_UNPAIRED_NONISO = cg->isPassedBits() & TAVmask;
                TBP_L1_J50_UNPAIRED_NONISO = cg->isPassedBits() & TBPmask;
                TAP_L1_J50_UNPAIRED_NONISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J50_ABORTGAPNOTCALIB")
            {
                L1_J50_ABORTGAPNOTCALIB = cg->isPassed();
                PS_L1_J50_ABORTGAPNOTCALIB = cg->getPrescale();
                TAV_L1_J50_ABORTGAPNOTCALIB = cg->isPassedBits() & TAVmask;
                TBP_L1_J50_ABORTGAPNOTCALIB = cg->isPassedBits() & TBPmask;
                TAP_L1_J50_ABORTGAPNOTCALIB = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_NSC")
            {
                L1_AFP_NSC = cg->isPassed();
                PS_L1_AFP_NSC = cg->getPrescale();
                TAV_L1_AFP_NSC = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_NSC = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_NSC = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J20_J20.31ETA49")
            {
                L1_J20_J20_31ETA49 = cg->isPassed();
                PS_L1_J20_J20_31ETA49 = cg->getPrescale();
                TAV_L1_J20_J20_31ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_J20_J20_31ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_J20_J20_31ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3J15")
            {
                L1_3J15 = cg->isPassed();
                PS_L1_3J15 = cg->getPrescale();
                TAV_L1_3J15 = cg->isPassedBits() & TAVmask;
                TBP_L1_3J15 = cg->isPassedBits() & TBPmask;
                TAP_L1_3J15 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3J20")
            {
                L1_3J20 = cg->isPassed();
                PS_L1_3J20 = cg->getPrescale();
                TAV_L1_3J20 = cg->isPassedBits() & TAVmask;
                TBP_L1_3J20 = cg->isPassedBits() & TBPmask;
                TAP_L1_3J20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3J40")
            {
                L1_3J40 = cg->isPassed();
                PS_L1_3J40 = cg->getPrescale();
                TAV_L1_3J40 = cg->isPassedBits() & TAVmask;
                TBP_L1_3J40 = cg->isPassedBits() & TBPmask;
                TAP_L1_3J40 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3J15.0ETA25")
            {
                L1_3J15_0ETA25 = cg->isPassed();
                PS_L1_3J15_0ETA25 = cg->getPrescale();
                TAV_L1_3J15_0ETA25 = cg->isPassedBits() & TAVmask;
                TBP_L1_3J15_0ETA25 = cg->isPassedBits() & TBPmask;
                TAP_L1_3J15_0ETA25 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3J50")
            {
                L1_3J50 = cg->isPassed();
                PS_L1_3J50 = cg->getPrescale();
                TAV_L1_3J50 = cg->isPassedBits() & TAVmask;
                TBP_L1_3J50 = cg->isPassedBits() & TBPmask;
                TAP_L1_3J50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_4J15")
            {
                L1_4J15 = cg->isPassed();
                PS_L1_4J15 = cg->getPrescale();
                TAV_L1_4J15 = cg->isPassedBits() & TAVmask;
                TBP_L1_4J15 = cg->isPassedBits() & TBPmask;
                TAP_L1_4J15 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_4J20")
            {
                L1_4J20 = cg->isPassed();
                PS_L1_4J20 = cg->getPrescale();
                TAV_L1_4J20 = cg->isPassedBits() & TAVmask;
                TBP_L1_4J20 = cg->isPassedBits() & TBPmask;
                TAP_L1_4J20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J75_XE50")
            {
                L1_J75_XE50 = cg->isPassed();
                PS_L1_J75_XE50 = cg->getPrescale();
                TAV_L1_J75_XE50 = cg->isPassedBits() & TAVmask;
                TBP_L1_J75_XE50 = cg->isPassedBits() & TBPmask;
                TAP_L1_J75_XE50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE75")
            {
                L1_XE75 = cg->isPassed();
                PS_L1_XE75 = cg->getPrescale();
                TAV_L1_XE75 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE75 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE75 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_6J15")
            {
                L1_6J15 = cg->isPassed();
                PS_L1_6J15 = cg->getPrescale();
                TAV_L1_6J15 = cg->isPassedBits() & TAVmask;
                TBP_L1_6J15 = cg->isPassedBits() & TBPmask;
                TAP_L1_6J15 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J75_3J20")
            {
                L1_J75_3J20 = cg->isPassed();
                PS_L1_J75_3J20 = cg->getPrescale();
                TAV_L1_J75_3J20 = cg->isPassedBits() & TAVmask;
                TBP_L1_J75_3J20 = cg->isPassedBits() & TBPmask;
                TAP_L1_J75_3J20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J30.0ETA49_2J20.0ETA49")
            {
                L1_J30_0ETA49_2J20_0ETA49 = cg->isPassed();
                PS_L1_J30_0ETA49_2J20_0ETA49 = cg->getPrescale();
                TAV_L1_J30_0ETA49_2J20_0ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_J30_0ETA49_2J20_0ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_J30_0ETA49_2J20_0ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE10")
            {
                L1_TE10 = cg->isPassed();
                PS_L1_TE10 = cg->getPrescale();
                TAV_L1_TE10 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE10 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE10 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_FSC")
            {
                L1_AFP_FSC = cg->isPassed();
                PS_L1_AFP_FSC = cg->getPrescale();
                TAV_L1_AFP_FSC = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_FSC = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_FSC = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_5J15.0ETA25")
            {
                L1_5J15_0ETA25 = cg->isPassed();
                PS_L1_5J15_0ETA25 = cg->getPrescale();
                TAV_L1_5J15_0ETA25 = cg->isPassedBits() & TAVmask;
                TBP_L1_5J15_0ETA25 = cg->isPassedBits() & TBPmask;
                TAP_L1_5J15_0ETA25 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2J15_XE55")
            {
                L1_2J15_XE55 = cg->isPassed();
                PS_L1_2J15_XE55 = cg->getPrescale();
                TAV_L1_2J15_XE55 = cg->isPassedBits() & TAVmask;
                TBP_L1_2J15_XE55 = cg->isPassedBits() & TBPmask;
                TAP_L1_2J15_XE55 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J40_XE50")
            {
                L1_J40_XE50 = cg->isPassed();
                PS_L1_J40_XE50 = cg->getPrescale();
                TAV_L1_J40_XE50 = cg->isPassedBits() & TAVmask;
                TBP_L1_J40_XE50 = cg->isPassedBits() & TBPmask;
                TAP_L1_J40_XE50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J75_XE40")
            {
                L1_J75_XE40 = cg->isPassed();
                PS_L1_J75_XE40 = cg->getPrescale();
                TAV_L1_J75_XE40 = cg->isPassedBits() & TAVmask;
                TBP_L1_J75_XE40 = cg->isPassedBits() & TBPmask;
                TAP_L1_J75_XE40 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE35")
            {
                L1_XE35 = cg->isPassed();
                PS_L1_XE35 = cg->getPrescale();
                TAV_L1_XE35 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE35 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE35 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE40")
            {
                L1_XE40 = cg->isPassed();
                PS_L1_XE40 = cg->getPrescale();
                TAV_L1_XE40 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE40 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE40 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE45")
            {
                L1_XE45 = cg->isPassed();
                PS_L1_XE45 = cg->getPrescale();
                TAV_L1_XE45 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE45 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE45 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE50")
            {
                L1_XE50 = cg->isPassed();
                PS_L1_XE50 = cg->getPrescale();
                TAV_L1_XE50 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE50 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE55")
            {
                L1_XE55 = cg->isPassed();
                PS_L1_XE55 = cg->getPrescale();
                TAV_L1_XE55 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE55 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE55 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE60")
            {
                L1_XE60 = cg->isPassed();
                PS_L1_XE60 = cg->getPrescale();
                TAV_L1_XE60 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE60 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE60 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE70")
            {
                L1_XE70 = cg->isPassed();
                PS_L1_XE70 = cg->getPrescale();
                TAV_L1_XE70 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE70 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE70 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE80")
            {
                L1_XE80 = cg->isPassed();
                PS_L1_XE80 = cg->getPrescale();
                TAV_L1_XE80 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE80 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE80 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XS20")
            {
                L1_XS20 = cg->isPassed();
                PS_L1_XS20 = cg->getPrescale();
                TAV_L1_XS20 = cg->isPassedBits() & TAVmask;
                TBP_L1_XS20 = cg->isPassedBits() & TBPmask;
                TAP_L1_XS20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XS30")
            {
                L1_XS30 = cg->isPassed();
                PS_L1_XS30 = cg->getPrescale();
                TAV_L1_XS30 = cg->isPassedBits() & TAVmask;
                TBP_L1_XS30 = cg->isPassedBits() & TBPmask;
                TAP_L1_XS30 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM12_XS20")
            {
                L1_EM12_XS20 = cg->isPassed();
                PS_L1_EM12_XS20 = cg->getPrescale();
                TAV_L1_EM12_XS20 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM12_XS20 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM12_XS20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15_XS30")
            {
                L1_EM15_XS30 = cg->isPassed();
                PS_L1_EM15_XS30 = cg->getPrescale();
                TAV_L1_EM15_XS30 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15_XS30 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15_XS30 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE150")
            {
                L1_XE150 = cg->isPassed();
                PS_L1_XE150 = cg->getPrescale();
                TAV_L1_XE150 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE150 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE150 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE30")
            {
                L1_TE30 = cg->isPassed();
                PS_L1_TE30 = cg->getPrescale();
                TAV_L1_TE30 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE30 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE30 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE40")
            {
                L1_TE40 = cg->isPassed();
                PS_L1_TE40 = cg->getPrescale();
                TAV_L1_TE40 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE40 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE40 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE70")
            {
                L1_TE70 = cg->isPassed();
                PS_L1_TE70 = cg->getPrescale();
                TAV_L1_TE70 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE70 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE70 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE30.0ETA24")
            {
                L1_TE30_0ETA24 = cg->isPassed();
                PS_L1_TE30_0ETA24 = cg->getPrescale();
                TAV_L1_TE30_0ETA24 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE30_0ETA24 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE30_0ETA24 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_Wide_UNPAIRED_NONISO")
            {
                L1_BCM_Wide_UNPAIRED_NONISO = cg->isPassed();
                PS_L1_BCM_Wide_UNPAIRED_NONISO = cg->getPrescale();
                TAV_L1_BCM_Wide_UNPAIRED_NONISO = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_Wide_UNPAIRED_NONISO = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_Wide_UNPAIRED_NONISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_AC_CA_UNPAIRED_ISO")
            {
                L1_BCM_AC_CA_UNPAIRED_ISO = cg->isPassed();
                PS_L1_BCM_AC_CA_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_BCM_AC_CA_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_AC_CA_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_AC_CA_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_AC_UNPAIRED_ISO")
            {
                L1_BCM_AC_UNPAIRED_ISO = cg->isPassed();
                PS_L1_BCM_AC_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_BCM_AC_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_AC_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_AC_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_1_EMPTY")
            {
                L1_MBTS_1_EMPTY = cg->isPassed();
                PS_L1_MBTS_1_EMPTY = cg->getPrescale();
                TAV_L1_MBTS_1_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_1_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_1_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_1_UNPAIRED_ISO")
            {
                L1_MBTS_1_UNPAIRED_ISO = cg->isPassed();
                PS_L1_MBTS_1_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_MBTS_1_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_1_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_1_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_2_EMPTY")
            {
                L1_MBTS_2_EMPTY = cg->isPassed();
                PS_L1_MBTS_2_EMPTY = cg->getPrescale();
                TAV_L1_MBTS_2_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_2_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_2_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_2_UNPAIRED_ISO")
            {
                L1_MBTS_2_UNPAIRED_ISO = cg->isPassed();
                PS_L1_MBTS_2_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_MBTS_2_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_2_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_2_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_1_1_EMPTY")
            {
                L1_MBTS_1_1_EMPTY = cg->isPassed();
                PS_L1_MBTS_1_1_EMPTY = cg->getPrescale();
                TAV_L1_MBTS_1_1_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_1_1_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_1_1_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_1_1_UNPAIRED_ISO")
            {
                L1_MBTS_1_1_UNPAIRED_ISO = cg->isPassed();
                PS_L1_MBTS_1_1_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_MBTS_1_1_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_1_1_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_1_1_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_C_ALFA_A")
            {
                L1_AFP_C_ALFA_A = cg->isPassed();
                PS_L1_AFP_C_ALFA_A = cg->getPrescale();
                TAV_L1_AFP_C_ALFA_A = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_C_ALFA_A = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_C_ALFA_A = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_C_ANY")
            {
                L1_AFP_C_ANY = cg->isPassed();
                PS_L1_AFP_C_ANY = cg->getPrescale();
                TAV_L1_AFP_C_ANY = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_C_ANY = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_C_ANY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM13VH_3J20")
            {
                L1_EM13VH_3J20 = cg->isPassed();
                PS_L1_EM13VH_3J20 = cg->getPrescale();
                TAV_L1_EM13VH_3J20 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM13VH_3J20 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM13VH_3J20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU10_3J20")
            {
                L1_MU10_3J20 = cg->isPassed();
                PS_L1_MU10_3J20 = cg->getPrescale();
                TAV_L1_MU10_3J20 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU10_3J20 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU10_3J20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_C_ANY_MBTS_A")
            {
                L1_AFP_C_ANY_MBTS_A = cg->isPassed();
                PS_L1_AFP_C_ANY_MBTS_A = cg->getPrescale();
                TAV_L1_AFP_C_ANY_MBTS_A = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_C_ANY_MBTS_A = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_C_ANY_MBTS_A = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2J50_XE40")
            {
                L1_2J50_XE40 = cg->isPassed();
                PS_L1_2J50_XE40 = cg->getPrescale();
                TAV_L1_2J50_XE40 = cg->isPassedBits() & TAVmask;
                TBP_L1_2J50_XE40 = cg->isPassedBits() & TBPmask;
                TAP_L1_2J50_XE40 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J40_XE60")
            {
                L1_J40_XE60 = cg->isPassed();
                PS_L1_J40_XE60 = cg->getPrescale();
                TAV_L1_J40_XE60 = cg->isPassedBits() & TAVmask;
                TBP_L1_J40_XE60 = cg->isPassedBits() & TBPmask;
                TAP_L1_J40_XE60 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J40.0ETA25_XE50")
            {
                L1_J40_0ETA25_XE50 = cg->isPassed();
                PS_L1_J40_0ETA25_XE50 = cg->getPrescale();
                TAV_L1_J40_0ETA25_XE50 = cg->isPassedBits() & TAVmask;
                TBP_L1_J40_0ETA25_XE50 = cg->isPassedBits() & TBPmask;
                TAP_L1_J40_0ETA25_XE50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BPH-2M8-2MU4")
            {
                L1_BPH_2M8_2MU4 = cg->isPassed();
                PS_L1_BPH_2M8_2MU4 = cg->getPrescale();
                TAV_L1_BPH_2M8_2MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_BPH_2M8_2MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_BPH_2M8_2MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BPH-8M15-MU6MU4")
            {
                L1_BPH_8M15_MU6MU4 = cg->isPassed();
                PS_L1_BPH_8M15_MU6MU4 = cg->getPrescale();
                TAV_L1_BPH_8M15_MU6MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_BPH_8M15_MU6MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_BPH_8M15_MU6MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BPH-8M15-2MU6")
            {
                L1_BPH_8M15_2MU6 = cg->isPassed();
                PS_L1_BPH_8M15_2MU6 = cg->getPrescale();
                TAV_L1_BPH_8M15_2MU6 = cg->isPassedBits() & TAVmask;
                TBP_L1_BPH_8M15_2MU6 = cg->isPassedBits() & TBPmask;
                TAP_L1_BPH_8M15_2MU6 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J40.0ETA25_2J15.31ETA49")
            {
                L1_J40_0ETA25_2J15_31ETA49 = cg->isPassed();
                PS_L1_J40_0ETA25_2J15_31ETA49 = cg->getPrescale();
                TAV_L1_J40_0ETA25_2J15_31ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_J40_0ETA25_2J15_31ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_J40_0ETA25_2J15_31ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J40.0ETA25_2J25_J20.31ETA49")
            {
                L1_J40_0ETA25_2J25_J20_31ETA49 = cg->isPassed();
                PS_L1_J40_0ETA25_2J25_J20_31ETA49 = cg->getPrescale();
                TAV_L1_J40_0ETA25_2J25_J20_31ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_J40_0ETA25_2J25_J20_31ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_J40_0ETA25_2J25_J20_31ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_C_MBTS_A")
            {
                L1_AFP_C_MBTS_A = cg->isPassed();
                PS_L1_AFP_C_MBTS_A = cg->getPrescale();
                TAV_L1_AFP_C_MBTS_A = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_C_MBTS_A = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_C_MBTS_A = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TGC_BURST_EMPTY")
            {
                L1_TGC_BURST_EMPTY = cg->isPassed();
                PS_L1_TGC_BURST_EMPTY = cg->getPrescale();
                TAV_L1_TGC_BURST_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_TGC_BURST_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_TGC_BURST_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_4_A_UNPAIRED_ISO")
            {
                L1_MBTS_4_A_UNPAIRED_ISO = cg->isPassed();
                PS_L1_MBTS_4_A_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_MBTS_4_A_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_4_A_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_4_A_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_4_C_UNPAIRED_ISO")
            {
                L1_MBTS_4_C_UNPAIRED_ISO = cg->isPassed();
                PS_L1_MBTS_4_C_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_MBTS_4_C_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_4_C_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_4_C_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE300")
            {
                L1_XE300 = cg->isPassed();
                PS_L1_XE300 = cg->getPrescale();
                TAV_L1_XE300 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE300 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE300 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD1_BGRP10")
            {
                L1_RD1_BGRP10 = cg->isPassed();
                PS_L1_RD1_BGRP10 = cg->getPrescale();
                TAV_L1_RD1_BGRP10 = cg->isPassedBits() & TAVmask;
                TBP_L1_RD1_BGRP10 = cg->isPassedBits() & TBPmask;
                TAP_L1_RD1_BGRP10 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_C_ZDC_C")
            {
                L1_AFP_C_ZDC_C = cg->isPassed();
                PS_L1_AFP_C_ZDC_C = cg->getPrescale();
                TAV_L1_AFP_C_ZDC_C = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_C_ZDC_C = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_C_ZDC_C = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_C_J12")
            {
                L1_AFP_C_J12 = cg->isPassed();
                PS_L1_AFP_C_J12 = cg->getPrescale();
                TAV_L1_AFP_C_J12 = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_C_J12 = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_C_J12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_C_EM3")
            {
                L1_AFP_C_EM3 = cg->isPassed();
                PS_L1_AFP_C_EM3 = cg->getPrescale();
                TAV_L1_AFP_C_EM3 = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_C_EM3 = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_C_EM3 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_C_TE5")
            {
                L1_AFP_C_TE5 = cg->isPassed();
                PS_L1_AFP_C_TE5 = cg->getPrescale();
                TAV_L1_AFP_C_TE5 = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_C_TE5 = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_C_TE5 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_C_ALFA_C")
            {
                L1_AFP_C_ALFA_C = cg->isPassed();
                PS_L1_AFP_C_ALFA_C = cg->getPrescale();
                TAV_L1_AFP_C_ALFA_C = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_C_ALFA_C = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_C_ALFA_C = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_4_A")
            {
                L1_MBTS_4_A = cg->isPassed();
                PS_L1_MBTS_4_A = cg->getPrescale();
                TAV_L1_MBTS_4_A = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_4_A = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_4_A = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_4_C")
            {
                L1_MBTS_4_C = cg->isPassed();
                PS_L1_MBTS_4_C = cg->getPrescale();
                TAV_L1_MBTS_4_C = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_4_C = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_4_C = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_1_BGRP9")
            {
                L1_MBTS_1_BGRP9 = cg->isPassed();
                PS_L1_MBTS_1_BGRP9 = cg->getPrescale();
                TAV_L1_MBTS_1_BGRP9 = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_1_BGRP9 = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_1_BGRP9 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_2_BGRP9")
            {
                L1_MBTS_2_BGRP9 = cg->isPassed();
                PS_L1_MBTS_2_BGRP9 = cg->getPrescale();
                TAV_L1_MBTS_2_BGRP9 = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_2_BGRP9 = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_2_BGRP9 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_1_BGRP11")
            {
                L1_MBTS_1_BGRP11 = cg->isPassed();
                PS_L1_MBTS_1_BGRP11 = cg->getPrescale();
                TAV_L1_MBTS_1_BGRP11 = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_1_BGRP11 = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_1_BGRP11 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_2_BGRP11")
            {
                L1_MBTS_2_BGRP11 = cg->isPassed();
                PS_L1_MBTS_2_BGRP11 = cg->getPrescale();
                TAV_L1_MBTS_2_BGRP11 = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_2_BGRP11 = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_2_BGRP11 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD0_FILLED")
            {
                L1_RD0_FILLED = cg->isPassed();
                PS_L1_RD0_FILLED = cg->getPrescale();
                TAV_L1_RD0_FILLED = cg->isPassedBits() & TAVmask;
                TBP_L1_RD0_FILLED = cg->isPassedBits() & TBPmask;
                TAP_L1_RD0_FILLED = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD0_UNPAIRED_ISO")
            {
                L1_RD0_UNPAIRED_ISO = cg->isPassed();
                PS_L1_RD0_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_RD0_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_RD0_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_RD0_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD0_EMPTY")
            {
                L1_RD0_EMPTY = cg->isPassed();
                PS_L1_RD0_EMPTY = cg->getPrescale();
                TAV_L1_RD0_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_RD0_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_RD0_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD1_FILLED")
            {
                L1_RD1_FILLED = cg->isPassed();
                PS_L1_RD1_FILLED = cg->getPrescale();
                TAV_L1_RD1_FILLED = cg->isPassedBits() & TAVmask;
                TBP_L1_RD1_FILLED = cg->isPassedBits() & TBPmask;
                TAP_L1_RD1_FILLED = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD1_EMPTY")
            {
                L1_RD1_EMPTY = cg->isPassed();
                PS_L1_RD1_EMPTY = cg->getPrescale();
                TAV_L1_RD1_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_RD1_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_RD1_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD2_FILLED")
            {
                L1_RD2_FILLED = cg->isPassed();
                PS_L1_RD2_FILLED = cg->getPrescale();
                TAV_L1_RD2_FILLED = cg->isPassedBits() & TAVmask;
                TBP_L1_RD2_FILLED = cg->isPassedBits() & TBPmask;
                TAP_L1_RD2_FILLED = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD2_EMPTY")
            {
                L1_RD2_EMPTY = cg->isPassed();
                PS_L1_RD2_EMPTY = cg->getPrescale();
                TAV_L1_RD2_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_RD2_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_RD2_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD3_FILLED")
            {
                L1_RD3_FILLED = cg->isPassed();
                PS_L1_RD3_FILLED = cg->getPrescale();
                TAV_L1_RD3_FILLED = cg->isPassedBits() & TAVmask;
                TBP_L1_RD3_FILLED = cg->isPassedBits() & TBPmask;
                TAP_L1_RD3_FILLED = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD3_EMPTY")
            {
                L1_RD3_EMPTY = cg->isPassed();
                PS_L1_RD3_EMPTY = cg->getPrescale();
                TAV_L1_RD3_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_RD3_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_RD3_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD0_FIRSTEMPTY")
            {
                L1_RD0_FIRSTEMPTY = cg->isPassed();
                PS_L1_RD0_FIRSTEMPTY = cg->getPrescale();
                TAV_L1_RD0_FIRSTEMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_RD0_FIRSTEMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_RD0_FIRSTEMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD0_BGRP9")
            {
                L1_RD0_BGRP9 = cg->isPassed();
                PS_L1_RD0_BGRP9 = cg->getPrescale();
                TAV_L1_RD0_BGRP9 = cg->isPassedBits() & TAVmask;
                TBP_L1_RD0_BGRP9 = cg->isPassedBits() & TBPmask;
                TAP_L1_RD0_BGRP9 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD0_BGRP11")
            {
                L1_RD0_BGRP11 = cg->isPassed();
                PS_L1_RD0_BGRP11 = cg->getPrescale();
                TAV_L1_RD0_BGRP11 = cg->isPassedBits() & TAVmask;
                TBP_L1_RD0_BGRP11 = cg->isPassedBits() & TBPmask;
                TAP_L1_RD0_BGRP11 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LUCID")
            {
                L1_LUCID = cg->isPassed();
                PS_L1_LUCID = cg->getPrescale();
                TAV_L1_LUCID = cg->isPassedBits() & TAVmask;
                TBP_L1_LUCID = cg->isPassedBits() & TBPmask;
                TAP_L1_LUCID = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LUCID_EMPTY")
            {
                L1_LUCID_EMPTY = cg->isPassed();
                PS_L1_LUCID_EMPTY = cg->getPrescale();
                TAV_L1_LUCID_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_LUCID_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_LUCID_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LUCID_UNPAIRED_ISO")
            {
                L1_LUCID_UNPAIRED_ISO = cg->isPassed();
                PS_L1_LUCID_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_LUCID_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_LUCID_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_LUCID_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LUCID_A_C_EMPTY")
            {
                L1_LUCID_A_C_EMPTY = cg->isPassed();
                PS_L1_LUCID_A_C_EMPTY = cg->getPrescale();
                TAV_L1_LUCID_A_C_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_LUCID_A_C_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_LUCID_A_C_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LUCID_A_C_UNPAIRED_ISO")
            {
                L1_LUCID_A_C_UNPAIRED_ISO = cg->isPassed();
                PS_L1_LUCID_A_C_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_LUCID_A_C_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_LUCID_A_C_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_LUCID_A_C_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LUCID_A_C_UNPAIRED_NONISO")
            {
                L1_LUCID_A_C_UNPAIRED_NONISO = cg->isPassed();
                PS_L1_LUCID_A_C_UNPAIRED_NONISO = cg->getPrescale();
                TAV_L1_LUCID_A_C_UNPAIRED_NONISO = cg->isPassedBits() & TAVmask;
                TBP_L1_LUCID_A_C_UNPAIRED_NONISO = cg->isPassedBits() & TBPmask;
                TAP_L1_LUCID_A_C_UNPAIRED_NONISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TRT_FILLED")
            {
                L1_TRT_FILLED = cg->isPassed();
                PS_L1_TRT_FILLED = cg->getPrescale();
                TAV_L1_TRT_FILLED = cg->isPassedBits() & TAVmask;
                TBP_L1_TRT_FILLED = cg->isPassedBits() & TBPmask;
                TAP_L1_TRT_FILLED = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TRT_EMPTY")
            {
                L1_TRT_EMPTY = cg->isPassed();
                PS_L1_TRT_EMPTY = cg->getPrescale();
                TAV_L1_TRT_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_TRT_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_TRT_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TGC_BURST")
            {
                L1_TGC_BURST = cg->isPassed();
                PS_L1_TGC_BURST = cg->getPrescale();
                TAV_L1_TGC_BURST = cg->isPassedBits() & TAVmask;
                TBP_L1_TGC_BURST = cg->isPassedBits() & TBPmask;
                TAP_L1_TGC_BURST = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LHCF")
            {
                L1_LHCF = cg->isPassed();
                PS_L1_LHCF = cg->getPrescale();
                TAV_L1_LHCF = cg->isPassedBits() & TAVmask;
                TBP_L1_LHCF = cg->isPassedBits() & TBPmask;
                TAP_L1_LHCF = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_Wide_BGRP0")
            {
                L1_BCM_Wide_BGRP0 = cg->isPassed();
                PS_L1_BCM_Wide_BGRP0 = cg->getPrescale();
                TAV_L1_BCM_Wide_BGRP0 = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_Wide_BGRP0 = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_Wide_BGRP0 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_AC_CA_BGRP0")
            {
                L1_BCM_AC_CA_BGRP0 = cg->isPassed();
                PS_L1_BCM_AC_CA_BGRP0 = cg->getPrescale();
                TAV_L1_BCM_AC_CA_BGRP0 = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_AC_CA_BGRP0 = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_AC_CA_BGRP0 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_Wide_EMPTY")
            {
                L1_BCM_Wide_EMPTY = cg->isPassed();
                PS_L1_BCM_Wide_EMPTY = cg->getPrescale();
                TAV_L1_BCM_Wide_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_Wide_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_Wide_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_Wide_UNPAIRED_ISO")
            {
                L1_BCM_Wide_UNPAIRED_ISO = cg->isPassed();
                PS_L1_BCM_Wide_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_BCM_Wide_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_Wide_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_Wide_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_1")
            {
                L1_MBTS_1 = cg->isPassed();
                PS_L1_MBTS_1 = cg->getPrescale();
                TAV_L1_MBTS_1 = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_1 = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_1 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_2")
            {
                L1_MBTS_2 = cg->isPassed();
                PS_L1_MBTS_2 = cg->getPrescale();
                TAV_L1_MBTS_2 = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_2 = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_2 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_1_1")
            {
                L1_MBTS_1_1 = cg->isPassed();
                PS_L1_MBTS_1_1 = cg->getPrescale();
                TAV_L1_MBTS_1_1 = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_1_1 = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_1_1 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_CA_UNPAIRED_ISO")
            {
                L1_BCM_CA_UNPAIRED_ISO = cg->isPassed();
                PS_L1_BCM_CA_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_BCM_CA_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_CA_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_CA_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_AC_UNPAIRED_NONISO")
            {
                L1_BCM_AC_UNPAIRED_NONISO = cg->isPassed();
                PS_L1_BCM_AC_UNPAIRED_NONISO = cg->getPrescale();
                TAV_L1_BCM_AC_UNPAIRED_NONISO = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_AC_UNPAIRED_NONISO = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_AC_UNPAIRED_NONISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_CA_UNPAIRED_NONISO")
            {
                L1_BCM_CA_UNPAIRED_NONISO = cg->isPassed();
                PS_L1_BCM_CA_UNPAIRED_NONISO = cg->getPrescale();
                TAV_L1_BCM_CA_UNPAIRED_NONISO = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_CA_UNPAIRED_NONISO = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_CA_UNPAIRED_NONISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_AC_ABORTGAPNOTCALIB")
            {
                L1_BCM_AC_ABORTGAPNOTCALIB = cg->isPassed();
                PS_L1_BCM_AC_ABORTGAPNOTCALIB = cg->getPrescale();
                TAV_L1_BCM_AC_ABORTGAPNOTCALIB = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_AC_ABORTGAPNOTCALIB = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_AC_ABORTGAPNOTCALIB = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_CA_ABORTGAPNOTCALIB")
            {
                L1_BCM_CA_ABORTGAPNOTCALIB = cg->isPassed();
                PS_L1_BCM_CA_ABORTGAPNOTCALIB = cg->getPrescale();
                TAV_L1_BCM_CA_ABORTGAPNOTCALIB = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_CA_ABORTGAPNOTCALIB = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_CA_ABORTGAPNOTCALIB = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_Wide_ABORTGAPNOTCALIB")
            {
                L1_BCM_Wide_ABORTGAPNOTCALIB = cg->isPassed();
                PS_L1_BCM_Wide_ABORTGAPNOTCALIB = cg->getPrescale();
                TAV_L1_BCM_Wide_ABORTGAPNOTCALIB = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_Wide_ABORTGAPNOTCALIB = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_Wide_ABORTGAPNOTCALIB = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_AC_CALIB")
            {
                L1_BCM_AC_CALIB = cg->isPassed();
                PS_L1_BCM_AC_CALIB = cg->getPrescale();
                TAV_L1_BCM_AC_CALIB = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_AC_CALIB = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_AC_CALIB = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_CA_CALIB")
            {
                L1_BCM_CA_CALIB = cg->isPassed();
                PS_L1_BCM_CA_CALIB = cg->getPrescale();
                TAV_L1_BCM_CA_CALIB = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_CA_CALIB = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_CA_CALIB = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BCM_Wide_CALIB")
            {
                L1_BCM_Wide_CALIB = cg->isPassed();
                PS_L1_BCM_Wide_CALIB = cg->getPrescale();
                TAV_L1_BCM_Wide_CALIB = cg->isPassedBits() & TAVmask;
                TBP_L1_BCM_Wide_CALIB = cg->isPassedBits() & TBPmask;
                TAP_L1_BCM_Wide_CALIB = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BTAG-MU4J15")
            {
                L1_BTAG_MU4J15 = cg->isPassed();
                PS_L1_BTAG_MU4J15 = cg->getPrescale();
                TAV_L1_BTAG_MU4J15 = cg->isPassedBits() & TAVmask;
                TBP_L1_BTAG_MU4J15 = cg->isPassedBits() & TBPmask;
                TAP_L1_BTAG_MU4J15 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BTAG-MU4J30")
            {
                L1_BTAG_MU4J30 = cg->isPassed();
                PS_L1_BTAG_MU4J30 = cg->getPrescale();
                TAV_L1_BTAG_MU4J30 = cg->isPassedBits() & TAVmask;
                TBP_L1_BTAG_MU4J30 = cg->isPassedBits() & TBPmask;
                TAP_L1_BTAG_MU4J30 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ZB")
            {
                L1_ZB = cg->isPassed();
                PS_L1_ZB = cg->getPrescale();
                TAV_L1_ZB = cg->isPassedBits() & TAVmask;
                TBP_L1_ZB = cg->isPassedBits() & TBPmask;
                TAP_L1_ZB = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BPTX0_BGRP0")
            {
                L1_BPTX0_BGRP0 = cg->isPassed();
                PS_L1_BPTX0_BGRP0 = cg->getPrescale();
                TAV_L1_BPTX0_BGRP0 = cg->isPassedBits() & TAVmask;
                TBP_L1_BPTX0_BGRP0 = cg->isPassedBits() & TBPmask;
                TAP_L1_BPTX0_BGRP0 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BPTX1_BGRP0")
            {
                L1_BPTX1_BGRP0 = cg->isPassed();
                PS_L1_BPTX1_BGRP0 = cg->getPrescale();
                TAV_L1_BPTX1_BGRP0 = cg->isPassedBits() & TAVmask;
                TBP_L1_BPTX1_BGRP0 = cg->isPassedBits() & TBPmask;
                TAP_L1_BPTX1_BGRP0 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BTAG-MU6J20")
            {
                L1_BTAG_MU6J20 = cg->isPassed();
                PS_L1_BTAG_MU6J20 = cg->getPrescale();
                TAV_L1_BTAG_MU6J20 = cg->isPassedBits() & TAVmask;
                TBP_L1_BTAG_MU6J20 = cg->isPassedBits() & TBPmask;
                TAP_L1_BTAG_MU6J20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BTAG-MU6J25")
            {
                L1_BTAG_MU6J25 = cg->isPassed();
                PS_L1_BTAG_MU6J25 = cg->getPrescale();
                TAV_L1_BTAG_MU6J25 = cg->isPassedBits() & TAVmask;
                TBP_L1_BTAG_MU6J25 = cg->isPassedBits() & TBPmask;
                TAP_L1_BTAG_MU6J25 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_C_ANY_UNPAIRED_ISO")
            {
                L1_AFP_C_ANY_UNPAIRED_ISO = cg->isPassed();
                PS_L1_AFP_C_ANY_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_AFP_C_ANY_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_C_ANY_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_C_ANY_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3J15_BTAG-MU4J15")
            {
                L1_3J15_BTAG_MU4J15 = cg->isPassed();
                PS_L1_3J15_BTAG_MU4J15 = cg->getPrescale();
                TAV_L1_3J15_BTAG_MU4J15 = cg->isPassedBits() & TAVmask;
                TBP_L1_3J15_BTAG_MU4J15 = cg->isPassedBits() & TBPmask;
                TAP_L1_3J15_BTAG_MU4J15 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3J15_BTAG-MU4J30")
            {
                L1_3J15_BTAG_MU4J30 = cg->isPassed();
                PS_L1_3J15_BTAG_MU4J30 = cg->getPrescale();
                TAV_L1_3J15_BTAG_MU4J30 = cg->isPassedBits() & TAVmask;
                TBP_L1_3J15_BTAG_MU4J30 = cg->isPassedBits() & TBPmask;
                TAP_L1_3J15_BTAG_MU4J30 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3J15_BTAG-MU6J25")
            {
                L1_3J15_BTAG_MU6J25 = cg->isPassed();
                PS_L1_3J15_BTAG_MU6J25 = cg->getPrescale();
                TAV_L1_3J15_BTAG_MU6J25 = cg->isPassedBits() & TAVmask;
                TBP_L1_3J15_BTAG_MU6J25 = cg->isPassedBits() & TBPmask;
                TAP_L1_3J15_BTAG_MU6J25 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3J20_BTAG-MU4J20")
            {
                L1_3J20_BTAG_MU4J20 = cg->isPassed();
                PS_L1_3J20_BTAG_MU4J20 = cg->getPrescale();
                TAV_L1_3J20_BTAG_MU4J20 = cg->isPassedBits() & TAVmask;
                TBP_L1_3J20_BTAG_MU4J20 = cg->isPassedBits() & TBPmask;
                TAP_L1_3J20_BTAG_MU4J20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J40_DPHI-Js2XE50")
            {
                L1_J40_DPHI_Js2XE50 = cg->isPassed();
                PS_L1_J40_DPHI_Js2XE50 = cg->getPrescale();
                TAV_L1_J40_DPHI_Js2XE50 = cg->isPassedBits() & TAVmask;
                TBP_L1_J40_DPHI_Js2XE50 = cg->isPassedBits() & TBPmask;
                TAP_L1_J40_DPHI_Js2XE50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J40_DPHI-J20s2XE50")
            {
                L1_J40_DPHI_J20s2XE50 = cg->isPassed();
                PS_L1_J40_DPHI_J20s2XE50 = cg->getPrescale();
                TAV_L1_J40_DPHI_J20s2XE50 = cg->isPassedBits() & TAVmask;
                TBP_L1_J40_DPHI_J20s2XE50 = cg->isPassedBits() & TBPmask;
                TAP_L1_J40_DPHI_J20s2XE50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J40_DPHI-J20XE50")
            {
                L1_J40_DPHI_J20XE50 = cg->isPassed();
                PS_L1_J40_DPHI_J20XE50 = cg->getPrescale();
                TAV_L1_J40_DPHI_J20XE50 = cg->isPassedBits() & TAVmask;
                TBP_L1_J40_DPHI_J20XE50 = cg->isPassedBits() & TBPmask;
                TAP_L1_J40_DPHI_J20XE50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J40_DPHI-CJ20XE50")
            {
                L1_J40_DPHI_CJ20XE50 = cg->isPassed();
                PS_L1_J40_DPHI_CJ20XE50 = cg->getPrescale();
                TAV_L1_J40_DPHI_CJ20XE50 = cg->isPassedBits() & TAVmask;
                TBP_L1_J40_DPHI_CJ20XE50 = cg->isPassedBits() & TBPmask;
                TAP_L1_J40_DPHI_CJ20XE50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU40_2TAU20IM")
            {
                L1_TAU40_2TAU20IM = cg->isPassed();
                PS_L1_TAU40_2TAU20IM = cg->getPrescale();
                TAV_L1_TAU40_2TAU20IM = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU40_2TAU20IM = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU40_2TAU20IM = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU10_2J15_J20")
            {
                L1_MU10_2J15_J20 = cg->isPassed();
                PS_L1_MU10_2J15_J20 = cg->getPrescale();
                TAV_L1_MU10_2J15_J20 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU10_2J15_J20 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU10_2J15_J20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU11")
            {
                L1_MU11 = cg->isPassed();
                PS_L1_MU11 = cg->getPrescale();
                TAV_L1_MU11 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU11 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU11 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_C_ANY_UNPAIRED_NONISO")
            {
                L1_AFP_C_ANY_UNPAIRED_NONISO = cg->isPassed();
                PS_L1_AFP_C_ANY_UNPAIRED_NONISO = cg->getPrescale();
                TAV_L1_AFP_C_ANY_UNPAIRED_NONISO = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_C_ANY_UNPAIRED_NONISO = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_C_ANY_UNPAIRED_NONISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_HT190-J15.ETA21")
            {
                L1_HT190_J15_ETA21 = cg->isPassed();
                PS_L1_HT190_J15_ETA21 = cg->getPrescale();
                TAV_L1_HT190_J15_ETA21 = cg->isPassedBits() & TAVmask;
                TBP_L1_HT190_J15_ETA21 = cg->isPassedBits() & TBPmask;
                TAP_L1_HT190_J15_ETA21 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_HT190-J15s5.ETA21")
            {
                L1_HT190_J15s5_ETA21 = cg->isPassed();
                PS_L1_HT190_J15s5_ETA21 = cg->getPrescale();
                TAV_L1_HT190_J15s5_ETA21 = cg->isPassedBits() & TAVmask;
                TBP_L1_HT190_J15s5_ETA21 = cg->isPassedBits() & TBPmask;
                TAP_L1_HT190_J15s5_ETA21 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_HT150-J20.ETA31")
            {
                L1_HT150_J20_ETA31 = cg->isPassed();
                PS_L1_HT150_J20_ETA31 = cg->getPrescale();
                TAV_L1_HT150_J20_ETA31 = cg->isPassedBits() & TAVmask;
                TBP_L1_HT150_J20_ETA31 = cg->isPassedBits() & TBPmask;
                TAP_L1_HT150_J20_ETA31 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_HT150-J20s5.ETA31")
            {
                L1_HT150_J20s5_ETA31 = cg->isPassed();
                PS_L1_HT150_J20s5_ETA31 = cg->getPrescale();
                TAV_L1_HT150_J20s5_ETA31 = cg->isPassedBits() & TAVmask;
                TBP_L1_HT150_J20s5_ETA31 = cg->isPassedBits() & TBPmask;
                TAP_L1_HT150_J20s5_ETA31 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_JPSI-1M5")
            {
                L1_JPSI_1M5 = cg->isPassed();
                PS_L1_JPSI_1M5 = cg->getPrescale();
                TAV_L1_JPSI_1M5 = cg->isPassedBits() & TAVmask;
                TBP_L1_JPSI_1M5 = cg->isPassedBits() & TBPmask;
                TAP_L1_JPSI_1M5 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_JPSI-1M5-EM7")
            {
                L1_JPSI_1M5_EM7 = cg->isPassed();
                PS_L1_JPSI_1M5_EM7 = cg->getPrescale();
                TAV_L1_JPSI_1M5_EM7 = cg->isPassedBits() & TAVmask;
                TBP_L1_JPSI_1M5_EM7 = cg->isPassedBits() & TBPmask;
                TAP_L1_JPSI_1M5_EM7 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_JPSI-1M5-EM12")
            {
                L1_JPSI_1M5_EM12 = cg->isPassed();
                PS_L1_JPSI_1M5_EM12 = cg->getPrescale();
                TAV_L1_JPSI_1M5_EM12 = cg->isPassedBits() & TAVmask;
                TBP_L1_JPSI_1M5_EM12 = cg->isPassedBits() & TBPmask;
                TAP_L1_JPSI_1M5_EM12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_KF-XE35")
            {
                L1_KF_XE35 = cg->isPassed();
                PS_L1_KF_XE35 = cg->getPrescale();
                TAV_L1_KF_XE35 = cg->isPassedBits() & TAVmask;
                TBP_L1_KF_XE35 = cg->isPassedBits() & TBPmask;
                TAP_L1_KF_XE35 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_KF-XE45")
            {
                L1_KF_XE45 = cg->isPassed();
                PS_L1_KF_XE45 = cg->getPrescale();
                TAV_L1_KF_XE45 = cg->isPassedBits() & TAVmask;
                TBP_L1_KF_XE45 = cg->isPassedBits() & TBPmask;
                TAP_L1_KF_XE45 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_KF-XE55")
            {
                L1_KF_XE55 = cg->isPassed();
                PS_L1_KF_XE55 = cg->getPrescale();
                TAV_L1_KF_XE55 = cg->isPassedBits() & TAVmask;
                TBP_L1_KF_XE55 = cg->isPassedBits() & TBPmask;
                TAP_L1_KF_XE55 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_KF-XE60")
            {
                L1_KF_XE60 = cg->isPassed();
                PS_L1_KF_XE60 = cg->getPrescale();
                TAV_L1_KF_XE60 = cg->isPassedBits() & TAVmask;
                TBP_L1_KF_XE60 = cg->isPassedBits() & TBPmask;
                TAP_L1_KF_XE60 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_KF-XE65")
            {
                L1_KF_XE65 = cg->isPassed();
                PS_L1_KF_XE65 = cg->getPrescale();
                TAV_L1_KF_XE65 = cg->isPassedBits() & TAVmask;
                TBP_L1_KF_XE65 = cg->isPassedBits() & TBPmask;
                TAP_L1_KF_XE65 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_KF-XE75")
            {
                L1_KF_XE75 = cg->isPassed();
                PS_L1_KF_XE75 = cg->getPrescale();
                TAV_L1_KF_XE75 = cg->isPassedBits() & TAVmask;
                TBP_L1_KF_XE75 = cg->isPassedBits() & TBPmask;
                TAP_L1_KF_XE75 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_C_ANY_EMPTY")
            {
                L1_AFP_C_ANY_EMPTY = cg->isPassed();
                PS_L1_AFP_C_ANY_EMPTY = cg->getPrescale();
                TAV_L1_AFP_C_ANY_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_C_ANY_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_C_ANY_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_C_ANY_FIRSTEMPTY")
            {
                L1_AFP_C_ANY_FIRSTEMPTY = cg->isPassed();
                PS_L1_AFP_C_ANY_FIRSTEMPTY = cg->getPrescale();
                TAV_L1_AFP_C_ANY_FIRSTEMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_C_ANY_FIRSTEMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_C_ANY_FIRSTEMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_AFP_C_AND")
            {
                L1_AFP_C_AND = cg->isPassed();
                PS_L1_AFP_C_AND = cg->getPrescale();
                TAV_L1_AFP_C_AND = cg->isPassedBits() & TAVmask;
                TBP_L1_AFP_C_AND = cg->isPassedBits() & TBPmask;
                TAP_L1_AFP_C_AND = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM12_W-MT25")
            {
                L1_EM12_W_MT25 = cg->isPassed();
                PS_L1_EM12_W_MT25 = cg->getPrescale();
                TAV_L1_EM12_W_MT25 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM12_W_MT25 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM12_W_MT25 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM12_W-MT30")
            {
                L1_EM12_W_MT30 = cg->isPassed();
                PS_L1_EM12_W_MT30 = cg->getPrescale();
                TAV_L1_EM12_W_MT30 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM12_W_MT30 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM12_W_MT30 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15_W-MT35_W-250RO2-XEHT-0_W-05DPHI-JXE-0_W-05DPHI-EM15XE")
            {
                L1_EM15_W_MT35_W_250RO2_XEHT_0_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassed();
                PS_L1_EM15_W_MT35_W_250RO2_XEHT_0_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->getPrescale();
                TAV_L1_EM15_W_MT35_W_250RO2_XEHT_0_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15_W_MT35_W_250RO2_XEHT_0_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15_W_MT35_W_250RO2_XEHT_0_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_W-05RO-XEHT-0")
            {
                L1_W_05RO_XEHT_0 = cg->isPassed();
                PS_L1_W_05RO_XEHT_0 = cg->getPrescale();
                TAV_L1_W_05RO_XEHT_0 = cg->isPassedBits() & TAVmask;
                TBP_L1_W_05RO_XEHT_0 = cg->isPassedBits() & TBPmask;
                TAP_L1_W_05RO_XEHT_0 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU10_2J20")
            {
                L1_MU10_2J20 = cg->isPassed();
                PS_L1_MU10_2J20 = cg->getPrescale();
                TAV_L1_MU10_2J20 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU10_2J20 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU10_2J20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_W-90RO2-XEHT-0")
            {
                L1_W_90RO2_XEHT_0 = cg->isPassed();
                PS_L1_W_90RO2_XEHT_0 = cg->getPrescale();
                TAV_L1_W_90RO2_XEHT_0 = cg->isPassedBits() & TAVmask;
                TBP_L1_W_90RO2_XEHT_0 = cg->isPassedBits() & TBPmask;
                TAP_L1_W_90RO2_XEHT_0 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_W-250RO2-XEHT-0")
            {
                L1_W_250RO2_XEHT_0 = cg->isPassed();
                PS_L1_W_250RO2_XEHT_0 = cg->getPrescale();
                TAV_L1_W_250RO2_XEHT_0 = cg->isPassedBits() & TAVmask;
                TBP_L1_W_250RO2_XEHT_0 = cg->isPassedBits() & TBPmask;
                TAP_L1_W_250RO2_XEHT_0 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_W-HT20-JJ15.ETA49")
            {
                L1_W_HT20_JJ15_ETA49 = cg->isPassed();
                PS_L1_W_HT20_JJ15_ETA49 = cg->getPrescale();
                TAV_L1_W_HT20_JJ15_ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_W_HT20_JJ15_ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_W_HT20_JJ15_ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_W-NOMATCH")
            {
                L1_W_NOMATCH = cg->isPassed();
                PS_L1_W_NOMATCH = cg->getPrescale();
                TAV_L1_W_NOMATCH = cg->isPassedBits() & TAVmask;
                TBP_L1_W_NOMATCH = cg->isPassedBits() & TBPmask;
                TAP_L1_W_NOMATCH = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_W-NOMATCH_W-05RO-XEEMHT")
            {
                L1_W_NOMATCH_W_05RO_XEEMHT = cg->isPassed();
                PS_L1_W_NOMATCH_W_05RO_XEEMHT = cg->getPrescale();
                TAV_L1_W_NOMATCH_W_05RO_XEEMHT = cg->isPassedBits() & TAVmask;
                TBP_L1_W_NOMATCH_W_05RO_XEEMHT = cg->isPassedBits() & TBPmask;
                TAP_L1_W_NOMATCH_W_05RO_XEEMHT = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15_W-MT35_XS60_W-05DPHI-JXE-0_W-05DPHI-EM15XE")
            {
                L1_EM15_W_MT35_XS60_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassed();
                PS_L1_EM15_W_MT35_XS60_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->getPrescale();
                TAV_L1_EM15_W_MT35_XS60_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15_W_MT35_XS60_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15_W_MT35_XS60_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15_W-MT35_XS40_W-05DPHI-JXE-0_W-05DPHI-EM15XE")
            {
                L1_EM15_W_MT35_XS40_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassed();
                PS_L1_EM15_W_MT35_XS40_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->getPrescale();
                TAV_L1_EM15_W_MT35_XS40_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15_W_MT35_XS40_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15_W_MT35_XS40_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15_W-MT35")
            {
                L1_EM15_W_MT35 = cg->isPassed();
                PS_L1_EM15_W_MT35 = cg->getPrescale();
                TAV_L1_EM15_W_MT35 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15_W_MT35 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15_W_MT35 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15_W-MT35_XS60")
            {
                L1_EM15_W_MT35_XS60 = cg->isPassed();
                PS_L1_EM15_W_MT35_XS60 = cg->getPrescale();
                TAV_L1_EM15_W_MT35_XS60 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15_W_MT35_XS60 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15_W_MT35_XS60 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15VH_W-MT35_XS60")
            {
                L1_EM15VH_W_MT35_XS60 = cg->isPassed();
                PS_L1_EM15VH_W_MT35_XS60 = cg->getPrescale();
                TAV_L1_EM15VH_W_MT35_XS60 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15VH_W_MT35_XS60 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15VH_W_MT35_XS60 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM20VH_W-MT35_XS60")
            {
                L1_EM20VH_W_MT35_XS60 = cg->isPassed();
                PS_L1_EM20VH_W_MT35_XS60 = cg->getPrescale();
                TAV_L1_EM20VH_W_MT35_XS60 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM20VH_W_MT35_XS60 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM20VH_W_MT35_XS60 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM22VHI_W-MT35_XS40")
            {
                L1_EM22VHI_W_MT35_XS40 = cg->isPassed();
                PS_L1_EM22VHI_W_MT35_XS40 = cg->getPrescale();
                TAV_L1_EM22VHI_W_MT35_XS40 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM22VHI_W_MT35_XS40 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM22VHI_W_MT35_XS40 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15_W-MT35_W-05DPHI-JXE-0_W-05DPHI-EM15XE_XS30")
            {
                L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE_XS30 = cg->isPassed();
                PS_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE_XS30 = cg->getPrescale();
                TAV_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE_XS30 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE_XS30 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE_XS30 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15_W-MT35_W-05DPHI-JXE-0_W-05DPHI-EM15XE")
            {
                L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassed();
                PS_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->getPrescale();
                TAV_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15_W_MT35_W_05DPHI_JXE_0_W_05DPHI_EM15XE = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_HT150-JJ15.ETA49_MJJ-400")
            {
                L1_HT150_JJ15_ETA49_MJJ_400 = cg->isPassed();
                PS_L1_HT150_JJ15_ETA49_MJJ_400 = cg->getPrescale();
                TAV_L1_HT150_JJ15_ETA49_MJJ_400 = cg->isPassedBits() & TAVmask;
                TBP_L1_HT150_JJ15_ETA49_MJJ_400 = cg->isPassedBits() & TBPmask;
                TAP_L1_HT150_JJ15_ETA49_MJJ_400 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD2_BGRP14")
            {
                L1_RD2_BGRP14 = cg->isPassed();
                PS_L1_RD2_BGRP14 = cg->getPrescale();
                TAV_L1_RD2_BGRP14 = cg->isPassedBits() & TAVmask;
                TBP_L1_RD2_BGRP14 = cg->isPassedBits() & TBPmask;
                TAP_L1_RD2_BGRP14 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BPH-1M19-2MU4_BPH-0DR34-2MU4")
            {
                L1_BPH_1M19_2MU4_BPH_0DR34_2MU4 = cg->isPassed();
                PS_L1_BPH_1M19_2MU4_BPH_0DR34_2MU4 = cg->getPrescale();
                TAV_L1_BPH_1M19_2MU4_BPH_0DR34_2MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_BPH_1M19_2MU4_BPH_0DR34_2MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_BPH_1M19_2MU4_BPH_0DR34_2MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD3_BGRP15")
            {
                L1_RD3_BGRP15 = cg->isPassed();
                PS_L1_RD3_BGRP15 = cg->getPrescale();
                TAV_L1_RD3_BGRP15 = cg->isPassedBits() & TAVmask;
                TBP_L1_RD3_BGRP15 = cg->isPassedBits() & TBPmask;
                TAP_L1_RD3_BGRP15 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BPH-2M8-MU6MU4_BPH-0DR15-MU6MU4")
            {
                L1_BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4 = cg->isPassed();
                PS_L1_BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4 = cg->getPrescale();
                TAV_L1_BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BPH-2M9-2MU6_BPH-2DR15-2MU6")
            {
                L1_BPH_2M9_2MU6_BPH_2DR15_2MU6 = cg->isPassed();
                PS_L1_BPH_2M9_2MU6_BPH_2DR15_2MU6 = cg->getPrescale();
                TAV_L1_BPH_2M9_2MU6_BPH_2DR15_2MU6 = cg->isPassedBits() & TAVmask;
                TBP_L1_BPH_2M9_2MU6_BPH_2DR15_2MU6 = cg->isPassedBits() & TBPmask;
                TAP_L1_BPH_2M9_2MU6_BPH_2DR15_2MU6 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU6MU4-BO")
            {
                L1_MU6MU4_BO = cg->isPassed();
                PS_L1_MU6MU4_BO = cg->getPrescale();
                TAV_L1_MU6MU4_BO = cg->isPassedBits() & TAVmask;
                TBP_L1_MU6MU4_BO = cg->isPassedBits() & TBPmask;
                TAP_L1_MU6MU4_BO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2MU4-B")
            {
                L1_2MU4_B = cg->isPassed();
                PS_L1_2MU4_B = cg->getPrescale();
                TAV_L1_2MU4_B = cg->isPassedBits() & TAVmask;
                TBP_L1_2MU4_B = cg->isPassedBits() & TBPmask;
                TAP_L1_2MU4_B = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2MU6-B")
            {
                L1_2MU6_B = cg->isPassed();
                PS_L1_2MU6_B = cg->getPrescale();
                TAV_L1_2MU6_B = cg->isPassedBits() & TAVmask;
                TBP_L1_2MU6_B = cg->isPassedBits() & TBPmask;
                TAP_L1_2MU6_B = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BPH-1M19-2MU4-B_BPH-0DR34-2MU4")
            {
                L1_BPH_1M19_2MU4_B_BPH_0DR34_2MU4 = cg->isPassed();
                PS_L1_BPH_1M19_2MU4_B_BPH_0DR34_2MU4 = cg->getPrescale();
                TAV_L1_BPH_1M19_2MU4_B_BPH_0DR34_2MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_BPH_1M19_2MU4_B_BPH_0DR34_2MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_BPH_1M19_2MU4_B_BPH_0DR34_2MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BPH-1M19-2MU4-BO_BPH-0DR34-2MU4")
            {
                L1_BPH_1M19_2MU4_BO_BPH_0DR34_2MU4 = cg->isPassed();
                PS_L1_BPH_1M19_2MU4_BO_BPH_0DR34_2MU4 = cg->getPrescale();
                TAV_L1_BPH_1M19_2MU4_BO_BPH_0DR34_2MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_BPH_1M19_2MU4_BO_BPH_0DR34_2MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_BPH_1M19_2MU4_BO_BPH_0DR34_2MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BPH-2M8-MU6MU4-B_BPH-0DR15-MU6MU4")
            {
                L1_BPH_2M8_MU6MU4_B_BPH_0DR15_MU6MU4 = cg->isPassed();
                PS_L1_BPH_2M8_MU6MU4_B_BPH_0DR15_MU6MU4 = cg->getPrescale();
                TAV_L1_BPH_2M8_MU6MU4_B_BPH_0DR15_MU6MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_BPH_2M8_MU6MU4_B_BPH_0DR15_MU6MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_BPH_2M8_MU6MU4_B_BPH_0DR15_MU6MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2MU4-BO")
            {
                L1_2MU4_BO = cg->isPassed();
                PS_L1_2MU4_BO = cg->getPrescale();
                TAV_L1_2MU4_BO = cg->isPassedBits() & TAVmask;
                TBP_L1_2MU4_BO = cg->isPassedBits() & TBPmask;
                TAP_L1_2MU4_BO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2MU6-BO")
            {
                L1_2MU6_BO = cg->isPassed();
                PS_L1_2MU6_BO = cg->getPrescale();
                TAV_L1_2MU6_BO = cg->isPassedBits() & TAVmask;
                TBP_L1_2MU6_BO = cg->isPassedBits() & TBPmask;
                TAP_L1_2MU6_BO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU6_2MU4-B")
            {
                L1_MU6_2MU4_B = cg->isPassed();
                PS_L1_MU6_2MU4_B = cg->getPrescale();
                TAV_L1_MU6_2MU4_B = cg->isPassedBits() & TAVmask;
                TBP_L1_MU6_2MU4_B = cg->isPassedBits() & TBPmask;
                TAP_L1_MU6_2MU4_B = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_DY-DR-2MU4")
            {
                L1_DY_DR_2MU4 = cg->isPassed();
                PS_L1_DY_DR_2MU4 = cg->getPrescale();
                TAV_L1_DY_DR_2MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_DY_DR_2MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_DY_DR_2MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_DY-BOX-2MU4")
            {
                L1_DY_BOX_2MU4 = cg->isPassed();
                PS_L1_DY_BOX_2MU4 = cg->getPrescale();
                TAV_L1_DY_BOX_2MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_DY_BOX_2MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_DY_BOX_2MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_DY-BOX-MU6MU4")
            {
                L1_DY_BOX_MU6MU4 = cg->isPassed();
                PS_L1_DY_BOX_MU6MU4 = cg->getPrescale();
                TAV_L1_DY_BOX_MU6MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_DY_BOX_MU6MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_DY_BOX_MU6MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_DY-BOX-2MU6")
            {
                L1_DY_BOX_2MU6 = cg->isPassed();
                PS_L1_DY_BOX_2MU6 = cg->getPrescale();
                TAV_L1_DY_BOX_2MU6 = cg->isPassedBits() & TAVmask;
                TBP_L1_DY_BOX_2MU6 = cg->isPassedBits() & TBPmask;
                TAP_L1_DY_BOX_2MU6 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LFV-MU")
            {
                L1_LFV_MU = cg->isPassed();
                PS_L1_LFV_MU = cg->getPrescale();
                TAV_L1_LFV_MU = cg->isPassedBits() & TAVmask;
                TBP_L1_LFV_MU = cg->isPassedBits() & TBPmask;
                TAP_L1_LFV_MU = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LFV-EM8I")
            {
                L1_LFV_EM8I = cg->isPassed();
                PS_L1_LFV_EM8I = cg->getPrescale();
                TAV_L1_LFV_EM8I = cg->isPassedBits() & TAVmask;
                TBP_L1_LFV_EM8I = cg->isPassedBits() & TBPmask;
                TAP_L1_LFV_EM8I = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LFV-EM15I")
            {
                L1_LFV_EM15I = cg->isPassed();
                PS_L1_LFV_EM15I = cg->getPrescale();
                TAV_L1_LFV_EM15I = cg->isPassedBits() & TAVmask;
                TBP_L1_LFV_EM15I = cg->isPassedBits() & TBPmask;
                TAP_L1_LFV_EM15I = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_DPHI-Js2XE50")
            {
                L1_DPHI_Js2XE50 = cg->isPassed();
                PS_L1_DPHI_Js2XE50 = cg->getPrescale();
                TAV_L1_DPHI_Js2XE50 = cg->isPassedBits() & TAVmask;
                TBP_L1_DPHI_Js2XE50 = cg->isPassedBits() & TBPmask;
                TAP_L1_DPHI_Js2XE50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_DPHI-J20s2XE50")
            {
                L1_DPHI_J20s2XE50 = cg->isPassed();
                PS_L1_DPHI_J20s2XE50 = cg->getPrescale();
                TAV_L1_DPHI_J20s2XE50 = cg->isPassedBits() & TAVmask;
                TBP_L1_DPHI_J20s2XE50 = cg->isPassedBits() & TBPmask;
                TAP_L1_DPHI_J20s2XE50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_DPHI-J20XE50")
            {
                L1_DPHI_J20XE50 = cg->isPassed();
                PS_L1_DPHI_J20XE50 = cg->getPrescale();
                TAV_L1_DPHI_J20XE50 = cg->isPassedBits() & TAVmask;
                TBP_L1_DPHI_J20XE50 = cg->isPassedBits() & TBPmask;
                TAP_L1_DPHI_J20XE50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_DPHI-CJ20XE50")
            {
                L1_DPHI_CJ20XE50 = cg->isPassed();
                PS_L1_DPHI_CJ20XE50 = cg->getPrescale();
                TAV_L1_DPHI_CJ20XE50 = cg->isPassedBits() & TAVmask;
                TBP_L1_DPHI_CJ20XE50 = cg->isPassedBits() & TBPmask;
                TAP_L1_DPHI_CJ20XE50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MJJ-900")
            {
                L1_MJJ_900 = cg->isPassed();
                PS_L1_MJJ_900 = cg->getPrescale();
                TAV_L1_MJJ_900 = cg->isPassedBits() & TAVmask;
                TBP_L1_MJJ_900 = cg->isPassedBits() & TBPmask;
                TAP_L1_MJJ_900 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MJJ-800")
            {
                L1_MJJ_800 = cg->isPassed();
                PS_L1_MJJ_800 = cg->getPrescale();
                TAV_L1_MJJ_800 = cg->isPassedBits() & TAVmask;
                TBP_L1_MJJ_800 = cg->isPassedBits() & TBPmask;
                TAP_L1_MJJ_800 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MJJ-700")
            {
                L1_MJJ_700 = cg->isPassed();
                PS_L1_MJJ_700 = cg->getPrescale();
                TAV_L1_MJJ_700 = cg->isPassedBits() & TAVmask;
                TBP_L1_MJJ_700 = cg->isPassedBits() & TBPmask;
                TAP_L1_MJJ_700 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MJJ-400")
            {
                L1_MJJ_400 = cg->isPassed();
                PS_L1_MJJ_400 = cg->getPrescale();
                TAV_L1_MJJ_400 = cg->isPassedBits() & TAVmask;
                TBP_L1_MJJ_400 = cg->isPassedBits() & TBPmask;
                TAP_L1_MJJ_400 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MJJ-100")
            {
                L1_MJJ_100 = cg->isPassed();
                PS_L1_MJJ_100 = cg->getPrescale();
                TAV_L1_MJJ_100 = cg->isPassedBits() & TAVmask;
                TBP_L1_MJJ_100 = cg->isPassedBits() & TBPmask;
                TAP_L1_MJJ_100 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_HT150-JJ15.ETA49")
            {
                L1_HT150_JJ15_ETA49 = cg->isPassed();
                PS_L1_HT150_JJ15_ETA49 = cg->getPrescale();
                TAV_L1_HT150_JJ15_ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_HT150_JJ15_ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_HT150_JJ15_ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_DETA-JJ")
            {
                L1_DETA_JJ = cg->isPassed();
                PS_L1_DETA_JJ = cg->getPrescale();
                TAV_L1_DETA_JJ = cg->isPassedBits() & TAVmask;
                TBP_L1_DETA_JJ = cg->isPassedBits() & TBPmask;
                TAP_L1_DETA_JJ = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J4-MATCH")
            {
                L1_J4_MATCH = cg->isPassed();
                PS_L1_J4_MATCH = cg->getPrescale();
                TAV_L1_J4_MATCH = cg->isPassedBits() & TAVmask;
                TBP_L1_J4_MATCH = cg->isPassedBits() & TBPmask;
                TAP_L1_J4_MATCH = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LLP-NOMATCH")
            {
                L1_LLP_NOMATCH = cg->isPassed();
                PS_L1_LLP_NOMATCH = cg->getPrescale();
                TAV_L1_LLP_NOMATCH = cg->isPassedBits() & TAVmask;
                TBP_L1_LLP_NOMATCH = cg->isPassedBits() & TBPmask;
                TAP_L1_LLP_NOMATCH = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_DR-MU10TAU12I")
            {
                L1_DR_MU10TAU12I = cg->isPassed();
                PS_L1_DR_MU10TAU12I = cg->getPrescale();
                TAV_L1_DR_MU10TAU12I = cg->isPassedBits() & TAVmask;
                TBP_L1_DR_MU10TAU12I = cg->isPassedBits() & TBPmask;
                TAP_L1_DR_MU10TAU12I = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU30_EMPTY")
            {
                L1_TAU30_EMPTY = cg->isPassed();
                PS_L1_TAU30_EMPTY = cg->getPrescale();
                TAV_L1_TAU30_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU30_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU30_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15-TAU40")
            {
                L1_EM15_TAU40 = cg->isPassed();
                PS_L1_EM15_TAU40 = cg->getPrescale();
                TAV_L1_EM15_TAU40 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15_TAU40 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15_TAU40 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU30_UNPAIRED_ISO")
            {
                L1_TAU30_UNPAIRED_ISO = cg->isPassed();
                PS_L1_TAU30_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_TAU30_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU30_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU30_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15-TAU12I")
            {
                L1_EM15_TAU12I = cg->isPassed();
                PS_L1_EM15_TAU12I = cg->getPrescale();
                TAV_L1_EM15_TAU12I = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15_TAU12I = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15_TAU12I = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15TAU12I-J25")
            {
                L1_EM15TAU12I_J25 = cg->isPassed();
                PS_L1_EM15TAU12I_J25 = cg->getPrescale();
                TAV_L1_EM15TAU12I_J25 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15TAU12I_J25 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15TAU12I_J25 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_DR-EM15TAU12I-J25")
            {
                L1_DR_EM15TAU12I_J25 = cg->isPassed();
                PS_L1_DR_EM15TAU12I_J25 = cg->getPrescale();
                TAV_L1_DR_EM15TAU12I_J25 = cg->isPassedBits() & TAVmask;
                TBP_L1_DR_EM15TAU12I_J25 = cg->isPassedBits() & TBPmask;
                TAP_L1_DR_EM15TAU12I_J25 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU20ITAU12I-J25")
            {
                L1_TAU20ITAU12I_J25 = cg->isPassed();
                PS_L1_TAU20ITAU12I_J25 = cg->getPrescale();
                TAV_L1_TAU20ITAU12I_J25 = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU20ITAU12I_J25 = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU20ITAU12I_J25 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_DR-TAU20ITAU12I")
            {
                L1_DR_TAU20ITAU12I = cg->isPassed();
                PS_L1_DR_TAU20ITAU12I = cg->getPrescale();
                TAV_L1_DR_TAU20ITAU12I = cg->isPassedBits() & TAVmask;
                TBP_L1_DR_TAU20ITAU12I = cg->isPassedBits() & TBPmask;
                TAP_L1_DR_TAU20ITAU12I = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BOX-TAU20ITAU12I")
            {
                L1_BOX_TAU20ITAU12I = cg->isPassed();
                PS_L1_BOX_TAU20ITAU12I = cg->getPrescale();
                TAV_L1_BOX_TAU20ITAU12I = cg->isPassedBits() & TAVmask;
                TBP_L1_BOX_TAU20ITAU12I = cg->isPassedBits() & TBPmask;
                TAP_L1_BOX_TAU20ITAU12I = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_DR-TAU20ITAU12I-J25")
            {
                L1_DR_TAU20ITAU12I_J25 = cg->isPassed();
                PS_L1_DR_TAU20ITAU12I_J25 = cg->getPrescale();
                TAV_L1_DR_TAU20ITAU12I_J25 = cg->isPassedBits() & TAVmask;
                TBP_L1_DR_TAU20ITAU12I_J25 = cg->isPassedBits() & TBPmask;
                TAP_L1_DR_TAU20ITAU12I_J25 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LAR-EM")
            {
                L1_LAR_EM = cg->isPassed();
                PS_L1_LAR_EM = cg->getPrescale();
                TAV_L1_LAR_EM = cg->isPassedBits() & TAVmask;
                TBP_L1_LAR_EM = cg->isPassedBits() & TBPmask;
                TAP_L1_LAR_EM = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LAR-J")
            {
                L1_LAR_J = cg->isPassed();
                PS_L1_LAR_J = cg->getPrescale();
                TAV_L1_LAR_J = cg->isPassedBits() & TAVmask;
                TBP_L1_LAR_J = cg->isPassedBits() & TBPmask;
                TAP_L1_LAR_J = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU6_MJJ-200")
            {
                L1_MU6_MJJ_200 = cg->isPassed();
                PS_L1_MU6_MJJ_200 = cg->getPrescale();
                TAV_L1_MU6_MJJ_200 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU6_MJJ_200 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU6_MJJ_200 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU6_MJJ-300")
            {
                L1_MU6_MJJ_300 = cg->isPassed();
                PS_L1_MU6_MJJ_300 = cg->getPrescale();
                TAV_L1_MU6_MJJ_300 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU6_MJJ_300 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU6_MJJ_300 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU6_MJJ-400")
            {
                L1_MU6_MJJ_400 = cg->isPassed();
                PS_L1_MU6_MJJ_400 = cg->getPrescale();
                TAV_L1_MU6_MJJ_400 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU6_MJJ_400 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU6_MJJ_400 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU6_MJJ-500")
            {
                L1_MU6_MJJ_500 = cg->isPassed();
                PS_L1_MU6_MJJ_500 = cg->getPrescale();
                TAV_L1_MU6_MJJ_500 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU6_MJJ_500 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU6_MJJ_500 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J30_2J20_4J20.0ETA49_MJJ-400")
            {
                L1_J30_2J20_4J20_0ETA49_MJJ_400 = cg->isPassed();
                PS_L1_J30_2J20_4J20_0ETA49_MJJ_400 = cg->getPrescale();
                TAV_L1_J30_2J20_4J20_0ETA49_MJJ_400 = cg->isPassedBits() & TAVmask;
                TBP_L1_J30_2J20_4J20_0ETA49_MJJ_400 = cg->isPassedBits() & TBPmask;
                TAP_L1_J30_2J20_4J20_0ETA49_MJJ_400 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J30_2J20_4J20.0ETA49_MJJ-700")
            {
                L1_J30_2J20_4J20_0ETA49_MJJ_700 = cg->isPassed();
                PS_L1_J30_2J20_4J20_0ETA49_MJJ_700 = cg->getPrescale();
                TAV_L1_J30_2J20_4J20_0ETA49_MJJ_700 = cg->isPassedBits() & TAVmask;
                TBP_L1_J30_2J20_4J20_0ETA49_MJJ_700 = cg->isPassedBits() & TBPmask;
                TAP_L1_J30_2J20_4J20_0ETA49_MJJ_700 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J30_2J20_4J20.0ETA49_MJJ-800")
            {
                L1_J30_2J20_4J20_0ETA49_MJJ_800 = cg->isPassed();
                PS_L1_J30_2J20_4J20_0ETA49_MJJ_800 = cg->getPrescale();
                TAV_L1_J30_2J20_4J20_0ETA49_MJJ_800 = cg->isPassedBits() & TAVmask;
                TBP_L1_J30_2J20_4J20_0ETA49_MJJ_800 = cg->isPassedBits() & TBPmask;
                TAP_L1_J30_2J20_4J20_0ETA49_MJJ_800 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J30_2J20_4J20.0ETA49_MJJ-900")
            {
                L1_J30_2J20_4J20_0ETA49_MJJ_900 = cg->isPassed();
                PS_L1_J30_2J20_4J20_0ETA49_MJJ_900 = cg->getPrescale();
                TAV_L1_J30_2J20_4J20_0ETA49_MJJ_900 = cg->isPassedBits() & TAVmask;
                TBP_L1_J30_2J20_4J20_0ETA49_MJJ_900 = cg->isPassedBits() & TBPmask;
                TAP_L1_J30_2J20_4J20_0ETA49_MJJ_900 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3J20_4J20.0ETA49_MJJ-400")
            {
                L1_3J20_4J20_0ETA49_MJJ_400 = cg->isPassed();
                PS_L1_3J20_4J20_0ETA49_MJJ_400 = cg->getPrescale();
                TAV_L1_3J20_4J20_0ETA49_MJJ_400 = cg->isPassedBits() & TAVmask;
                TBP_L1_3J20_4J20_0ETA49_MJJ_400 = cg->isPassedBits() & TBPmask;
                TAP_L1_3J20_4J20_0ETA49_MJJ_400 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3J20_4J20.0ETA49_MJJ-700")
            {
                L1_3J20_4J20_0ETA49_MJJ_700 = cg->isPassed();
                PS_L1_3J20_4J20_0ETA49_MJJ_700 = cg->getPrescale();
                TAV_L1_3J20_4J20_0ETA49_MJJ_700 = cg->isPassedBits() & TAVmask;
                TBP_L1_3J20_4J20_0ETA49_MJJ_700 = cg->isPassedBits() & TBPmask;
                TAP_L1_3J20_4J20_0ETA49_MJJ_700 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3J20_4J20.0ETA49_MJJ-800")
            {
                L1_3J20_4J20_0ETA49_MJJ_800 = cg->isPassed();
                PS_L1_3J20_4J20_0ETA49_MJJ_800 = cg->getPrescale();
                TAV_L1_3J20_4J20_0ETA49_MJJ_800 = cg->isPassedBits() & TAVmask;
                TBP_L1_3J20_4J20_0ETA49_MJJ_800 = cg->isPassedBits() & TBPmask;
                TAP_L1_3J20_4J20_0ETA49_MJJ_800 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3J20_4J20.0ETA49_MJJ-900")
            {
                L1_3J20_4J20_0ETA49_MJJ_900 = cg->isPassed();
                PS_L1_3J20_4J20_0ETA49_MJJ_900 = cg->getPrescale();
                TAV_L1_3J20_4J20_0ETA49_MJJ_900 = cg->isPassedBits() & TAVmask;
                TBP_L1_3J20_4J20_0ETA49_MJJ_900 = cg->isPassedBits() & TBPmask;
                TAP_L1_3J20_4J20_0ETA49_MJJ_900 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XE35_MJJ-200")
            {
                L1_XE35_MJJ_200 = cg->isPassed();
                PS_L1_XE35_MJJ_200 = cg->getPrescale();
                TAV_L1_XE35_MJJ_200 = cg->isPassedBits() & TAVmask;
                TBP_L1_XE35_MJJ_200 = cg->isPassedBits() & TBPmask;
                TAP_L1_XE35_MJJ_200 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM7_FIRSTEMPTY")
            {
                L1_EM7_FIRSTEMPTY = cg->isPassed();
                PS_L1_EM7_FIRSTEMPTY = cg->getPrescale();
                TAV_L1_EM7_FIRSTEMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_EM7_FIRSTEMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_EM7_FIRSTEMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD0_ABORTGAPNOTCALIB")
            {
                L1_RD0_ABORTGAPNOTCALIB = cg->isPassed();
                PS_L1_RD0_ABORTGAPNOTCALIB = cg->getPrescale();
                TAV_L1_RD0_ABORTGAPNOTCALIB = cg->isPassedBits() & TAVmask;
                TBP_L1_RD0_ABORTGAPNOTCALIB = cg->isPassedBits() & TBPmask;
                TAP_L1_RD0_ABORTGAPNOTCALIB = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_3J25.0ETA23")
            {
                L1_3J25_0ETA23 = cg->isPassed();
                PS_L1_3J25_0ETA23 = cg->getPrescale();
                TAV_L1_3J25_0ETA23 = cg->isPassedBits() & TAVmask;
                TBP_L1_3J25_0ETA23 = cg->isPassedBits() & TBPmask;
                TAP_L1_3J25_0ETA23 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE20")
            {
                L1_TE20 = cg->isPassed();
                PS_L1_TE20 = cg->getPrescale();
                TAV_L1_TE20 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE20 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE20 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE10.0ETA24")
            {
                L1_TE10_0ETA24 = cg->isPassed();
                PS_L1_TE10_0ETA24 = cg->getPrescale();
                TAV_L1_TE10_0ETA24 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE10_0ETA24 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE10_0ETA24 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE20.0ETA24")
            {
                L1_TE20_0ETA24 = cg->isPassed();
                PS_L1_TE20_0ETA24 = cg->getPrescale();
                TAV_L1_TE20_0ETA24 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE20_0ETA24 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE20_0ETA24 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XS40")
            {
                L1_XS40 = cg->isPassed();
                PS_L1_XS40 = cg->getPrescale();
                TAV_L1_XS40 = cg->isPassedBits() & TAVmask;
                TBP_L1_XS40 = cg->isPassedBits() & TBPmask;
                TAP_L1_XS40 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XS50")
            {
                L1_XS50 = cg->isPassed();
                PS_L1_XS50 = cg->getPrescale();
                TAV_L1_XS50 = cg->isPassedBits() & TAVmask;
                TBP_L1_XS50 = cg->isPassedBits() & TBPmask;
                TAP_L1_XS50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_XS60")
            {
                L1_XS60 = cg->isPassed();
                PS_L1_XS60 = cg->getPrescale();
                TAV_L1_XS60 = cg->isPassedBits() & TAVmask;
                TBP_L1_XS60 = cg->isPassedBits() & TBPmask;
                TAP_L1_XS60 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J12_BGRP12")
            {
                L1_J12_BGRP12 = cg->isPassed();
                PS_L1_J12_BGRP12 = cg->getPrescale();
                TAV_L1_J12_BGRP12 = cg->isPassedBits() & TAVmask;
                TBP_L1_J12_BGRP12 = cg->isPassedBits() & TBPmask;
                TAP_L1_J12_BGRP12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J30.31ETA49_BGRP12")
            {
                L1_J30_31ETA49_BGRP12 = cg->isPassed();
                PS_L1_J30_31ETA49_BGRP12 = cg->getPrescale();
                TAV_L1_J30_31ETA49_BGRP12 = cg->isPassedBits() & TAVmask;
                TBP_L1_J30_31ETA49_BGRP12 = cg->isPassedBits() & TBPmask;
                TAP_L1_J30_31ETA49_BGRP12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU6_J30.0ETA49_2J20.0ETA49")
            {
                L1_MU6_J30_0ETA49_2J20_0ETA49 = cg->isPassed();
                PS_L1_MU6_J30_0ETA49_2J20_0ETA49 = cg->getPrescale();
                TAV_L1_MU6_J30_0ETA49_2J20_0ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU6_J30_0ETA49_2J20_0ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU6_J30_0ETA49_2J20_0ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_4J20.0ETA49")
            {
                L1_4J20_0ETA49 = cg->isPassed();
                PS_L1_4J20_0ETA49 = cg->getPrescale();
                TAV_L1_4J20_0ETA49 = cg->isPassedBits() & TAVmask;
                TBP_L1_4J20_0ETA49 = cg->isPassedBits() & TBPmask;
                TAP_L1_4J20_0ETA49 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU8_UNPAIRED_ISO")
            {
                L1_TAU8_UNPAIRED_ISO = cg->isPassed();
                PS_L1_TAU8_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_TAU8_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU8_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU8_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM7_UNPAIRED_ISO")
            {
                L1_EM7_UNPAIRED_ISO = cg->isPassed();
                PS_L1_EM7_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_EM7_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_EM7_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_EM7_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_RD2_BGRP12")
            {
                L1_RD2_BGRP12 = cg->isPassed();
                PS_L1_RD2_BGRP12 = cg->getPrescale();
                TAV_L1_RD2_BGRP12 = cg->isPassedBits() & TAVmask;
                TBP_L1_RD2_BGRP12 = cg->isPassedBits() & TBPmask;
                TAP_L1_RD2_BGRP12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU8_FIRSTEMPTY")
            {
                L1_TAU8_FIRSTEMPTY = cg->isPassed();
                PS_L1_TAU8_FIRSTEMPTY = cg->getPrescale();
                TAV_L1_TAU8_FIRSTEMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU8_FIRSTEMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU8_FIRSTEMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM24VHI")
            {
                L1_EM24VHI = cg->isPassed();
                PS_L1_EM24VHI = cg->getPrescale();
                TAV_L1_EM24VHI = cg->isPassedBits() & TAVmask;
                TBP_L1_EM24VHI = cg->isPassedBits() & TBPmask;
                TAP_L1_EM24VHI = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LHCF_UNPAIRED_ISO")
            {
                L1_LHCF_UNPAIRED_ISO = cg->isPassed();
                PS_L1_LHCF_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_LHCF_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_LHCF_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_LHCF_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LHCF_EMPTY")
            {
                L1_LHCF_EMPTY = cg->isPassed();
                PS_L1_LHCF_EMPTY = cg->getPrescale();
                TAV_L1_LHCF_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_LHCF_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_LHCF_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15VH_2EM10VH_3EM7")
            {
                L1_EM15VH_2EM10VH_3EM7 = cg->isPassed();
                PS_L1_EM15VH_2EM10VH_3EM7 = cg->getPrescale();
                TAV_L1_EM15VH_2EM10VH_3EM7 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15VH_2EM10VH_3EM7 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15VH_2EM10VH_3EM7 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15VH_3EM8VH")
            {
                L1_EM15VH_3EM8VH = cg->isPassed();
                PS_L1_EM15VH_3EM8VH = cg->getPrescale();
                TAV_L1_EM15VH_3EM8VH = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15VH_3EM8VH = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15VH_3EM8VH = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM8I_MU10")
            {
                L1_EM8I_MU10 = cg->isPassed();
                PS_L1_EM8I_MU10 = cg->getPrescale();
                TAV_L1_EM8I_MU10 = cg->isPassedBits() & TAVmask;
                TBP_L1_EM8I_MU10 = cg->isPassedBits() & TBPmask;
                TAP_L1_EM8I_MU10 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM15HI")
            {
                L1_EM15HI = cg->isPassed();
                PS_L1_EM15HI = cg->getPrescale();
                TAV_L1_EM15HI = cg->isPassedBits() & TAVmask;
                TBP_L1_EM15HI = cg->isPassedBits() & TBPmask;
                TAP_L1_EM15HI = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU6_3MU4")
            {
                L1_MU6_3MU4 = cg->isPassed();
                PS_L1_MU6_3MU4 = cg->getPrescale();
                TAV_L1_MU6_3MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_MU6_3MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_MU6_3MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_2MU6_3MU4")
            {
                L1_2MU6_3MU4 = cg->isPassed();
                PS_L1_2MU6_3MU4 = cg->getPrescale();
                TAV_L1_2MU6_3MU4 = cg->isPassedBits() & TAVmask;
                TBP_L1_2MU6_3MU4 = cg->isPassedBits() & TBPmask;
                TAP_L1_2MU6_3MU4 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_BGRP9")
            {
                L1_BGRP9 = cg->isPassed();
                PS_L1_BGRP9 = cg->getPrescale();
                TAV_L1_BGRP9 = cg->isPassedBits() & TAVmask;
                TBP_L1_BGRP9 = cg->isPassedBits() & TBPmask;
                TAP_L1_BGRP9 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE50")
            {
                L1_TE50 = cg->isPassed();
                PS_L1_TE50 = cg->getPrescale();
                TAV_L1_TE50 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE50 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE50 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE5")
            {
                L1_TE5 = cg->isPassed();
                PS_L1_TE5 = cg->getPrescale();
                TAV_L1_TE5 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE5 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE5 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE60")
            {
                L1_TE60 = cg->isPassed();
                PS_L1_TE60 = cg->getPrescale();
                TAV_L1_TE60 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE60 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE60 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE50.0ETA24")
            {
                L1_TE50_0ETA24 = cg->isPassed();
                PS_L1_TE50_0ETA24 = cg->getPrescale();
                TAV_L1_TE50_0ETA24 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE50_0ETA24 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE50_0ETA24 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE5.0ETA24")
            {
                L1_TE5_0ETA24 = cg->isPassed();
                PS_L1_TE5_0ETA24 = cg->getPrescale();
                TAV_L1_TE5_0ETA24 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE5_0ETA24 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE5_0ETA24 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM20VH_FIRSTEMPTY")
            {
                L1_EM20VH_FIRSTEMPTY = cg->isPassed();
                PS_L1_EM20VH_FIRSTEMPTY = cg->getPrescale();
                TAV_L1_EM20VH_FIRSTEMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_EM20VH_FIRSTEMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_EM20VH_FIRSTEMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM22VHI_FIRSTEMPTY")
            {
                L1_EM22VHI_FIRSTEMPTY = cg->isPassed();
                PS_L1_EM22VHI_FIRSTEMPTY = cg->getPrescale();
                TAV_L1_EM22VHI_FIRSTEMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_EM22VHI_FIRSTEMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_EM22VHI_FIRSTEMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU20_FIRSTEMPTY")
            {
                L1_MU20_FIRSTEMPTY = cg->isPassed();
                PS_L1_MU20_FIRSTEMPTY = cg->getPrescale();
                TAV_L1_MU20_FIRSTEMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_MU20_FIRSTEMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_MU20_FIRSTEMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J100_FIRSTEMPTY")
            {
                L1_J100_FIRSTEMPTY = cg->isPassed();
                PS_L1_J100_FIRSTEMPTY = cg->getPrescale();
                TAV_L1_J100_FIRSTEMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_J100_FIRSTEMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_J100_FIRSTEMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J100.31ETA49_FIRSTEMPTY")
            {
                L1_J100_31ETA49_FIRSTEMPTY = cg->isPassed();
                PS_L1_J100_31ETA49_FIRSTEMPTY = cg->getPrescale();
                TAV_L1_J100_31ETA49_FIRSTEMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_J100_31ETA49_FIRSTEMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_J100_31ETA49_FIRSTEMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE60.0ETA24")
            {
                L1_TE60_0ETA24 = cg->isPassed();
                PS_L1_TE60_0ETA24 = cg->getPrescale();
                TAV_L1_TE60_0ETA24 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE60_0ETA24 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE60_0ETA24 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE70.0ETA24")
            {
                L1_TE70_0ETA24 = cg->isPassed();
                PS_L1_TE70_0ETA24 = cg->getPrescale();
                TAV_L1_TE70_0ETA24 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE70_0ETA24 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE70_0ETA24 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE40.0ETA24")
            {
                L1_TE40_0ETA24 = cg->isPassed();
                PS_L1_TE40_0ETA24 = cg->getPrescale();
                TAV_L1_TE40_0ETA24 = cg->isPassedBits() & TAVmask;
                TBP_L1_TE40_0ETA24 = cg->isPassedBits() & TBPmask;
                TAP_L1_TE40_0ETA24 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ZDC_A")
            {
                L1_ZDC_A = cg->isPassed();
                PS_L1_ZDC_A = cg->getPrescale();
                TAV_L1_ZDC_A = cg->isPassedBits() & TAVmask;
                TBP_L1_ZDC_A = cg->isPassedBits() & TBPmask;
                TAP_L1_ZDC_A = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ZDC_C")
            {
                L1_ZDC_C = cg->isPassed();
                PS_L1_ZDC_C = cg->getPrescale();
                TAV_L1_ZDC_C = cg->isPassedBits() & TAVmask;
                TBP_L1_ZDC_C = cg->isPassedBits() & TBPmask;
                TAP_L1_ZDC_C = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ZDC_AND")
            {
                L1_ZDC_AND = cg->isPassed();
                PS_L1_ZDC_AND = cg->getPrescale();
                TAV_L1_ZDC_AND = cg->isPassedBits() & TAVmask;
                TBP_L1_ZDC_AND = cg->isPassedBits() & TBPmask;
                TAP_L1_ZDC_AND = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ZDC_A_C")
            {
                L1_ZDC_A_C = cg->isPassed();
                PS_L1_ZDC_A_C = cg->getPrescale();
                TAV_L1_ZDC_A_C = cg->isPassedBits() & TAVmask;
                TBP_L1_ZDC_A_C = cg->isPassedBits() & TBPmask;
                TAP_L1_ZDC_A_C = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ELAST1")
            {
                L1_ALFA_ELAST1 = cg->isPassed();
                PS_L1_ALFA_ELAST1 = cg->getPrescale();
                TAV_L1_ALFA_ELAST1 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ELAST1 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ELAST1 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ELAST2")
            {
                L1_ALFA_ELAST2 = cg->isPassed();
                PS_L1_ALFA_ELAST2 = cg->getPrescale();
                TAV_L1_ALFA_ELAST2 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ELAST2 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ELAST2 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ELAST11")
            {
                L1_ALFA_ELAST11 = cg->isPassed();
                PS_L1_ALFA_ELAST11 = cg->getPrescale();
                TAV_L1_ALFA_ELAST11 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ELAST11 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ELAST11 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ELAST12")
            {
                L1_ALFA_ELAST12 = cg->isPassed();
                PS_L1_ALFA_ELAST12 = cg->getPrescale();
                TAV_L1_ALFA_ELAST12 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ELAST12 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ELAST12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ELAST13")
            {
                L1_ALFA_ELAST13 = cg->isPassed();
                PS_L1_ALFA_ELAST13 = cg->getPrescale();
                TAV_L1_ALFA_ELAST13 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ELAST13 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ELAST13 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ELAST14")
            {
                L1_ALFA_ELAST14 = cg->isPassed();
                PS_L1_ALFA_ELAST14 = cg->getPrescale();
                TAV_L1_ALFA_ELAST14 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ELAST14 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ELAST14 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ELAST15")
            {
                L1_ALFA_ELAST15 = cg->isPassed();
                PS_L1_ALFA_ELAST15 = cg->getPrescale();
                TAV_L1_ALFA_ELAST15 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ELAST15 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ELAST15 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ELAST15_Calib")
            {
                L1_ALFA_ELAST15_Calib = cg->isPassed();
                PS_L1_ALFA_ELAST15_Calib = cg->getPrescale();
                TAV_L1_ALFA_ELAST15_Calib = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ELAST15_Calib = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ELAST15_Calib = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ELAST16")
            {
                L1_ALFA_ELAST16 = cg->isPassed();
                PS_L1_ALFA_ELAST16 = cg->getPrescale();
                TAV_L1_ALFA_ELAST16 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ELAST16 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ELAST16 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ELAST17")
            {
                L1_ALFA_ELAST17 = cg->isPassed();
                PS_L1_ALFA_ELAST17 = cg->getPrescale();
                TAV_L1_ALFA_ELAST17 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ELAST17 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ELAST17 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ELAST18")
            {
                L1_ALFA_ELAST18 = cg->isPassed();
                PS_L1_ALFA_ELAST18 = cg->getPrescale();
                TAV_L1_ALFA_ELAST18 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ELAST18 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ELAST18 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ELAST18_Calib")
            {
                L1_ALFA_ELAST18_Calib = cg->isPassed();
                PS_L1_ALFA_ELAST18_Calib = cg->getPrescale();
                TAV_L1_ALFA_ELAST18_Calib = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ELAST18_Calib = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ELAST18_Calib = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_SDIFF5")
            {
                L1_ALFA_SDIFF5 = cg->isPassed();
                PS_L1_ALFA_SDIFF5 = cg->getPrescale();
                TAV_L1_ALFA_SDIFF5 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_SDIFF5 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_SDIFF5 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_SDIFF6")
            {
                L1_ALFA_SDIFF6 = cg->isPassed();
                PS_L1_ALFA_SDIFF6 = cg->getPrescale();
                TAV_L1_ALFA_SDIFF6 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_SDIFF6 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_SDIFF6 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_SDIFF7")
            {
                L1_ALFA_SDIFF7 = cg->isPassed();
                PS_L1_ALFA_SDIFF7 = cg->getPrescale();
                TAV_L1_ALFA_SDIFF7 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_SDIFF7 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_SDIFF7 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_SDIFF8")
            {
                L1_ALFA_SDIFF8 = cg->isPassed();
                PS_L1_ALFA_SDIFF8 = cg->getPrescale();
                TAV_L1_ALFA_SDIFF8 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_SDIFF8 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_SDIFF8 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_1_A_ALFA_C")
            {
                L1_MBTS_1_A_ALFA_C = cg->isPassed();
                PS_L1_MBTS_1_A_ALFA_C = cg->getPrescale();
                TAV_L1_MBTS_1_A_ALFA_C = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_1_A_ALFA_C = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_1_A_ALFA_C = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_1_C_ALFA_A")
            {
                L1_MBTS_1_C_ALFA_A = cg->isPassed();
                PS_L1_MBTS_1_C_ALFA_A = cg->getPrescale();
                TAV_L1_MBTS_1_C_ALFA_A = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_1_C_ALFA_A = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_1_C_ALFA_A = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO")
            {
                L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO = cg->isPassed();
                PS_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO")
            {
                L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO = cg->isPassed();
                PS_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_2_A_ALFA_C")
            {
                L1_MBTS_2_A_ALFA_C = cg->isPassed();
                PS_L1_MBTS_2_A_ALFA_C = cg->getPrescale();
                TAV_L1_MBTS_2_A_ALFA_C = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_2_A_ALFA_C = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_2_A_ALFA_C = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_2_C_ALFA_A")
            {
                L1_MBTS_2_C_ALFA_A = cg->isPassed();
                PS_L1_MBTS_2_C_ALFA_A = cg->getPrescale();
                TAV_L1_MBTS_2_C_ALFA_A = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_2_C_ALFA_A = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_2_C_ALFA_A = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO")
            {
                L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO = cg->isPassed();
                PS_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO")
            {
                L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO = cg->isPassed();
                PS_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LUCID_A_ALFA_C")
            {
                L1_LUCID_A_ALFA_C = cg->isPassed();
                PS_L1_LUCID_A_ALFA_C = cg->getPrescale();
                TAV_L1_LUCID_A_ALFA_C = cg->isPassedBits() & TAVmask;
                TBP_L1_LUCID_A_ALFA_C = cg->isPassedBits() & TBPmask;
                TAP_L1_LUCID_A_ALFA_C = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LUCID_C_ALFA_A")
            {
                L1_LUCID_C_ALFA_A = cg->isPassed();
                PS_L1_LUCID_C_ALFA_A = cg->getPrescale();
                TAV_L1_LUCID_C_ALFA_A = cg->isPassedBits() & TAVmask;
                TBP_L1_LUCID_C_ALFA_A = cg->isPassedBits() & TBPmask;
                TAP_L1_LUCID_C_ALFA_A = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LUCID_A_ALFA_C_UNPAIRED_ISO")
            {
                L1_LUCID_A_ALFA_C_UNPAIRED_ISO = cg->isPassed();
                PS_L1_LUCID_A_ALFA_C_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_LUCID_A_ALFA_C_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_LUCID_A_ALFA_C_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_LUCID_A_ALFA_C_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_LUCID_C_ALFA_A_UNPAIRED_ISO")
            {
                L1_LUCID_C_ALFA_A_UNPAIRED_ISO = cg->isPassed();
                PS_L1_LUCID_C_ALFA_A_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_LUCID_C_ALFA_A_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_LUCID_C_ALFA_A_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_LUCID_C_ALFA_A_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM3_ALFA_ANY")
            {
                L1_EM3_ALFA_ANY = cg->isPassed();
                PS_L1_EM3_ALFA_ANY = cg->getPrescale();
                TAV_L1_EM3_ALFA_ANY = cg->isPassedBits() & TAVmask;
                TBP_L1_EM3_ALFA_ANY = cg->isPassedBits() & TBPmask;
                TAP_L1_EM3_ALFA_ANY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM3_ALFA_ANY_UNPAIRED_ISO")
            {
                L1_EM3_ALFA_ANY_UNPAIRED_ISO = cg->isPassed();
                PS_L1_EM3_ALFA_ANY_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_EM3_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_EM3_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_EM3_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM3_ALFA_EINE")
            {
                L1_EM3_ALFA_EINE = cg->isPassed();
                PS_L1_EM3_ALFA_EINE = cg->getPrescale();
                TAV_L1_EM3_ALFA_EINE = cg->isPassedBits() & TAVmask;
                TBP_L1_EM3_ALFA_EINE = cg->isPassedBits() & TBPmask;
                TAP_L1_EM3_ALFA_EINE = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ELASTIC_UNPAIRED_ISO")
            {
                L1_ALFA_ELASTIC_UNPAIRED_ISO = cg->isPassed();
                PS_L1_ALFA_ELASTIC_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_ALFA_ELASTIC_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ELASTIC_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ELASTIC_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO")
            {
                L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO = cg->isPassed();
                PS_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ANY_A_EMPTY")
            {
                L1_ALFA_ANY_A_EMPTY = cg->isPassed();
                PS_L1_ALFA_ANY_A_EMPTY = cg->getPrescale();
                TAV_L1_ALFA_ANY_A_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ANY_A_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ANY_A_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ANY_C_EMPTY")
            {
                L1_ALFA_ANY_C_EMPTY = cg->isPassed();
                PS_L1_ALFA_ANY_C_EMPTY = cg->getPrescale();
                TAV_L1_ALFA_ANY_C_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ANY_C_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ANY_C_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J12_ALFA_ANY")
            {
                L1_J12_ALFA_ANY = cg->isPassed();
                PS_L1_J12_ALFA_ANY = cg->getPrescale();
                TAV_L1_J12_ALFA_ANY = cg->isPassedBits() & TAVmask;
                TBP_L1_J12_ALFA_ANY = cg->isPassedBits() & TBPmask;
                TAP_L1_J12_ALFA_ANY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_J12_ALFA_ANY_UNPAIRED_ISO")
            {
                L1_J12_ALFA_ANY_UNPAIRED_ISO = cg->isPassed();
                PS_L1_J12_ALFA_ANY_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_J12_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_J12_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_J12_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE5_ALFA_ANY")
            {
                L1_TE5_ALFA_ANY = cg->isPassed();
                PS_L1_TE5_ALFA_ANY = cg->getPrescale();
                TAV_L1_TE5_ALFA_ANY = cg->isPassedBits() & TAVmask;
                TBP_L1_TE5_ALFA_ANY = cg->isPassedBits() & TBPmask;
                TAP_L1_TE5_ALFA_ANY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE5_ALFA_ANY_UNPAIRED_ISO")
            {
                L1_TE5_ALFA_ANY_UNPAIRED_ISO = cg->isPassed();
                PS_L1_TE5_ALFA_ANY_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_TE5_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_TE5_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_TE5_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TE5_ALFA_EINE")
            {
                L1_TE5_ALFA_EINE = cg->isPassed();
                PS_L1_TE5_ALFA_EINE = cg->getPrescale();
                TAV_L1_TE5_ALFA_EINE = cg->isPassedBits() & TAVmask;
                TBP_L1_TE5_ALFA_EINE = cg->isPassedBits() & TBPmask;
                TAP_L1_TE5_ALFA_EINE = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TRT_ALFA_ANY")
            {
                L1_TRT_ALFA_ANY = cg->isPassed();
                PS_L1_TRT_ALFA_ANY = cg->getPrescale();
                TAV_L1_TRT_ALFA_ANY = cg->isPassedBits() & TAVmask;
                TBP_L1_TRT_ALFA_ANY = cg->isPassedBits() & TBPmask;
                TAP_L1_TRT_ALFA_ANY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TRT_ALFA_ANY_UNPAIRED_ISO")
            {
                L1_TRT_ALFA_ANY_UNPAIRED_ISO = cg->isPassed();
                PS_L1_TRT_ALFA_ANY_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_TRT_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_TRT_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_TRT_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TRT_ALFA_EINE")
            {
                L1_TRT_ALFA_EINE = cg->isPassed();
                PS_L1_TRT_ALFA_EINE = cg->getPrescale();
                TAV_L1_TRT_ALFA_EINE = cg->isPassedBits() & TAVmask;
                TBP_L1_TRT_ALFA_EINE = cg->isPassedBits() & TBPmask;
                TAP_L1_TRT_ALFA_EINE = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_TAU8_UNPAIRED_NONISO")
            {
                L1_TAU8_UNPAIRED_NONISO = cg->isPassed();
                PS_L1_TAU8_UNPAIRED_NONISO = cg->getPrescale();
                TAV_L1_TAU8_UNPAIRED_NONISO = cg->isPassedBits() & TAVmask;
                TBP_L1_TAU8_UNPAIRED_NONISO = cg->isPassedBits() & TBPmask;
                TAP_L1_TAU8_UNPAIRED_NONISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_EM7_UNPAIRED_NONISO")
            {
                L1_EM7_UNPAIRED_NONISO = cg->isPassed();
                PS_L1_EM7_UNPAIRED_NONISO = cg->getPrescale();
                TAV_L1_EM7_UNPAIRED_NONISO = cg->isPassedBits() & TAVmask;
                TBP_L1_EM7_UNPAIRED_NONISO = cg->isPassedBits() & TBPmask;
                TAP_L1_EM7_UNPAIRED_NONISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_MU4_UNPAIRED_NONISO")
            {
                L1_MU4_UNPAIRED_NONISO = cg->isPassed();
                PS_L1_MU4_UNPAIRED_NONISO = cg->getPrescale();
                TAV_L1_MU4_UNPAIRED_NONISO = cg->isPassedBits() & TAVmask;
                TBP_L1_MU4_UNPAIRED_NONISO = cg->isPassedBits() & TBPmask;
                TAP_L1_MU4_UNPAIRED_NONISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_BGT")
            {
                L1_ALFA_BGT = cg->isPassed();
                PS_L1_ALFA_BGT = cg->getPrescale();
                TAV_L1_ALFA_BGT = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_BGT = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_BGT = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_BGT_UNPAIRED_ISO")
            {
                L1_ALFA_BGT_UNPAIRED_ISO = cg->isPassed();
                PS_L1_ALFA_BGT_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_ALFA_BGT_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_BGT_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_BGT_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_BGT_BGRP10")
            {
                L1_ALFA_BGT_BGRP10 = cg->isPassed();
                PS_L1_ALFA_BGT_BGRP10 = cg->getPrescale();
                TAV_L1_ALFA_BGT_BGRP10 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_BGT_BGRP10 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_BGT_BGRP10 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_SHOWSYST5")
            {
                L1_ALFA_SHOWSYST5 = cg->isPassed();
                PS_L1_ALFA_SHOWSYST5 = cg->getPrescale();
                TAV_L1_ALFA_SHOWSYST5 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_SHOWSYST5 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_SHOWSYST5 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_SYST9")
            {
                L1_ALFA_SYST9 = cg->isPassed();
                PS_L1_ALFA_SYST9 = cg->getPrescale();
                TAV_L1_ALFA_SYST9 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_SYST9 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_SYST9 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_SYST10")
            {
                L1_ALFA_SYST10 = cg->isPassed();
                PS_L1_ALFA_SYST10 = cg->getPrescale();
                TAV_L1_ALFA_SYST10 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_SYST10 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_SYST10 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_SYST11")
            {
                L1_ALFA_SYST11 = cg->isPassed();
                PS_L1_ALFA_SYST11 = cg->getPrescale();
                TAV_L1_ALFA_SYST11 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_SYST11 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_SYST11 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_SYST12")
            {
                L1_ALFA_SYST12 = cg->isPassed();
                PS_L1_ALFA_SYST12 = cg->getPrescale();
                TAV_L1_ALFA_SYST12 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_SYST12 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_SYST12 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_SYST17")
            {
                L1_ALFA_SYST17 = cg->isPassed();
                PS_L1_ALFA_SYST17 = cg->getPrescale();
                TAV_L1_ALFA_SYST17 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_SYST17 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_SYST17 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_SYST18")
            {
                L1_ALFA_SYST18 = cg->isPassed();
                PS_L1_ALFA_SYST18 = cg->getPrescale();
                TAV_L1_ALFA_SYST18 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_SYST18 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_SYST18 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ANY")
            {
                L1_ALFA_ANY = cg->isPassed();
                PS_L1_ALFA_ANY = cg->getPrescale();
                TAV_L1_ALFA_ANY = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ANY = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ANY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ANY_EMPTY")
            {
                L1_ALFA_ANY_EMPTY = cg->isPassed();
                PS_L1_ALFA_ANY_EMPTY = cg->getPrescale();
                TAV_L1_ALFA_ANY_EMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ANY_EMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ANY_EMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ANY_FIRSTEMPTY")
            {
                L1_ALFA_ANY_FIRSTEMPTY = cg->isPassed();
                PS_L1_ALFA_ANY_FIRSTEMPTY = cg->getPrescale();
                TAV_L1_ALFA_ANY_FIRSTEMPTY = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ANY_FIRSTEMPTY = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ANY_FIRSTEMPTY = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ANY_UNPAIRED_ISO")
            {
                L1_ALFA_ANY_UNPAIRED_ISO = cg->isPassed();
                PS_L1_ALFA_ANY_UNPAIRED_ISO = cg->getPrescale();
                TAV_L1_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ANY_UNPAIRED_ISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ANY_UNPAIRED_NONISO")
            {
                L1_ALFA_ANY_UNPAIRED_NONISO = cg->isPassed();
                PS_L1_ALFA_ANY_UNPAIRED_NONISO = cg->getPrescale();
                TAV_L1_ALFA_ANY_UNPAIRED_NONISO = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ANY_UNPAIRED_NONISO = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ANY_UNPAIRED_NONISO = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ANY_BGRP10")
            {
                L1_ALFA_ANY_BGRP10 = cg->isPassed();
                PS_L1_ALFA_ANY_BGRP10 = cg->getPrescale();
                TAV_L1_ALFA_ANY_BGRP10 = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ANY_BGRP10 = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ANY_BGRP10 = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ANY_ABORTGAPNOTCALIB")
            {
                L1_ALFA_ANY_ABORTGAPNOTCALIB = cg->isPassed();
                PS_L1_ALFA_ANY_ABORTGAPNOTCALIB = cg->getPrescale();
                TAV_L1_ALFA_ANY_ABORTGAPNOTCALIB = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ANY_ABORTGAPNOTCALIB = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ANY_ABORTGAPNOTCALIB = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_ANY_CALIB")
            {
                L1_ALFA_ANY_CALIB = cg->isPassed();
                PS_L1_ALFA_ANY_CALIB = cg->getPrescale();
                TAV_L1_ALFA_ANY_CALIB = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_ANY_CALIB = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_ANY_CALIB = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_B7L1U")
            {
                L1_ALFA_B7L1U = cg->isPassed();
                PS_L1_ALFA_B7L1U = cg->getPrescale();
                TAV_L1_ALFA_B7L1U = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_B7L1U = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_B7L1U = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_B7L1L")
            {
                L1_ALFA_B7L1L = cg->isPassed();
                PS_L1_ALFA_B7L1L = cg->getPrescale();
                TAV_L1_ALFA_B7L1L = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_B7L1L = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_B7L1L = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_A7L1U")
            {
                L1_ALFA_A7L1U = cg->isPassed();
                PS_L1_ALFA_A7L1U = cg->getPrescale();
                TAV_L1_ALFA_A7L1U = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_A7L1U = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_A7L1U = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_A7L1L")
            {
                L1_ALFA_A7L1L = cg->isPassed();
                PS_L1_ALFA_A7L1L = cg->getPrescale();
                TAV_L1_ALFA_A7L1L = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_A7L1L = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_A7L1L = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_A7R1U")
            {
                L1_ALFA_A7R1U = cg->isPassed();
                PS_L1_ALFA_A7R1U = cg->getPrescale();
                TAV_L1_ALFA_A7R1U = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_A7R1U = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_A7R1U = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_A7R1L")
            {
                L1_ALFA_A7R1L = cg->isPassed();
                PS_L1_ALFA_A7R1L = cg->getPrescale();
                TAV_L1_ALFA_A7R1L = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_A7R1L = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_A7R1L = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_B7R1U")
            {
                L1_ALFA_B7R1U = cg->isPassed();
                PS_L1_ALFA_B7R1U = cg->getPrescale();
                TAV_L1_ALFA_B7R1U = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_B7R1U = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_B7R1U = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_B7R1L")
            {
                L1_ALFA_B7R1L = cg->isPassed();
                PS_L1_ALFA_B7R1L = cg->getPrescale();
                TAV_L1_ALFA_B7R1L = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_B7R1L = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_B7R1L = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_B7L1U_OD")
            {
                L1_ALFA_B7L1U_OD = cg->isPassed();
                PS_L1_ALFA_B7L1U_OD = cg->getPrescale();
                TAV_L1_ALFA_B7L1U_OD = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_B7L1U_OD = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_B7L1U_OD = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_B7L1L_OD")
            {
                L1_ALFA_B7L1L_OD = cg->isPassed();
                PS_L1_ALFA_B7L1L_OD = cg->getPrescale();
                TAV_L1_ALFA_B7L1L_OD = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_B7L1L_OD = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_B7L1L_OD = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_A7L1U_OD")
            {
                L1_ALFA_A7L1U_OD = cg->isPassed();
                PS_L1_ALFA_A7L1U_OD = cg->getPrescale();
                TAV_L1_ALFA_A7L1U_OD = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_A7L1U_OD = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_A7L1U_OD = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_A7L1L_OD")
            {
                L1_ALFA_A7L1L_OD = cg->isPassed();
                PS_L1_ALFA_A7L1L_OD = cg->getPrescale();
                TAV_L1_ALFA_A7L1L_OD = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_A7L1L_OD = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_A7L1L_OD = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_A7R1U_OD")
            {
                L1_ALFA_A7R1U_OD = cg->isPassed();
                PS_L1_ALFA_A7R1U_OD = cg->getPrescale();
                TAV_L1_ALFA_A7R1U_OD = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_A7R1U_OD = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_A7R1U_OD = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_A7R1L_OD")
            {
                L1_ALFA_A7R1L_OD = cg->isPassed();
                PS_L1_ALFA_A7R1L_OD = cg->getPrescale();
                TAV_L1_ALFA_A7R1L_OD = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_A7R1L_OD = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_A7R1L_OD = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_B7R1U_OD")
            {
                L1_ALFA_B7R1U_OD = cg->isPassed();
                PS_L1_ALFA_B7R1U_OD = cg->getPrescale();
                TAV_L1_ALFA_B7R1U_OD = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_B7R1U_OD = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_B7R1U_OD = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_B7R1L_OD")
            {
                L1_ALFA_B7R1L_OD = cg->isPassed();
                PS_L1_ALFA_B7R1L_OD = cg->getPrescale();
                TAV_L1_ALFA_B7R1L_OD = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_B7R1L_OD = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_B7R1L_OD = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_B7L1_OD")
            {
                L1_ALFA_B7L1_OD = cg->isPassed();
                PS_L1_ALFA_B7L1_OD = cg->getPrescale();
                TAV_L1_ALFA_B7L1_OD = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_B7L1_OD = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_B7L1_OD = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_A7L1_OD")
            {
                L1_ALFA_A7L1_OD = cg->isPassed();
                PS_L1_ALFA_A7L1_OD = cg->getPrescale();
                TAV_L1_ALFA_A7L1_OD = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_A7L1_OD = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_A7L1_OD = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_B7R1_OD")
            {
                L1_ALFA_B7R1_OD = cg->isPassed();
                PS_L1_ALFA_B7R1_OD = cg->getPrescale();
                TAV_L1_ALFA_B7R1_OD = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_B7R1_OD = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_B7R1_OD = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_ALFA_A7R1_OD")
            {
                L1_ALFA_A7R1_OD = cg->isPassed();
                PS_L1_ALFA_A7R1_OD = cg->getPrescale();
                TAV_L1_ALFA_A7R1_OD = cg->isPassedBits() & TAVmask;
                TBP_L1_ALFA_A7R1_OD = cg->isPassedBits() & TBPmask;
                TAP_L1_ALFA_A7R1_OD = cg->isPassedBits() & TAPmask;
            }
            if (thisTrig == "L1_CALREQ2")
            {
                L1_CALREQ2 = cg->isPassed();
                PS_L1_CALREQ2 = cg->getPrescale();
                TAV_L1_CALREQ2 = cg->isPassedBits() & TAVmask;
                TBP_L1_CALREQ2 = cg->isPassedBits() & TBPmask;
                TAP_L1_CALREQ2 = cg->isPassedBits() & TAPmask;
            }
        }
    }

    m_runNumber = eventInfo->runNumber();
    m_eventNumber = eventInfo->eventNumber();

    INTEvt_num = eventInfo->eventNumber();
    INTLum_block = eventInfo->lumiBlock();
    INTBCId = eventInfo->bcid();

    // Fill the events into the trees:
    tree("analysis")->Fill();

    tree("EventHeader")->Fill();
    tree("TrackingData")->Fill();
    tree("TriggerKeys")->Fill();
    tree("TriggerKeysBP")->Fill();
    tree("TriggerKeysAP")->Fill();
    tree("TriggerKeysAV")->Fill();
    tree("TriggerPrescales")->Fill();

    return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis::finalize()
{
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.
    return StatusCode::SUCCESS;
}
